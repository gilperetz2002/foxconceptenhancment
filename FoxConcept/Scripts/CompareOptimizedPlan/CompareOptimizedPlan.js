﻿var data;
var mTPTimelines = [];
var mUtilizationTimelines = [];

google.load("visualization", "1");
google.setOnLoadCallback(drawTimlines);

$(function ()
{

});

function drawTimlines()
{
	drawTPTimeline();
	drawUtilizationTimeline();
	$(document).trigger("DomUpdated");
	$("div.timeline-groups-axis-onleft");
	$("div.timeline-container").scroll(function ()
	{
		var leftGroup = $(this).parent();
		leftGroup.children("div.timeline-groups-axis-onleft").scrollTop($(this).scrollTop())
	});
}

function onRangeChange(ev)
{
	mTPTimelines[0].setVisibleChartRange(ev.start, ev.end);
	mTPTimelines[1].setVisibleChartRange(ev.start, ev.end);
	mUtilizationTimelines[0].setVisibleChartRange(ev.start, ev.end);
	mUtilizationTimelines[1].setVisibleChartRange(ev.start, ev.end);
}

function drawTPTimeline()
{
	var dataFromServer = [];
	dataFromServer[0] = $.parseJSON($("#timeline-data0").val());
	dataFromServer[1] = $.parseJSON($("#timeline-data1").val());

	var data = [];
	data[0] = convertJSONToTimelineData(dataFromServer[0]);
	data[1] = convertJSONToTimelineData(dataFromServer[1]);

	mTPTimelines[0] = drawTimeline(data[0], "timeline0");
	mTPTimelines[1] = drawTimeline(data[1], "timeline1");

	google.visualization.events.addListener(mTPTimelines[0], 'rangechange', onRangeChange);
	google.visualization.events.addListener(mTPTimelines[1], 'rangechange', onRangeChange);
}


function drawUtilizationTimeline()
{
	var utilizationData = [];
	utilizationData[0] = $.parseJSON($("#utilization-data0").val());
	utilizationData[1] = $.parseJSON($("#utilization-data1").val());

	var data = [];
	data[0] = convertJSONToUtilizationTimelineData(utilizationData[0]);
	data[1] = convertJSONToUtilizationTimelineData(utilizationData[1]);

	mUtilizationTimelines[0] = drawTimeline(data[0], "utilization-timeline0");
	mUtilizationTimelines[1] = drawTimeline(data[1], "utilization-timeline1");


	google.visualization.events.addListener(mUtilizationTimelines[0], 'rangechange', onRangeChange);
	google.visualization.events.addListener(mUtilizationTimelines[1], 'rangechange', onRangeChange);
}


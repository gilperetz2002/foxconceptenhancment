﻿var data;
var mTPTimeline;
var mUtilizationTimeline;

//google.load("visualization", "1");
google.setOnLoadCallback(drawTimlines);

$(function ()
{
	$(".unpairedCheckbox").click(function ()
	{
		var checkedItems = [];

		$(".unpairedCheckbox:checked").each(function ()
		{
			checkedItems.push(this.id);
		});

		if (checkedItems.length > 0)
		{
			$("#unpairedMultipleActionBar").show();
		}
		else
		{
			$("#unpairedMultipleActionBar").hide();
		}

	});
});

function drawTimlines()
{
	drawTPTimeline();
	drawUtilizationTimeline();
	$(document).trigger("DomUpdated");
	$("div.timeline-groups-axis-onleft");
	$("div.timeline-container").scroll(function ()
	{
		var leftGroup = $(this).parent();
		leftGroup.children("div.timeline-groups-axis-onleft").scrollTop($(this).scrollTop())
	});
}

function onRangeChange(ev)
{
	mUtilizationTimeline.setVisibleChartRange(ev.start, ev.end);
	mTPTimeline.setVisibleChartRange(ev.start, ev.end);
}

function drawTPTimeline()
{
	var dataFromServer = $.parseJSON($("#timeline-data").val());
	var data = convertJSONToTimelineData(dataFromServer);
	mTPTimeline = drawTimeline(data, "timeline", {'height': '280px'});
	google.visualization.events.addListener(mTPTimeline, 'rangechange', onRangeChange);
}


function drawUtilizationTimeline()
{
	var utilizationData = $.parseJSON($("#utilization-data").val());
	var data = convertJSONToUtilizationTimelineData(utilizationData);
	mUtilizationTimeline = drawTimeline(data, "utilization-timeline", { 'height': '280px' });
	google.visualization.events.addListener(mUtilizationTimeline, 'rangechange', onRangeChange);
}


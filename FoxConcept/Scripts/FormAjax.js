﻿$(function () {
    $('#FormSubmit').click(function (event) {
        var form = $('form');
        if (form.valid()) {
            $.ajax({
                url: $(form).attr('action'),
                type: $(form).attr('method'),
                data: form.serialize(),
                success: function (result) {
                    console.log(result);
                    console.log("success");
                    window.opener.location.reload();
                },
                beforeSend: function () {
                    $("#progressSpan").show();
                },
                complete: function () {
                    $("#progressImg").attr('src', $("#progressSpan").attr('data-success'));
                }
            });
        }
        return false;
    });
});


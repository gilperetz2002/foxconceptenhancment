﻿$(document).ready(function (event) { init() });

$(document).ajaxStop(function () { init() });

$(document).bind("DomUpdated", function () { init() });

function init()
{
	registerDatePickers();
	formatCurrencyValues();
	setTooltips();
	registerTogglers();
	registerMultipleActionBar();
	registerNewWindowTrigger()

}

function registerNewWindowTrigger()
{
	$('[data-new-window]').each(function (i, obj)
	{
		$(obj).click(function (e)
		{
			e.preventDefault();
			var url = $(this).attr("href");
			var width = $(this).data("newWindowWidth");
			var Height = $(this).data("newWindowHeight");
			var left = ($(window).width() / 2) - (width / 2);
			var top = ($(window).height() / 2) - (Height / 2);

			var popup = window.open(url, "_blank", "width=" + width + ", height=" + Height + ", top=" + top + ", left=" + left);
			
		});
	});
}

function registerMultipleActionBar()
{
	$(".MultipleActionBar").each(function ()
	{
		var $multipleActionBar = $(this);
		var togglerClass = $multipleActionBar.data("togglerElement");
		$("." + togglerClass).change(function (){
			var numOfCheckedTogglers = $("." + togglerClass + ":checked").length;
			if (numOfCheckedTogglers > 0)
				$multipleActionBar.show();
			else
				$multipleActionBar.hide();
		
		});
	});

}

function setTooltips()
{
	$('[data-tooltip!=""]').qtip({ // Grab all elements with a non-blank data-tooltip attr.
		content: {
			attr: 'data-tooltip' // Tell qTip2 to look inside this attr for it's content
		},
		style: {
			widget: true, // Use the jQuery UI widget classes
			def: false, // Remove the default styling (usually a good idea, see below)
			classes: "bks-tooltip"
		},
		show: {
			effect: function (offset)
			{
				$(this).slideDown(100); // "this" refers to the tooltip
			}
		}
	})
}

function registerDatePickers()
{
	$("input[type=date-picker]").datepicker({
		showOn: "button",
		buttonImage: "/Content/themes/base/images/DatePicker_Normal.png",
		buttonImageOnly: true,
		dateFormat: "mm/dd/yy"
	});
}

function registerTogglers()
{
	$("[toggler], [invers-toggler]").each(function (i, obj)
	{
		var isInvers = $(obj).attr("invers-toggler") != null;
		var togglerID = isInvers? $(obj).attr("invers-toggler") : $(obj).attr("toggler");

		var $toggler = $('#' + togglerID);
		$toggler.change(function ()
		{
			var checked = $(this).is(":checked");
			checked = isInvers? !checked:checked;
			if (checked)
				$(obj).attr('disabled', 'disabled');
			else
				$(obj).removeAttr('disabled', 'disabled');

		});
	});
}

function formatCurrencyValues()
{
	$('[currency]').formatCurrency({ colorize: true, region: 'en-US', roundToDecimalPlace:0 });
}

/**
* Calculate the color based on the given value.
* @param {number} H   Hue, a value be between 0 and 360
* @param {number} S   Saturation, a value between 0 and 1
* @param {number} V   Value, a value between 0 and 1
*/
var hsv2rgb = function (H, S, V)
{
	var R, G, B, C, Hi, X;

	C = V * S;
	Hi = Math.floor(H / 60);  // hi = 0,1,2,3,4,5
	X = C * (1 - Math.abs(((H / 60) % 2) - 1));

	switch (Hi)
	{
		case 0: R = C; G = X; B = 0; break;
		case 1: R = X; G = C; B = 0; break;
		case 2: R = 0; G = C; B = X; break;
		case 3: R = 0; G = X; B = C; break;
		case 4: R = X; G = 0; B = C; break;
		case 5: R = C; G = 0; B = X; break;

		default: R = 0; G = 0; B = 0; break;
	}

	return "RGB(" + parseInt(R * 255) + "," + parseInt(G * 255) + "," + parseInt(B * 255) + ")";
};

function popupWindow(url, width, heigth)
{
	var left = ($(window).width() / 2) - (width / 2),
    top = ($(window).height() / 2) - (heigth / 2),
    popup = window.open(url, "_Blank", "width=" + width + ", height=" + heigth + ", top=" + top + ", left=" + left);
}

var convertJsonDate = function (inputDate)
{
	return new Date(parseInt(inputDate.replace("/Date(", "").replace(")/", ""), 10));
};



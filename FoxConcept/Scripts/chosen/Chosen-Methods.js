﻿$(function () {
    $('.chosen-select').chosen({
        width: "90%",
        no_results_text: "No results found",
        disable_search_threshold: 10,
        placeholder_text_multiple: "Select a Value"
    });

});
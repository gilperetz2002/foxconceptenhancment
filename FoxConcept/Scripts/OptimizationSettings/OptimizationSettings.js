﻿
$(function () {

    $("#AdditionalTraininrRequirements select").each(function () {
        $(this).change(function () {
            var selectedMps = new Array();
            $("#AdditionalTraininrRequirements select option:selected").each(function (index, obj) {
                str = $(obj).text() + "";
                val = $(obj).val() + "";
                selectedMps.push(val);
            });
            //append Training request MPs.
            $("#trainingRequestMasterPlanID").each(function () {
                val = $(this).html();
                selectedMps.push(val);
            });
            getRelevantResources(selectedMps);

        });
    });

    $("#btnAddMasterPlan").click(function (e) {
        var masterPlanCount = 0;
        e.preventDefault();
        masterPlanCount = ($("#masterPlanOptimizationSettings tr.masterPlanInputRow").length - 1);
        masterPlanCount++;

        var selectDropdownTemplate = $("#masterPlanOptimizationSettings tr.masterPlanInputRow select")[0];
        selectDropdownTemplate.id = "AdditioanlTrainingRequests_" + masterPlanCount + "__.RequiredMasterPlan";
        selectDropdownTemplate.name = "AdditioanlTrainingRequests[" + masterPlanCount + "].RequiredMasterPlan";

        var htmlStr = new String();
        htmlStr += '<tr class="masterPlanInputRow">';
        htmlStr += '<td>' + selectDropdownTemplate.outerHTML + '</td>';
        htmlStr += '<td><input id="AdditioanlTrainingRequests_' + masterPlanCount + '__RequiredQuantity" name="AdditioanlTrainingRequests[' + masterPlanCount + '].RequiredQuantity" class="tiny" type="text" /><input id="cb_max[' + masterPlanCount + ']" name="cb_max_' + masterPlanCount + '" type="checkbox" /> <label for="cb_max[' + masterPlanCount + ']">Max.</label>';
        htmlStr += '<td><input id="AdditioanlTrainingRequests_' + masterPlanCount + '__RequestStartDate" name="AdditioanlTrainingRequests[' + masterPlanCount + '].RequestStartDate" class="medium" type="date-picker" /></td>';
        htmlStr += '<td><input id="AdditioanlTrainingRequests_' + masterPlanCount + '__RequestEndDate" name="AdditioanlTrainingRequests[' + masterPlanCount + '].RequestEndDate" class="medium" type="date-picker" /></td>';
        htmlStr += '<td><input id="AdditioanlTrainingRequests_' + masterPlanCount + '__Priorety" name="AdditioanlTrainingRequests[' + masterPlanCount + '].Priorety" class="tiny" type="text" /></td>';
        htmlStr += '</tr>';

        $("#masterPlanOptimizationSettings").children("table").append(htmlStr);
        $(document).trigger("DomUpdated");

    });

});

function getRelevantResources(relevantMasterPlanIDs)
{
	var $resourceSettingsContainer = $('#resourceTypeOptimizationSettings');
	//$resourceSettingsContainer.html("<img src='/Content/Images/Loading.gif' />");
	var params = relevantMasterPlanIDs.join("&RelevantMasterPlanIDs=");
	var actionUrl = '/optimization/OptimizationSettingsRelevantResources/?RelevantMasterPlanIDs=' + params + '';
	$.get(actionUrl, function (data)
	{
		$resourceSettingsContainer.html(data);
    });



}

function toggleInputVisiblity(TypeID, sender)
{
	var selectedValue = $(sender).val();
	if (selectedValue != 3)
	{
		$("#LimitInputBox-" + TypeID).hide();
		$("#PriorityInputBox-" + TypeID).prop('disabled', false);
	}
	else
	{
		$("#LimitInputBox-" + TypeID).show();

		$("#PriorityInputBox-" + TypeID).prop('disabled', true);
		$("#PriorityInputBox-" + TypeID).val("");
	}
}
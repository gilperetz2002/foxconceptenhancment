﻿var data;
var mResourceTimeline;

google.setOnLoadCallback(drawTimlines);

function drawTimlines() {
    drawResourcesTimeline();
    $(document).trigger("DomUpdated");
    $("div.timeline-groups-axis-onleft");
    $("div.timeline-container").scroll(function () {
        var leftGroup = $(this).parent();
        leftGroup.children("div.timeline-groups-axis-onleft").scrollTop($(this).scrollTop());
    });
}

function onRangeChange(ev) {
    mUtilizationTimeline.setVisibleChartRange(ev.start, ev.end);
    mTPTimeline.setVisibleChartRange(ev.start, ev.end);
}

function drawResourcesTimeline() {
    var dataFromServer = $.parseJSON($("#resource-data").val());
    var data = convertJSONToTimelineData(dataFromServer);
    mResourceTimeline = drawTimeline(data, "timeline", { 'height': '900px' });
    google.visualization.events.addListener(mResourceTimeline, 'rangechange', onRangeChange);
}



﻿function getAllocationAfterOptimizationDOM(val, available, required)
{
	// create item with minimum requirement
	
	var cellHeight = 50
	var height = Math.round((val / 100) * cellHeight); //normalize percentrage to the height of the cell
	height = height > cellHeight ? cellHeight : height;

	var hue = 120 - ((val / 100) * 120); // hue between 0 (red) and 120 (green)
	hue = hue < 0 ? 0 : hue;
	var color = hsv2rgb(hue, 0.95, 0.95);

	var greenColor = hsv2rgb(120, 0.95, 0.95);
	var redColor = hsv2rgb(0, 0.95, 0.95);

	color = (val > 100) ? redColor : "#4169A2";
	
	var style = 'height:' + height + 'px; background-color:' + color + ';';

	return '<div class="afterPlanUtilization" style="' + style + '" ' + 'title="' + val.toFixed(1) + '% (' + required + 'Hr/ ' + available + 'Hr)"></div>';
}


function getAllocationsBeforeOptimizationDOM(val, available, required)
{
	// create item with actual number
	var cellHeight = 45	
	var height = Math.round((val / 100) * cellHeight); //normalize percentrage to the height of the cell
	height = height > cellHeight ? cellHeight : height;

	style = 'height:' + height + 'px;';

	return '<div class="beforePlanUtilization" style="' + style + '" ' + ' title="' + val.toFixed(1) + '% (' + required + 'Hr/ ' + available + 'Hr)"></div>';
}

function drawTimeline(i_Data, i_TimelineContainerID)
{
	drawTimeline(i_Data, i_TimelineContainerID, null)
}

function drawTimeline(i_Data, i_TimelineContainerID, i_options)
{
	var data = i_Data;

	// specify options
	var options = {
		'width': '100%',
		'height': '270px',
		'groupsWidth': '170px',
		'editable': false, 			// Enable dragging and editing events
		'style': 'box',
		'selectable': true,
		'moveable': true,
		'showCurrentTime': false,
		'cluster': false, 			// Clusturing the events when zooming out
		'showNavigation': false,
		'axisOnTop': true
	};

	$.extend(options, i_options);

	// Instantiate our timeline object.
	var timeline = new links.Timeline(document.getElementById(i_TimelineContainerID));
	timeline.draw(data, options);

	return timeline;
}



function convertJSONToTimelineData(i_JSONData)
{
	var data = [];
	for (var i = 0; i < i_JSONData.length; i++)
	{
		var item = {
			'group': i_JSONData[i].GroupName,
			'start': convertJsonDate(i_JSONData[i].StartDate),
			'end': convertJsonDate(i_JSONData[i].EndDate),
			'content': i_JSONData[i].Content,
			'className': i_JSONData[i].ClassName + " tp-timeline-item"
		}
		data.push(item);
	}

	return data;
}

function convertJSONToUtilizationTimelineData(i_JSONData)
{
	var data = []
	for (var i = 0; i < i_JSONData.length; i++)
	{
		var currentStartValue = i_JSONData[i].StartDate;
		var start = convertJsonDate(currentStartValue);
		var end = convertJsonDate(currentStartValue);
		end.setHours(start.getHours() + 24);

		var origin = i_JSONData[i].Origin;
		var AllocatedMinutes = i_JSONData[i].SumAllocatedMinutes;
		var SumAllocatedMinutesBefore = i_JSONData[i].SumAllocatedMinutesBefore;
		var SumAllocatedMinutesAfter = i_JSONData[i].SumAllocatedMinutesAfter;
		var TotalAvalability = i_JSONData[i].TotalAvailability;
		var Utilization = AllocatedMinutes / TotalAvalability * 100;
		var UtilizationBefore = SumAllocatedMinutesBefore / TotalAvalability * 100;
		var UtilizationAfter = SumAllocatedMinutesAfter / TotalAvalability * 100;


		//var DOMABarElement = (origin == 1) ? getAllocationAfterOptimizationDOM(Utilization, TotalAvalability / 60, AllocatedMinutes / 60) : getAllocationsBeforeOptimizationDOM(Utilization, TotalAvalability / 60, AllocatedMinutes/60);

		var DOMABarElement = getAllocationAfterOptimizationDOM(UtilizationAfter, TotalAvalability / 60, SumAllocatedMinutesAfter / 60)
		var item = {
			'group': i_JSONData[i].ResourceTypeName,
			'start': start,
			'end': end,
			'content': DOMABarElement,
			'className': 'utilizationBar'
		}
		data.push(item);

		var DOMABarElement = getAllocationsBeforeOptimizationDOM(UtilizationBefore, TotalAvalability / 60, SumAllocatedMinutesBefore / 60)
		var item = {
			'group': i_JSONData[i].ResourceTypeName,
			'start': start,
			'end': end,
			'content': DOMABarElement,
			'className': 'utilizationBar'
		}
		data.push(item);
	}
	return data;
}

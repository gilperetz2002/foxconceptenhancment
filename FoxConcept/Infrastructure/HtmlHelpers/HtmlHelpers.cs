﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Linq.Expressions;
using System.Web.Routing;
using FoxConcept.Models;
using FoxConcept.Models.ViewModels.CostSettings;


namespace FoxConcept.Infrastructure
{
    public static class HtmlHelpers
    {
        public static MvcHtmlString LabelFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, Object htmlAttributes)
        {
            var metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            var innerText = metadata.DisplayName ?? metadata.PropertyName;

            if (String.IsNullOrEmpty(innerText))
            {
                return MvcHtmlString.Empty;
            }

            var tagBuilder = new TagBuilder("label");
            tagBuilder.Attributes.Add("for", TagBuilder.CreateSanitizedId(htmlHelper.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldName(metadata.PropertyName)));
            tagBuilder.MergeAttributes(new RouteValueDictionary(htmlAttributes));
            tagBuilder.SetInnerText(innerText);

            return new MvcHtmlString(tagBuilder.ToString(TagRenderMode.Normal));
        }

        public static MvcHtmlString ActionCostSettings(this HtmlHelper htmlHelper,
                                                               eEntityType costType, int costID, string lable, string controller, string action, bool newWindow = true)
        {
            string str = "<a ";

            switch (costType)
            {
                case eEntityType.Activity:
                    {
                        str += String.Format("href='/{0}/{1}/{2}'", controller,action,
                                             costID.ToString());
                        break;
                    }
                case eEntityType.HumanResource:
                    {
                        str += String.Format("href='/{0}/{1}/{2}'", controller, action,
                                             costID.ToString());
                        break;
                    }
                case eEntityType.PhysicalResource:
                    {
                        str += String.Format("href='/{0}/{1}/{2}'", controller, action,
                                             costID.ToString());
                        break;
                    }
               

            }

            if (newWindow)
            {
                str += " data-new-window='true' data-new-window-height='600' data-new-window-width='700'";
            }
            str += String.Format(" >{0}</a>", lable);

            return new MvcHtmlString(str);
        }

        public static MvcHtmlString NewWindowActionLink(this HtmlHelper htmlhelper, string action, string controller, string lable, string querystring = null, bool styleButton = true )
        {
            string str = "";
            if (styleButton)
            {
                str += "<button ";
            }
            else
            {
                str += "<a ";
            }

            str += String.Format("href='/{0}/{1}", controller, action);

            if (querystring != null)
            {
                str += String.Format("?{0}",querystring);
            }

            str += "' ";

            if (styleButton)
            {
                str += String.Format(" data-new-window='true' data-new-window-height='600' data-new-window-width='700'>{0}</button>", lable);
            }
            else
            {
                str += String.Format(" data-new-window='true' data-new-window-height='600' data-new-window-width='700'>{0}</a>", lable);
            }
            

            return new MvcHtmlString(str);
        }

        public static MvcHtmlString ActionSchedulingRuleDetails(this HtmlHelper htmlHelper,
                                                                SchedulingBusinessRule.eRuleType ruleType, int ruleID)
        {
            string str = "<a ";

            switch (ruleType)
            {
                case SchedulingBusinessRule.eRuleType.DaysOfftRule:
                    {
                        str += String.Format("href='SchedulingRules/EditDaysOffRule/{0}'",
                                             ruleID.ToString());
                        break;
                    }
                case SchedulingBusinessRule.eRuleType.AllocationIntervalLimit:
                    {
                        str += String.Format("href='SchedulingRules/EditAllocationIntervalLimit/{0}'",
                                             ruleID.ToString());
                        break;
                    }
                case SchedulingBusinessRule.eRuleType.AllocationWorkloadLimit:
                    {
                        str += String.Format("href='SchedulingRules/EditWorkloadLimit/{0}'",
                                             ruleID.ToString());
                        break;
                    }
                case SchedulingBusinessRule.eRuleType.AllocationTimeLimit:
                    {
                        str += String.Format("href='SchedulingRules/EditAllocationTimeLimit/{0}'",
                                             ruleID.ToString());
                        break;
                    }
                case SchedulingBusinessRule.eRuleType.ReschedulingLimit:
                    {
                        str += String.Format("href='SchedulingRules/EditReschedulingLimit/{0}'",
                                             ruleID.ToString());
                        break;
                    }


            }

            str += String.Format(" data-new-window='true' data-new-window-height='600' data-new-window-width='700'><img src='{0}' alt='Edit' /></a>", "/Content/Images/edit.gif");

            return new MvcHtmlString(str);
        }

        public static string GetDescription<TEnum>(this TEnum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            if (fi != null)
            {
                var display = ((DisplayAttribute[])fi.GetCustomAttributes(typeof(DisplayAttribute), false)).FirstOrDefault();
                if (display != null)
                {
                    return display.Name;
                }
            }



            return value.ToString();
        }

        public static MvcHtmlString NavigationTab(this HtmlHelper helper, string i_Url, string i_Lable, Object htmlAtrributes)
        {
            var Request = System.Web.HttpContext.Current.Request;
            var activeCSS = (string.Equals(Request.RawUrl, i_Url, StringComparison.InvariantCultureIgnoreCase)) ? "active" : "";


            var tagBuilder = new TagBuilder("a");
            tagBuilder.Attributes.Add("href",i_Url);
            tagBuilder.Attributes.Add("class",activeCSS);
            tagBuilder.MergeAttributes(new RouteValueDictionary(htmlAtrributes));
            tagBuilder.SetInnerText(i_Lable);


            return MvcHtmlString.Create(tagBuilder.ToString(TagRenderMode.Normal));

        }

        public static string GetDisplayName<TEnum>(this TEnum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            if (fi != null)
            {
                var attributes = (DisplayAttribute[])fi.GetCustomAttributes(typeof(DisplayAttribute), false);

                if (attributes.Length > 0)
                    return attributes[0].GetName();
            }

            return value.ToString();
        }

        public static string GetMinimumValueColor(this HtmlHelper helper, decimal val1, decimal val2)
        {
            return val1 < val2 ? "green" : "";
        }

    }
}
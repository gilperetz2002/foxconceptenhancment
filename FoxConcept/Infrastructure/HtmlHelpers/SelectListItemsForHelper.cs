﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace FoxConcept.Infrastructure
{
    public static class SelectListItemsForHelper
    {
        public static IEnumerable<SelectListItem> SelectListItemsFor<T>(T selected) where T : struct
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem()
                {
                    Text = "Select value",
                    Value = "-1"
                });
            Type t = typeof(T);
            if (t.IsEnum)
            {
                items.AddRange(Enum.GetValues(t).Cast<Enum>().Select(e => new SelectListItem { Value = e.ToString(), Text = e.GetDescription() }));
                return items;
            }
            return null;
        }

       
    }
}
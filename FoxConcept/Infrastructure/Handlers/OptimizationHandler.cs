﻿using System;
using System.Web;
using System.IO;
using System.Diagnostics;

namespace FoxConcept.Infrastructure.Handlers
{
	public class OptimizationHandler : IHttpHandler
	{
		#region IHttpHandler Members

		bool IHttpHandler.IsReusable
		{
			get { return false; }
		}

		void IHttpHandler.ProcessRequest(HttpContext context)
		{
			
		}

		#endregion
	}
}

﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace FoxConcept.Infrastructure
{
    public static class AppConfig
    {
        public static string OptimizationSummaryXML
        {
            get { return HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["OptimizationSummaryXML"]); }
        }

        public static string RecoveryDB
        {
            get { return HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["RecoveryDB"]); }
        }
    }
}
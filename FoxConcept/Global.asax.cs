﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Globalization;
using System.Threading;

namespace FoxConcept
{
	// Note: For instructions on enabling IIS6 or IIS7 classic mode, 
	// visit http://go.microsoft.com/?LinkId=9394801

	public class MvcApplication : System.Web.HttpApplication
	{
		public static void RegisterGlobalFilters(GlobalFilterCollection filters)
		{
			filters.Add(new HandleErrorAttribute());
		}

		public static void RegisterRoutes(RouteCollection routes)
		{
			routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

			routes.MapRoute("FoxWiki", "FoxWiki/WikiPage/{FoxTrainingProgramID}/{PageName}/{RevisionID}", new { controller = "FoxWiki", action = "WikiPage", PageName = "Main", RevisionID = UrlParameter.Optional });
			routes.MapRoute("FoxForum", "FoxForum/Forum/{ForumID}/", new { controller = "FoxForum", action = "DisplayForum", ForumID = 1 });
			routes.MapRoute("Default", "{controller}/{action}/{id}",new { controller = "Home", action = "Index", id = UrlParameter.Optional } );
			
		}

		protected void Application_Start()
		{
			AreaRegistration.RegisterAllAreas();

			RegisterGlobalFilters(GlobalFilters.Filters);
			RegisterRoutes(RouteTable.Routes);

           
		}

	    protected void Application_PreRequestHandlerExecute()
	    {
            CultureInfo ci = new CultureInfo("en-US");
            ci.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy";
            ci.DateTimeFormat.LongTimePattern = "dd/MM/yyyy hh:mm";

            //Finally setting culture for each request
            Thread.CurrentThread.CurrentUICulture = ci;
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(ci.Name);
	    }
	}
}
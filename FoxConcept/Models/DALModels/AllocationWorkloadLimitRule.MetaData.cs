﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Web.Mvc;

namespace FoxConcept.Models
{
    [MetadataTypeAttribute(typeof(AllocationWorkloadLimitRule.AllocationWorkloadLimitRuleMetaData))]
    public partial class AllocationWorkloadLimitRule
	{

		internal sealed class AllocationWorkloadLimitRuleMetaData
		{
			// Metadata classes are not meant to be instantiated.
            private AllocationWorkloadLimitRuleMetaData() { }

            public int ID { get; set; }

            [Display(Name = "Time Span (Hours)")]
            public int TimeSpanInDays { get; set; }

            [Display(Name = "Permitted Workload (hours)")]
            public int AllowdWorkloadInDays { get; set; }

            
			
		}
	}
	
}
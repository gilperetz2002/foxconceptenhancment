﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Web.Mvc;

namespace FoxConcept.Models
{
    [MetadataTypeAttribute(typeof(AllocationDaysOfftRule.AllocationDaysOfftRuleMetaData))]
	public partial class AllocationDaysOfftRule
	{

		internal sealed class AllocationDaysOfftRuleMetaData
		{
			// Metadata classes are not meant to be instantiated.
			private AllocationDaysOfftRuleMetaData(){}

            public int ID { get; set; }
            [Display(Name = "Time Span In Days")]
            public int TimeSpanInDays { get; set; }

            [Display(Name = "Days Off")]
            public int DaysOff { get; set; }

            
			
		}
	}
	
}
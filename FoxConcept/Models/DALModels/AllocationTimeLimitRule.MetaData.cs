﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Web.Mvc;

namespace FoxConcept.Models
{
    [MetadataTypeAttribute(typeof(AllocationTimeLimitRule.AllocationTimeLimitRuleMetaData))]
    public partial class AllocationTimeLimitRule
	{

		internal sealed class AllocationTimeLimitRuleMetaData
		{
			// Metadata classes are not meant to be instantiated.
            private AllocationTimeLimitRuleMetaData() { }

            public int ID { get; set; }

            [Display(Name = "Activity Type")]
            public Nullable<int> ActivityTypeID { get; set; }

            [Display(Name = "Week Day")]
            public int DayOfWeek { get; set; }

            [Display(Name = "Start Time")]
            public int StartTimeValue { get; set; }

            [Display(Name = "End Time")]
            public int EndTimeValue { get; set; }

            
			
		}
	}
	
}
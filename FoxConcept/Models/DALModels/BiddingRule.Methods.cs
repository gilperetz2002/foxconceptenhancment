﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Web.Mvc;

namespace FoxConcept.Models
{
    public partial class BiddingRule
    {



        public IEnumerable<BiddingPeriod> GetActiveBiddingPeriods()
        {

            var biddingPriods =
                this.BiddingCalendar.BiddingPeriods.AsEnumerable().Where(b => (b.StartDate.AddDays((int)this.StartBidding) <= DateTime.Now) && (b.EndDate.AddDays((int)this.EndBidding) >= DateTime.Now))
                    .ToList();

            return biddingPriods;
        } 

    }

}
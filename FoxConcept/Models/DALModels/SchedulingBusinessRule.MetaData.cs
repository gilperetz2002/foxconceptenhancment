﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Web.Mvc;

namespace FoxConcept.Models
{
	[MetadataTypeAttribute(typeof(SchedulingBusinessRule.SchedulingBusinessRuleMetaData))]
	public partial class SchedulingBusinessRule
	{

		internal sealed class SchedulingBusinessRuleMetaData
		{
			// Metadata classes are not meant to be instantiated.
			private SchedulingBusinessRuleMetaData(){}

			[HiddenInput(DisplayValue=false)]
			public int ID { get; set; }

			[DataType(DataType.Text)]
			[Display(Name="External ID")]
			public string ExternalID { get; set; }
			
			[DataType(DataType.Text)]
			[Display(Name = "Title")]
			public string Title { get; set; }

			[DataType(DataType.MultilineText)]
			[Display(Name = "Description")]
			public string Description { get; set; }

			[HiddenInput(DisplayValue = false)]
			public DateTime CreationDate { get; set; }

			[HiddenInput(DisplayValue = false)]
			public int AppliesToMDTypeID { get; set; }
			
			[HiddenInput(DisplayValue = false)]
			public int RuleTypeID { get; set; }

			[HiddenInput(DisplayValue = false)]
			public int RuleID { get; set; }

			[HiddenInput(DisplayValue = false)]
			public int ConstraintTypeID { get; set; }

			[HiddenInput(DisplayValue = false)]
			public int Priority { get; set; }

            
			
		}
	}
	
}
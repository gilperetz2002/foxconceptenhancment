﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Web.Mvc;

namespace FoxConcept.Models
{
	public partial class SchedulingBusinessRule
	{

		public enum eConstraintTypes
		{
            [Display(Name = "Hard Constraint")]
			HardConstraint = 0,
            [Display(Name = "Noraml Constraint")]
			NormalConstraint = 1,
		}

		public enum eRuleType
		{
            [Display(Name = "Allocation Time")]
			AllocationTimeLimit = 0,
            [Display(Name = "Rescheduling")]
			ReschedulingLimit = 1,
            [Display(Name = "Allocation Interval")]
			AllocationIntervalLimit = 2,
            [Display(Name = "Allocation Workload")]
			AllocationWorkloadLimit = 3,
            [Display(Name = "Days Off")]
			DaysOfftRule = 4,
		}

		public eConstraintTypes ConstraintType 
		{
			get { return (eConstraintTypes)ConstraintTypeID; }
			set { ConstraintTypeID = (int)value; } 
		}
		
		public eRuleType RuleType {
			get { return (eRuleType)RuleTypeID; }
			set { RuleTypeID = (int)value; } 
		}

        [Display(Name = "Apply To")]
        public string AppliesToMDTypeName { get; set; }

	}
	
}
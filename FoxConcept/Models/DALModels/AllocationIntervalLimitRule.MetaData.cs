﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Web.Mvc;

namespace FoxConcept.Models
{
    [MetadataTypeAttribute(typeof(AllocationIntervalLimitRule.AllocationIntervalLimitRuleMetaData))]
    public partial class AllocationIntervalLimitRule
	{

		internal sealed class AllocationIntervalLimitRuleMetaData
		{
			// Metadata classes are not meant to be instantiated.
            private AllocationIntervalLimitRuleMetaData() { }

            public int ID { get; set; }

            [Display(Name = "Between Activity Type")]
            public Nullable<int> ActivityTypeIDFrom { get; set; }

            [Display(Name = "And Activity Type")]
            public Nullable<int> ActivityTypeIDTo { get; set; }

            [Display(Name = "Minutes")]
            public int ValueInMinutes { get; set; }

            [Display(Name = "Exclude Activity Type")]
            public Nullable<int> ExcludeTypeID { get; set; }

            [Display(Name = "Exclude Threshold")]
            public Nullable<int> ExcludeThresholdInMinutes { get; set; }

            
			
		}
	}
	
}
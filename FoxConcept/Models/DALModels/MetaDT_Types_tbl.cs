//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FoxConcept.Models.DALModels
{
    using System;
    using System.Collections.Generic;
    
    public partial class MetaDT_Types_tbl
    {
        public int MDTypeID { get; set; }
        public string Name { get; set; }
        public Nullable<int> MDTypeIsVisible { get; set; }
        public Nullable<int> TargetContentEntityTypeID { get; set; }
        public Nullable<int> IsVisibleInpersonalFile { get; set; }
        public Nullable<int> StudentVisibilityMask { get; set; }
        public short MD_IsDeleted { get; set; }
        public Nullable<System.DateTime> MD_DateDeleted { get; set; }
        public Nullable<int> OwnerID { get; set; }
        public Nullable<int> OwnerType { get; set; }
        public string ExternalID { get; set; }
    }
}

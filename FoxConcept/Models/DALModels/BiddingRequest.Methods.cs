﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Web.Mvc;

namespace FoxConcept.Models
{
    public partial class BiddingRequest
    {

        public eStatusRequest StatusEnum
        {
            get
            {
                if (this.Status != null)
                {
                    return (eStatusRequest) Status;
                }

                return eStatusRequest.New;
            }
            set
            {
                if (StatusEnum != null)
                {
                    Status = (int) value;
                }

                Status = (int) eStatusRequest.New;
            }
        }

        public enum eStatusRequest
        {
            New = 1,
            InProgress = 2,
            Approved = 3,
            Declined = 4
        }

    }

}
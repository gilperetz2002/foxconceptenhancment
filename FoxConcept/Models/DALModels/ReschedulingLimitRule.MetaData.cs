﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects.DataClasses;
using System.Web.Mvc;

namespace FoxConcept.Models
{
    [MetadataTypeAttribute(typeof(ReschedulingLimitRule.ReschedulingLimitRuleMetaData))]
    public partial class ReschedulingLimitRule
	{

		internal sealed class ReschedulingLimitRuleMetaData
		{
			// Metadata classes are not meant to be instantiated.
            private ReschedulingLimitRuleMetaData() { }

            public int ID { get; set; }

            [Display(Name = "Activity Type")]
            public Nullable<int> ActivityTypeID { get; set; }

            [Display(Name = "Operator")]
            public int Operator { get; set; }

            [Display(Name = "Minutes")]
            public int ValueInMinutes { get; set; }
            public bool LimitToOriginalWorkDay { get; set; }
            
			
		}
	}
	
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace FoxConcept.Models.ViewModels.Timeline
{
    public class ResourceGanttViewModel
    {
        //Properties
        public List<GanttItem> Items { get; set; }

        public ResourceGanttViewModel()
        {
            Items = new List<GanttItem>();
        }


        public string GetJson()
        {
            var serializer = new JavaScriptSerializer();

            // For simplicity just use Int32's max value.
            // You could always read the value from the config section mentioned above.
            serializer.MaxJsonLength = Int32.MaxValue;

            var resultData = serializer.Serialize(Items);

            return resultData;
        }
    }
}
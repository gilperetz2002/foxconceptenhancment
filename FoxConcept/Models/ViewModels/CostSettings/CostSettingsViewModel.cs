﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FoxConcept.Models.ViewModels.CostSettings
{
    public class CostSettingsViewModel
    {
        public List<CostSettingViewModel> CostSettingViewModels { get; set; }
    }

    public class CostSettingViewModel
    {

        public int ID { get; set; }

        [Required(ErrorMessage = "Name is required")]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Display(Name = "Entity Type")]
        public eEntityType EntityType { get; set; }

        [Display(Name = "Applies to")]
        public int EntityTypeID { get; set; }

        [Display(Name = "Entity Name")]
        public string EntityTypeName { get; set; }

        [Display(Name = "Applies to")]
        public List<EntityType> EntityTypes { get; set; }

        [Display(Name = "Cost For")]
        public eCostType CostType { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:c0}")]
        public decimal Cost { get; set; }

        [Display(Name = "Cost Per")]
        public eCostPer CostPer { get; set; }

        [Display(Name = "Enable (optimization)")]
        public bool ConsiderInOptimization { get; set; }

        [Display(Name = "Comments")]
        public string Comments { get; set; }

        public int LocationID { get; set; }

        [Display(Name = "Location")]
        public string LocationName { get; set; }

        public List<EntityType> Locations { get; set; }

        [Display(Name = "Target Content")]
        public eTargetGroupType TargetGroupType { get; set; }

        [Display(Name = "Time of day")]
        public eStateType StateType { get; set; }

        public CostSettingViewModel()
        {
            EntityTypes = new List<EntityType>();
            Locations = new List<EntityType>();
        }
    }

    public class EntityType
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
    
    public enum eStateType
    {
        [Display(Name = "During working hours")]
        EntityType = 1,
        [Display(Name = "Outside working hours")]
        EntityProperty = 2,
        [Display(Name = "None working day")]
        SpecificEntity = 3,
    }

    public enum eTargetGroupType
    {
        [Display(Name = "Entity type")]
        EntityType = 0,
        [Display(Name = "Entity property")]
        EntityProperty = 2,
        [Display(Name = "Specific entity")]
        SpecificEntity = 3
    }
    
    public enum eEntityType
    {
        Activity = 12,
        HumanResource = 1,
        PhysicalResource = 2,
        Participant = 3
    }

    public enum eCostType
    {
        [Display(Name = "Cost of Training")]
        Training = 1,
        [Display(Name = "Hotel Expenses")]
        HotelExpenses = 2,
        [Display(Name = "Travel Expenses")]
        TravelExpenses = 3,
        [Display(Name = "Travel Time")]
        TravelTime = 4,
        [Display(Name = "Cost of Training")]
        TrainingMeterials = 5,
        [Display(Name = "Overtime")]
        Overtime = 6,
        [Display(Name = "Configuration Changes")]
        Configuration = 7


        
    }

    public enum eCostPer
    {
        Unit = 1,
        Day = 2,
        Hour = 3,
        Fixed = 4
    }

    public class CostTypeViewModel
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public eCostType CostType { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:c0}")]
        public decimal Cost { get; set; }

        [Display(Name = "Cost Per")]
        public eCostPer CostPer { get; set; }

        public int LocationID { get; set; }

        [Display(Name = "Location")]
        public string LocationName { get; set; }

        public List<EntityType> Locations { get; set; }

        public eTargetGroupType TargetGroupType { get; set; }

        public eStateType StateType { get; set; }
        
        public CostTypeViewModel()
        {
            Locations = new List<EntityType>();
            Locations.Add(new EntityType()
                {
                    ID = -1,
                    Name = "Select value"
                });
        }
    }

    
}
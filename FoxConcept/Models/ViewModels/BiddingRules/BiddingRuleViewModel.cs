﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using FoxConcept.Models.ViewModels.CostSettings;

namespace FoxConcept.Models.ViewModels.BiddingRules
{
    public class BiddingRuleViewModel
    {
        public int ID { get; set; }

        public string Name { get; set; }

        [Display(Name = "Entity Type")]
        public eEntityType EntityType { get; set; }

        [Display(Name = "Entity")]
        public int EntityTypeID { get; set; }

        [Display(Name = "Entity Name")]
        public string EntityTypeName { get; set; }

        [Display(Name = "Entity")]
        public List<EntityType> EntityTypes { get; set; }

        [Display(Name = "Type")]
        public EBiddingType BiddingType { get; set; }

        public int BiddingCalendarID { get; set; }

        [Display(Name = "Bidding Calendar")]
        public List<EntityType> BiddingCalendars { get; set; }

        [Display(Name = "Number of days to bid")]
        public int DaysPerInstructor { get; set; }

        [Display(Name = "Start Bidding day")]
        public int StartBidding { get; set; }

        [Display(Name = "End Bidding day")]
        public int EndBidding { get; set; }
    }

    public class BiddingRuleItemList
    {
        public int ID { get; set; }

        public string Name { get; set; }

        [Display(Name = "Entity Type")]
        public eEntityType EntityType { get; set; }

        [Display(Name = "Entity")]
        public int EntityTypeID { get; set; }

        [Display(Name = "Entity Name")]
        public string EntityTypeName { get; set; }

        public BiddingCalendar BiddingCalendar { get; set; }

        [Display(Name = "Number of days per instructor")]
        public int DaysPerInstructor { get; set; }

        [Display(Name = "Start Bidding day")]
        public int StartBidding { get; set; }

        [Display(Name = "End Bidding day")]
        public int EndBidding { get; set; }

        [Display(Name = "Bidding Type")]
        public EBiddingType BiddingType { get; set; }
    }

    public class BiddingPeriodViewModel
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "Name is required")]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [DataType(DataType.Date)]
        [Required(ErrorMessage = "Start date is required")]
        [Display(Name ="Start Date")]
        public DateTime StartDate { get; set; }

        [DataType(DataType.Date)]
        [Required(ErrorMessage = "End date is required")]
        [Display(Name = "End Date")]
        public DateTime EndDate { get; set; }

        public int CalendarID { get; set; }
    }

    public enum EBiddingType
    {
        [Display(Name = "Days off")]
        DaysOff = 1,

        [Display(Name = "Vacation")]
        Vacations = 2,

        [Display(Name = "Training day")]
        TrainingDay
    }

    public class BiddingCalendarViewMode
    {
        public int ID { get; set; }
        [Required(ErrorMessage = "Required field")]
        public string Name { get; set; }
    }


}
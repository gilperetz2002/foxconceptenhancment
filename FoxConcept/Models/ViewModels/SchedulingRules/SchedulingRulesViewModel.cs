﻿
namespace FoxConcept.Models
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using FoxConcept.Models.DALModels;

    public class SchedulingRulesViewModel
    {
        public IEnumerable<SchedulingBusinessRule> SchedulingRules { get; set; }

        public IDictionary<int, string> ApplyTo { get; set; }

        public SchedulingRulesViewModel()
        {

        }

        public SchedulingRulesViewModel(FoxModelContainer i_FoxModel, FoxConceptEntities i_FoxConceptModel)
        {
            ApplyTo = new Dictionary<int, string>();

            ApplyTo = i_FoxModel.DWH_ResourcesBasicData
                                                     .Select(r => new { r.ResourceMetaDataTypeID, r.ResourceMetaDataTypeName }).Distinct()
                                                     .ToDictionary(r => r.ResourceMetaDataTypeID, r => r.ResourceMetaDataTypeName);


        }
    }


}

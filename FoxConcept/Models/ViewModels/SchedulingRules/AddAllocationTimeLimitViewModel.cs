﻿
namespace FoxConcept.Models
{
	using System;
	using System.Data.Entity;
	using System.Linq;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using FoxConcept.Models.DALModels;

    public class AddAllocationTimeLimitViewModel : BaseSchedulingRuleForm
	{
	    public AllocationTimeLimitRule RuleDetails { get; set; }

        public IDictionary<int, string> AvailableActivityTypes { get; set; }

        public IDictionary<int,string> DayInWeek { get; set; }


        public AddAllocationTimeLimitViewModel()
        {
            
        }

        public AddAllocationTimeLimitViewModel(FoxModelContainer i_FoxModel, FoxConceptEntities i_FoxConceptModel)
			: base(i_FoxModel, i_FoxConceptModel)
        {
            RuleDetails = new AllocationTimeLimitRule();
            
            AvailableActivityTypes = new Dictionary<int, string>();

            AvailableActivityTypes = i_FoxModel.DWH_Activities.Select(ac => new { ac.Activity_MetaDataTypeID, ac.Activity_MetaDataTypeName }).Distinct().OrderBy(ac => ac.Activity_MetaDataTypeName).ToDictionary(ac => (int)ac.Activity_MetaDataTypeID, ac => ac.Activity_MetaDataTypeName);

            AvailableActivityTypes.Add(-1, "All");

            DayInWeek = new Dictionary<int, string>();
            DayInWeek.Add(2,"Monday");
            DayInWeek.Add(3, "Tuesday");
            DayInWeek.Add(4, "Wednesday");
            DayInWeek.Add(5, "Thursday");
            DayInWeek.Add(6, "Friday");
            DayInWeek.Add(7, "Saturday");
            DayInWeek.Add(1, "Sunday");
            DayInWeek.Add(8, "<All days>");


            base.GeneralDetails.RuleType = SchedulingBusinessRule.eRuleType.AllocationTimeLimit;
        }


	}
}

﻿
namespace FoxConcept.Models
{
	using System;
	using System.Data.Entity;
	using System.Linq;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using FoxConcept.Models.DALModels;

	public class AddAllocationIntervalLimitViewModel: BaseSchedulingRuleForm
	{
		public AllocationIntervalLimitRule RuleDetails { get; set; }

        public IDictionary<int,string> AvailableActivityTypes { get; set; }

	    public AddAllocationIntervalLimitViewModel()
	    {
	        
	    }
        
		public AddAllocationIntervalLimitViewModel(FoxModelContainer i_FoxModel, FoxConceptEntities i_FoxConceptModel)
			: base(i_FoxModel, i_FoxConceptModel)
		{
            RuleDetails = new AllocationIntervalLimitRule();
            AvailableActivityTypes = new Dictionary<int, string>();
            
            AvailableActivityTypes = i_FoxModel.DWH_Activities.Select(ac => new { ac.Activity_MetaDataTypeID, ac.Activity_MetaDataTypeName }).Distinct().OrderBy(ac => ac.Activity_MetaDataTypeName).ToDictionary(ac => (int)ac.Activity_MetaDataTypeID, ac => ac.Activity_MetaDataTypeName);

            AvailableActivityTypes.Add(-1, "All");
            AvailableActivityTypes.Add(-2, "None");

            
            base.GeneralDetails.RuleType = SchedulingBusinessRule.eRuleType.AllocationIntervalLimit;


		}

		public AddAllocationIntervalLimitViewModel(FoxModelContainer i_FoxModel, FoxConceptEntities i_FoxConceptModel, SchedulingBusinessRule i_BaseRule, AllocationIntervalLimitRule i_AllocationIntervalRule)
			: base(i_FoxModel, i_FoxConceptModel, i_BaseRule)
		{
			RuleDetails = i_AllocationIntervalRule;
		}

	}
}

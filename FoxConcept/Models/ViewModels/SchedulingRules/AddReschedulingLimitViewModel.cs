﻿
namespace FoxConcept.Models
{
	using System;
	using System.Data.Entity;
	using System.Linq;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using FoxConcept.Models.DALModels;

    public class AddReschedulingLimitViewModel : BaseSchedulingRuleForm
	{
        public ReschedulingLimitRule RuleDetails { get; set; }

        public IDictionary<int, string> AvailableActivityTypes { get; set; }

	    public IDictionary<int,string> Operators { get; set; }

	    public int ValueInMinutes { get; set; }

        

	    public AddReschedulingLimitViewModel()
	    {
	        
	    }

        public AddReschedulingLimitViewModel(FoxModelContainer i_FoxModel, FoxConceptEntities i_FoxConceptModel):
            base(i_FoxModel, i_FoxConceptModel)
	    {
            RuleDetails = new ReschedulingLimitRule();
            AvailableActivityTypes = new Dictionary<int, string>();

            AvailableActivityTypes = i_FoxModel.DWH_Activities.Select(ac => new { ac.Activity_MetaDataTypeID, ac.Activity_MetaDataTypeName }).Distinct().OrderBy(ac => ac.Activity_MetaDataTypeName).ToDictionary(ac => (int)ac.Activity_MetaDataTypeID, ac => ac.Activity_MetaDataTypeName);

            AvailableActivityTypes.Add(-1, "All");
            AvailableActivityTypes.Add(-2, "None");

            Operators = new Dictionary<int, string>();
            Operators.Add(0, "Earlier");
            Operators.Add(1, "Later");

            base.GeneralDetails.RuleType = SchedulingBusinessRule.eRuleType.ReschedulingLimit;
	    }
	}
}

﻿
namespace FoxConcept.Models
{
	using System;
	using System.Data.Entity;
	using System.Linq;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using FoxConcept.Models.DALModels;

	public class AddWorkloadLimitViewModel : BaseSchedulingRuleForm
	{
         public AllocationWorkloadLimitRule RuleDetails { get; set; }

	    public int TimeSpanInDays { get; set; }

	    public int AllowedWorkloadInDays { get; set; }

	    public AddWorkloadLimitViewModel()
	    {
	        
	    }

        public AddWorkloadLimitViewModel(FoxModelContainer i_FoxModel, FoxConceptEntities i_FoxConceptModel) : 
        base(i_FoxModel, i_FoxConceptModel)
        {
            RuleDetails = new AllocationWorkloadLimitRule();
            
            
            base.GeneralDetails.RuleType = SchedulingBusinessRule.eRuleType.AllocationTimeLimit;
        }
	}
}

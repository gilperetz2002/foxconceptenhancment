﻿
namespace FoxConcept.Models
{
	using System;
	using System.Data.Entity;
	using System.Linq;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using FoxConcept.Models.DALModels;

	public class AddDaysOffRuleViewModel : BaseSchedulingRuleForm
	{
	    public AllocationDaysOfftRule RuleDetails { get; set; }

	    public int TimeSpanInDays { get; set; }

	    public int DaysOff { get; set; }

	    public AddDaysOffRuleViewModel()
	    {
	        
	    }

	    public AddDaysOffRuleViewModel(FoxModelContainer i_FoxModel, FoxConceptEntities i_FoxConceptModel): 
        base(i_FoxModel, i_FoxConceptModel)
        {
            RuleDetails = new AllocationDaysOfftRule();
            
            
            base.GeneralDetails.RuleType = SchedulingBusinessRule.eRuleType.AllocationTimeLimit;
        }
	    
	        
	    
	}
}

﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FoxConcept.Models.DALModels;

namespace FoxConcept.Models
{
	public class BaseSchedulingRuleForm
	{
		public IDictionary<int,string> AvailablePhysicalResourceType { get; set; }
		public IDictionary<int, string> AvailableHumanResourceType { get; set; }
		public IDictionary<int, string> AvailableUserType { get; set; }

		public SchedulingBusinessRule GeneralDetails { get; set; }

	    public BaseSchedulingRuleForm()
	    {
	        
	    }
		public BaseSchedulingRuleForm(FoxModelContainer i_FoxModel, FoxConceptEntities i_FoxConceptModel)
		{
            GeneralDetails = new SchedulingBusinessRule();

			AvailablePhysicalResourceType = new Dictionary<int, string>();
			AvailablePhysicalResourceType = i_FoxModel.DWH_ResourcesBasicData
													.Where(r=>r.ResourceType == 1) 
													.Select(r => new { r.ResourceMetaDataTypeID, r.ResourceMetaDataTypeName }).Distinct()
													.ToDictionary(r => r.ResourceMetaDataTypeID, r => r.ResourceMetaDataTypeName);

			AvailableHumanResourceType = new Dictionary<int, string>();
			AvailableHumanResourceType = i_FoxModel.DWH_ResourcesBasicData
													.Where(r => r.ResourceType == 2) 
													.Select(r => new { r.ResourceMetaDataTypeID, r.ResourceMetaDataTypeName }).Distinct()
													.ToDictionary(r => r.ResourceMetaDataTypeID, r => r.ResourceMetaDataTypeName);

			AvailableUserType = new Dictionary<int, string>();
			AvailableUserType.Add(1, "Pilots");
			AvailableUserType.Add(2, "Customers");


		}

		public BaseSchedulingRuleForm(FoxModelContainer i_FoxModel, FoxConceptEntities i_FoxConceptModel, SchedulingBusinessRule i_BaseRule)
			: this(i_FoxModel, i_FoxConceptModel)
		{
			GeneralDetails = i_BaseRule;
		}

	}
}
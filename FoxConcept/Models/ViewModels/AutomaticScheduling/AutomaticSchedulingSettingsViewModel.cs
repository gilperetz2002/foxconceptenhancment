﻿
namespace FoxConcept.Models
{
	using System;
	using System.Data.Entity;
	using System.Linq;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using FoxConcept.Models.DALModels;
	using FoxConcept.Models.ViewModels.Shared;

	public class AutomaticSchedulingSettingsViewModel
	{
		public ResourceType[] ResourceTypesToConcider { get; set; }
		public AutomaticScheduleRequirement[] SchedulingRequirements { get; set; }
		public IEnumerable <MasterPlan> AvailableMasterPlans { get; set; }

		public class AutomaticScheduleRequirement
		{
			public IList<FoxUser> RequiringUsers { get; set; }
			public MasterPlan MasterPlan { get; set; }
			public int CertificationID { get; set; }
			public int? RequiredQuantity { get; set; }
			public IList<TrainingProgram> AvailableTrainingPrograms { get; set; }
			
			[DataType(System.ComponentModel.DataAnnotations.DataType.Date)]
			[DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
			public DateTime? RequiredStartDate { get; set; }

			[DataType(System.ComponentModel.DataAnnotations.DataType.Date)]
			[DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
			public DateTime? RequiredEndDate { get; set; }

			public string RequiredStartDateStr 
			{
				get { return RequiredStartDate.HasValue ? RequiredStartDate.Value.ToString("MM/dd/yyyy") : null; }
				set { RequiredStartDate = DateTime.ParseExact(value, "MM/dd/yyyy", null); }
			}
			public string RequiredEndDateStr 
			{
				get { return RequiredEndDate.HasValue ? RequiredEndDate.Value.ToString("MM/dd/yyyy") : null; }
				set { RequiredEndDate = DateTime.ParseExact(value, "MM/dd/yyyy", null); }
			}

			public AutomaticScheduleRequirement()
			{
				MasterPlan = new MasterPlan();
			}
		}
	}


}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FoxConcept.Models.ViewModels.Shared;
using FoxConcept.Models.DALModels;

namespace FoxConcept.Models
{
	public class AutomaticAssignemntsResultsViewModel
	{
		public List<UserTrainingProgramRegistration> UsersTrainingProgramRegistrations { get; set; }
		public List<TrainingRequest> UsersWithNoTrainingProgram { get; set; }
		public List<DWH_User_Certification> UsersWithNoTrainingProgramCertifciationInfo { get; set; }

		public class UserTrainingProgramRegistration
		{
			public FoxUser Users { get; set; }
			public TrainingProgram TrainingProgram { get; set; }
		}
	}

}

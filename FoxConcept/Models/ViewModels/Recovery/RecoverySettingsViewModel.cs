﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FoxConcept.Infrastructure;
using FoxConcept.Models.ViewModels.CostSettings;
using FoxConcept.Models.ViewModels.Optimization;

namespace FoxConcept.Models.ViewModels.Recovery
{
    
    public class RecoverySettingsViewModel
    {
        
        public DateTime EventsStartDate { get; set; }
        public DateTime EventsEndDate { get; set; }

        [Display(Name = "Start Date")]
        public DateTime TargetStartDate { get; set; }

        [Display(Name = "End Date")]
        public DateTime TargetEndTime { get; set; }

        public List<ResouceAllocationSettings> ResourceAllocations { get; set; }

        public List<EventDetailsViewModel> events { get; set; }

        public List<OptimizationGoal> Goals { get; set; }
        public IEnumerable<SchedulingBusinessRule> SchedulingBusinessRules { get; set; }

        public RecoverySettingsViewModel()
        {
            ResourceAllocations = new List<ResouceAllocationSettings>();
        }

        public List<OptimizationGoal> GetAvailableGoals()
        {
            List<OptimizationGoal> goals = new List<OptimizationGoal>();
            int goalID = 1;
            foreach (var value in Enum.GetValues(typeof(eCostType)))
            {
                goals.Add(new OptimizationGoal()
                {
                    GoalTypeID = goalID,
                    GoalTypeDescription = ((eCostType)value).GetDisplayName(),
                    Priority = 1,
                    Value = "50",
                    Operators = new SelectListItem[]{
					new SelectListItem{
						Selected= false,
						Text = "Maximize",
						Value = "100"
					}, new SelectListItem{
						Selected= true,
						Text = "Minimize",
						Value = "1"
					}
				}
                });

                goalID++;
            }
            goals.Add(new OptimizationGoal()
            {
                GoalTypeID = goalID,
                GoalTypeDescription = "Seniority",
                Priority = 1,
                Value = "50",
                Operators = new SelectListItem[]{
					new SelectListItem{
						Selected= false,
						Text = "Maximize",
						Value = "100"
					}, new SelectListItem{
						Selected= true,
						Text = "Minimize",
						Value = "1"
					}
				}
            });




            return goals;
        }

    }

    [Serializable]
    public class ResouceAllocationSettings
    {
        public bool Checked { get; set; }
        public int ID { get; set; }
        public int ResourceID { get; set; }

        public string ResourceName { get; set; }

        public IEnumerable<EntityDropDown> PreferedResource { get; set; }

        public List<EntityDropDown> MacthingStatus { get; set; }

        public List<EntityDropDown> HendlingAllocatedResources { get; set; }

        public List<EntityDropDown> Priority { get; set; }

        public IEnumerable<EntityDropDown> AlternativeResources { get; set; }

        public ResouceAllocationSettings()
        {
            MacthingStatus = new List<EntityDropDown>();

            MacthingStatus.Add(new EntityDropDown()
            {
                ID = 1,
                Name = "Full Match"
            });

            MacthingStatus.Add(new EntityDropDown()
            {
                ID = 2,
                Name = "Partial Match"
            });

            MacthingStatus.Add(new EntityDropDown()
            {
                ID = 3,
                Name = "Any Match"
            });

            HendlingAllocatedResources = new List<EntityDropDown>();

            HendlingAllocatedResources.Add(new EntityDropDown()
            {
                ID = 1,
                Name = "Keep allocated resources"
            });

            HendlingAllocatedResources.Add(new EntityDropDown()
            {
                ID = 2,
                Name = "Replace allocated resources if unavailable"
            });

            HendlingAllocatedResources.Add(new EntityDropDown()
            {
                ID = 3,
                Name = "Replace allocated resources"
            });

            Priority = new List<EntityDropDown>();

            Priority.Add(new EntityDropDown()
            {
                ID = 1,
                Name = "High"
            });

            Priority.Add(new EntityDropDown()
            {
                ID = 2,
                Name = "Medium"
            });

            Priority.Add(new EntityDropDown()
            {
                ID = 2,
                Name = "Low"
            });
        }
    }
}
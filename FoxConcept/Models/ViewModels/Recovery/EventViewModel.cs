﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using FoxConcept.Models.DALModels;

namespace FoxConcept.Models.ViewModels.Recovery
{
    public class EventTableListViewModel
    {
        public List<EventDetailsViewModel> events { get; set; }

        //Filters
        [Display(Name = "From")]
        public DateTime StartDate { get; set; }
        [Display(Name = "To")]
        public DateTime EndDate { get; set; }

        public string EventNameSearch { get; set; }

        [Display(Name = "Orgenizational Units")]
        public IEnumerable<EntityDropDown> Ous { get; set; }

        [Display(Name = "Organizational Units")]
        public List<int> ou_codes { get; set; }

        [Display(Name = "Training Programs")]
        public IEnumerable<EntityDropDown> Tps { get; set; }

        [Display(Name = "Training Programs")]
        public List<int> tps_code { get; set; }

        [Display(Name = "Activity Type")]
        public IEnumerable<EntityDropDown> ActivityTypes { get; set; }

        [Display(Name = "Activity Type")]
        public List<int> act_code { get; set; }

        [Display(Name = "Required Physical Resources")]
        public IEnumerable<EntityDropDown> PhysicalResourceTypes { get; set; }

        [Display(Name = "Required Physical Resources")]
        public List<int> phy_code { get; set; }

        [Display(Name = "Required Human Resources")]
        public IEnumerable<EntityDropDown> HumanResouceTypes { get; set; }

        [Display(Name = "Required Human Resources")]
        public List<int> hr_code { get; set; }

        public EventTableListViewModel()
        {
            StartDate = DateTime.Now.AddMonths(-1);
            EndDate = DateTime.Now.AddMonths(1);
        }


    }

    [Serializable]
    public class EventDetailsViewModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int TrainingProgramID { get; set; }
        public string TrainingProgramName { get; set; }
        public int OUID { get; set; }
        public string OUName { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public int EventType { get; set; }
        public string EventTypeName { get; set; }
        public int Duration { get; set; }
        public bool Checked { get; set; }
    }

    public class EntityDropDown
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}
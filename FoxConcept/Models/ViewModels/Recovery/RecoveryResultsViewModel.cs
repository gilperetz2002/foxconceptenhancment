﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using FoxConcept.Infrastructure;
using LinqToExcel;

namespace FoxConcept.Models.ViewModels.Recovery
{
    public class RecoveryResultsViewModel
    {
        public List<RecoveryResult> results { get; set; }

        public RecoveryResultsViewModel()
        {
            results = new List<RecoveryResult>();

        }
    }

    public class RecoveryResult
    {
        [Display(Name = "Organizational Unit")]
        public string OrganizationlUnit { get; set; }


        [Display(Name = "Training Program")]
        public string TrainingProgram { get; set; }

        [Display(Name = "Event Name")]
        public string EventName { get; set; }

        public DateTime EventOriginalStartDate { get; set; }
        public string EventOriginalStartTime { get; set; }
        public string EventOriginalEndTime { get; set; }
        public DateTime EventNewStartDate { get; set; }
        public string EventNewStartTime { get; set; }
        public string EventNewEndTime { get; set; }
        public string PhysicalResource { get; set; }
        public string HumanResource { get; set; }
    }
}
﻿using FoxConcept.Models.ViewModels.Optimization;


namespace FoxConcept.Models
{
	using System;
	using System.Data.Entity;
	using System.Linq;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using FoxConcept.Models.DALModels;

	public class ComparePlansViewModel
	{
		public ShowPlanViewModel[] Plans { get; set; }
	}
}

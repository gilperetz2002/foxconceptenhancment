﻿using System.Runtime.CompilerServices;
using System.Xml.Linq;
using FoxConcept.Infrastructure;
using FoxConcept.Models.ViewModels.Optimization;
using WikiPlex.Compilation.Macros;


namespace FoxConcept.Models
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using FoxConcept.Models.DALModels;
    using FoxConcept.Models.ViewModels.Shared;

    public class ShowPlanViewModel
    {
        public long PlanID { get; set; }
        public string PlanName { get; set; }
        public IEnumerable<TrainingProgram> TrainingPrograms { get; set; }
        public IEnumerable<GanttItem> GanntItems { get; set; }
        public IEnumerable<DayliUtilizationItem> UtilizationItems { get; set; }
        public IEnumerable<MasterPlanOptimizationStatistics> RequiredMasterPlanOptimizationStatistics { get; set; }
        public IEnumerable<MasterPlanOptimizationStatistics> AdditionalMasterPlanOptimizationStatistics { get; set; }
        public List<OptimizationGoal> Goals { get; set; }
        public PlanSummaryTable SummaryTableStatistic { get; set; }

        public string InputFilePath { get; set; }


    }

    public class GanttItem
    {
        public string GroupName { get; set; }
        public string Content { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string ClassName { get; set; }

    }

    public class DayliUtilizationItem
    {
        public DateTime StartDate { get; set; }
        public int ResourceTypeID { get; set; }
        public string ResourceTypeName { get; set; }
        public int SumAllocatedMinutes { get; set; }

        public int SumAllocatedMinutesBefore { get; set; }
        public int SumAllocatedMinutesAfter { get; set; }

        public int TotalAvailability { get; set; }
        public int Origin { get; set; }
    }

    public class MasterPlanOptimizationStatistics
    {
        public int MasterPlanID { get; set; }
        public string MasterPlanName { get; set; }

        public int ScheduledRequests { get; set; }
        public int UnscheduledRequests { get; set; }

        public int PairedRequests { get; set; }
        public int UnpairedRequests { get; set; }

        public int ScheduledCaptains { get; set; }
        public int ScheduledFirstOfficers { get; set; }

        public int UnpairedCaptains { get; set; }
        public int UnpairedFirstOfficers { get; set; }

        public long TotalCost { get; set; }
        public long TotalRevenue { get; set; }
    }

    public class PlanSummaryTable
    {
        public TrainingCosts TrainingCosts { get; set; }
        public TrainingValueAndRevalue TrainingValueAndRevalue { get; set; }

        public CostValues BiddingRequestsMet { get; set; }
        public CostValues BiddingRequestsMetSummary { get; set; }

        public CostValues Summary { get; set; }

        //// Scheduling Summary
        //[Display(Name = "Scheduled Requests")]
        //public int ScheduledRequests { get; set; }
        //[Display(Name = "Unscheduled Requests")]
        //public int UnscheduledRequests { get; set; }

        ////Finance Summary
        //[Display(Name = "Total Cost")]
        //public decimal TotalCost { get; set; }
        //public decimal Revenue { get; set; }
        //[Display(Name = "Training Session Cost")]
        //public decimal TrainingSessionCost{ get; set; }
        //[Display(Name = "External Simulators")]
        //public decimal ExternalSimulators { get; set; }
        //[Display(Name = "External Instructors")]
        //public decimal ExternalInstructors { get; set; }
        //[Display(Name = "Instructors Cost")]
        //public decimal InstructorCost { get; set; }
        //[Display(Name = "Hotel Expenses")]
        //public decimal HotelExpenses { get; set; }
        //[Display(Name = "Travel Expenses")]
        //public decimal TravelExpenses { get; set; }

        //// Bidding Summary
        //[Display(Name = "Instructors Bidding Satifaction")]
        //public int InstructorsBiddingSatifaction { get; set; }

        private XDocument doc;

        public PlanSummaryTable()
        {
            doc = XDocument.Load(AppConfig.OptimizationSummaryXML);
            TrainingCosts = new TrainingCosts();
            TrainingValueAndRevalue = new TrainingValueAndRevalue();
            BiddingRequestsMet = new CostValues();
            BiddingRequestsMetSummary = new CostValues();
            Summary = new CostValues();
        }
        //public void CalculateSummary()
        //{
        //    this.Summary.Value = this.TrainingCosts.Summary.Value + this.TrainingValueAndRevalue.Summary.Value +
        //                         BiddingRequestsMet.Value;
        //    this.Summary.Qty = this.TrainingCosts.Summary.Qty + this.TrainingValueAndRevalue.Summary.Qty +
        //                         BiddingRequestsMet.Qty;
        //    this.Summary.Precentage = (this.TrainingCosts.Summary.Precentage + this.TrainingValueAndRevalue.Summary.Precentage +
        //                         BiddingRequestsMet.Precentage) / 3 ;
        //}

        public void LoadOptimizationPlanSummary(int PlanID)
        {
            XElement plan = doc.Element("Plans").Elements("Plan").Where(e => e.Attribute("PlanID").Value == PlanID.ToString()).FirstOrDefault();
            //Training Costs
            this.TrainingCosts.Summary.Value = Convert.ToDecimal(plan.Element("TrainingCosts").Attribute("Value").Value);
            this.TrainingCosts.Summary.Qty = Convert.ToInt16(plan.Element("TrainingCosts").Attribute("Qty").Value);
            this.TrainingCosts.Summary.Precentage = Convert.ToInt32(plan.Element("TrainingCosts").Attribute("Precentage").Value);

            //Physical Resources
            this.TrainingCosts.PhysicalResourcesTrainingCosts.Sumarry.Value = Convert.ToDecimal(plan.Element("TrainingCosts").Element("PhysicalResources").Attribute("Value").Value);
            this.TrainingCosts.PhysicalResourcesTrainingCosts.Sumarry.Qty = Convert.ToInt16(plan.Element("TrainingCosts").Element("PhysicalResources").Attribute("Qty").Value);
            this.TrainingCosts.PhysicalResourcesTrainingCosts.Sumarry.Precentage = Convert.ToInt32(plan.Element("TrainingCosts").Element("PhysicalResources").Attribute("Precentage").Value);

            this.TrainingCosts.PhysicalResourcesTrainingCosts.Simulators.Value = Convert.ToDecimal(plan.Element("TrainingCosts").Element("PhysicalResources").Element("Simulators").Value);
            this.TrainingCosts.PhysicalResourcesTrainingCosts.Simulators.Qty = Convert.ToInt16(plan.Element("TrainingCosts").Element("PhysicalResources").Element("Simulators").Attribute("Qty").Value);
            this.TrainingCosts.PhysicalResourcesTrainingCosts.Simulators.Precentage = Convert.ToInt32(plan.Element("TrainingCosts").Element("PhysicalResources").Element("Simulators").Attribute("Precentage").Value);

            this.TrainingCosts.PhysicalResourcesTrainingCosts.OtherTrainingDevices.Value = Convert.ToDecimal(plan.Element("TrainingCosts").Element("PhysicalResources").Element("OtherTrainingDevices").Value);
            this.TrainingCosts.PhysicalResourcesTrainingCosts.OtherTrainingDevices.Qty = Convert.ToInt16(plan.Element("TrainingCosts").Element("PhysicalResources").Element("OtherTrainingDevices").Attribute("Qty").Value);
            this.TrainingCosts.PhysicalResourcesTrainingCosts.OtherTrainingDevices.Precentage = Convert.ToInt32(plan.Element("TrainingCosts").Element("PhysicalResources").Element("OtherTrainingDevices").Attribute("Precentage").Value);

            this.TrainingCosts.PhysicalResourcesTrainingCosts.Classrooms.Value = Convert.ToDecimal(plan.Element("TrainingCosts").Element("PhysicalResources").Element("Classrooms").Value);
            this.TrainingCosts.PhysicalResourcesTrainingCosts.Classrooms.Qty = Convert.ToInt16(plan.Element("TrainingCosts").Element("PhysicalResources").Element("Classrooms").Attribute("Qty").Value);
            this.TrainingCosts.PhysicalResourcesTrainingCosts.Classrooms.Precentage = Convert.ToInt32(plan.Element("TrainingCosts").Element("PhysicalResources").Element("Classrooms").Attribute("Precentage").Value);
            
            //Human Resources
            this.TrainingCosts.HumanResourcesTrainingCosts.Summary.Value = Convert.ToDecimal(plan.Element("TrainingCosts").Element("HumanResources").Attribute("Value").Value);
            this.TrainingCosts.HumanResourcesTrainingCosts.Summary.Qty = Convert.ToInt16(plan.Element("TrainingCosts").Element("HumanResources").Attribute("Qty").Value);
            this.TrainingCosts.HumanResourcesTrainingCosts.Summary.Precentage = Convert.ToInt32(plan.Element("TrainingCosts").Element("HumanResources").Attribute("Precentage").Value);

            this.TrainingCosts.HumanResourcesTrainingCosts.Instrucotrs.Summary.Value = Convert.ToDecimal(plan.Element("TrainingCosts").Element("HumanResources").Element("Instructors").Attribute("Value").Value);
            this.TrainingCosts.HumanResourcesTrainingCosts.Instrucotrs.Summary.Qty = Convert.ToInt16(plan.Element("TrainingCosts").Element("HumanResources").Element("Instructors").Attribute("Qty").Value);
            this.TrainingCosts.HumanResourcesTrainingCosts.Instrucotrs.Summary.Precentage = Convert.ToInt32(plan.Element("TrainingCosts").Element("HumanResources").Element("Instructors").Attribute("Precentage").Value);

            this.TrainingCosts.HumanResourcesTrainingCosts.Instrucotrs.StandardRate.Value = Convert.ToDecimal(plan.Element("TrainingCosts").Element("HumanResources").Element("Instructors").Element("StandardRate").Value);
            this.TrainingCosts.HumanResourcesTrainingCosts.Instrucotrs.StandardRate.Qty = Convert.ToInt16(plan.Element("TrainingCosts").Element("HumanResources").Element("Instructors").Element("StandardRate").Attribute("Qty").Value);
            this.TrainingCosts.HumanResourcesTrainingCosts.Instrucotrs.StandardRate.Precentage =Convert.ToInt32(plan.Element("TrainingCosts").Element("HumanResources").Element("Instructors").Element("StandardRate").Attribute("Precentage").Value);
            
            this.TrainingCosts.HumanResourcesTrainingCosts.Instrucotrs.SpecialRate.Value = Convert.ToDecimal(plan.Element("TrainingCosts").Element("HumanResources").Element("Instructors").Element("SpecialRate").Value);
            this.TrainingCosts.HumanResourcesTrainingCosts.Instrucotrs.SpecialRate.Qty = Convert.ToInt16(plan.Element("TrainingCosts").Element("HumanResources").Element("Instructors").Element("SpecialRate").Attribute("Qty").Value);
            this.TrainingCosts.HumanResourcesTrainingCosts.Instrucotrs.SpecialRate.Precentage = Convert.ToInt32(plan.Element("TrainingCosts").Element("HumanResources").Element("Instructors").Element("SpecialRate").Attribute("Precentage").Value);

            this.TrainingCosts.HumanResourcesTrainingCosts.Evaluators.Summary.Value = Convert.ToDecimal(plan.Element("TrainingCosts").Element("HumanResources").Element("Evaluators").Attribute("Value").Value);
            this.TrainingCosts.HumanResourcesTrainingCosts.Evaluators.Summary.Qty = Convert.ToInt16(plan.Element("TrainingCosts").Element("HumanResources").Element("Evaluators").Attribute("Qty").Value);
            this.TrainingCosts.HumanResourcesTrainingCosts.Evaluators.Summary.Precentage = Convert.ToInt32(plan.Element("TrainingCosts").Element("HumanResources").Element("Evaluators").Attribute("Precentage").Value);

            this.TrainingCosts.HumanResourcesTrainingCosts.Evaluators.StandardRate.Value = Convert.ToDecimal(plan.Element("TrainingCosts").Element("HumanResources").Element("Evaluators").Element("StandardRate").Value);
            this.TrainingCosts.HumanResourcesTrainingCosts.Evaluators.StandardRate.Qty = Convert.ToInt16(plan.Element("TrainingCosts").Element("HumanResources").Element("Evaluators").Element("StandardRate").Attribute("Qty").Value);
            this.TrainingCosts.HumanResourcesTrainingCosts.Evaluators.StandardRate.Precentage = Convert.ToInt32(plan.Element("TrainingCosts").Element("HumanResources").Element("Evaluators").Element("StandardRate").Attribute("Precentage").Value);

            this.TrainingCosts.HumanResourcesTrainingCosts.Evaluators.SpecialRate.Value = Convert.ToDecimal(plan.Element("TrainingCosts").Element("HumanResources").Element("Evaluators").Element("SpecialRate").Value);
            this.TrainingCosts.HumanResourcesTrainingCosts.Evaluators.SpecialRate.Qty = Convert.ToInt16(plan.Element("TrainingCosts").Element("HumanResources").Element("Evaluators").Element("SpecialRate").Attribute("Qty").Value);
            this.TrainingCosts.HumanResourcesTrainingCosts.Evaluators.SpecialRate.Precentage = Convert.ToInt32(plan.Element("TrainingCosts").Element("HumanResources").Element("Evaluators").Element("SpecialRate").Attribute("Precentage").Value);

            // Trainees
            this.TrainingCosts.TraineesTrainingCosts.Summary.Value = Convert.ToDecimal(plan.Element("TrainingCosts").Element("Trainees").Attribute("Value").Value);
            this.TrainingCosts.TraineesTrainingCosts.Summary.Qty = Convert.ToInt16(plan.Element("TrainingCosts").Element("Trainees").Attribute("Qty").Value);
            this.TrainingCosts.TraineesTrainingCosts.Summary.Precentage = Convert.ToInt32(plan.Element("TrainingCosts").Element("Trainees").Attribute("Precentage").Value);

            this.TrainingCosts.TraineesTrainingCosts.Pilots.Summary.Value = Convert.ToDecimal(plan.Element("TrainingCosts").Element("Trainees").Attribute("Value").Value);
            this.TrainingCosts.TraineesTrainingCosts.Pilots.Summary.Qty = Convert.ToInt16(plan.Element("TrainingCosts").Element("Trainees").Attribute("Qty").Value);
            this.TrainingCosts.TraineesTrainingCosts.Pilots.Summary.Precentage = Convert.ToInt32(plan.Element("TrainingCosts").Element("Trainees").Attribute("Precentage").Value);

            this.TrainingCosts.TraineesTrainingCosts.Pilots.StandardRate.Value =Convert.ToDecimal(plan.Element("TrainingCosts").Element("Trainees").Element("SpecialRate").Value);
            this.TrainingCosts.TraineesTrainingCosts.Pilots.StandardRate.Qty =Convert.ToInt16(plan.Element("TrainingCosts").Element("Trainees").Element("SpecialRate").Attribute("Qty").Value);
            this.TrainingCosts.TraineesTrainingCosts.Pilots.StandardRate.Precentage =Convert.ToInt32(plan.Element("TrainingCosts").Element("Trainees").Element("SpecialRate").Attribute("Precentage").Value);

            this.TrainingCosts.TraineesTrainingCosts.Pilots.SpecialRate.Value =Convert.ToDecimal(plan.Element("TrainingCosts").Element("Trainees").Element("SpecialRate").Value);
            this.TrainingCosts.TraineesTrainingCosts.Pilots.SpecialRate.Qty =Convert.ToInt16(plan.Element("TrainingCosts").Element("Trainees").Element("SpecialRate").Attribute("Qty").Value);
            this.TrainingCosts.TraineesTrainingCosts.Pilots.SpecialRate.Precentage =Convert.ToInt32(plan.Element("TrainingCosts").Element("Trainees").Element("SpecialRate").Attribute("Precentage").Value);

            // TravelAndLivingCosts
            this.TrainingCosts.TravelAndLivingCosts.Summary.Value = Convert.ToDecimal(plan.Element("TrainingCosts").Element("TravelAndLivingCosts").Attribute("Value").Value);
            this.TrainingCosts.TravelAndLivingCosts.Summary.Qty = Convert.ToInt16(plan.Element("TrainingCosts").Element("TravelAndLivingCosts").Attribute("Qty").Value);
            this.TrainingCosts.TravelAndLivingCosts.Summary.Precentage = Convert.ToInt32(plan.Element("TrainingCosts").Element("TravelAndLivingCosts").Attribute("Precentage").Value);

            //Flights
            this.TrainingCosts.TravelAndLivingCosts.Summary.Value = Convert.ToDecimal(plan.Element("TrainingCosts").Element("TravelAndLivingCosts").Element("Flights").Attribute("Value").Value);
            this.TrainingCosts.TravelAndLivingCosts.Summary.Qty = Convert.ToInt16(plan.Element("TrainingCosts").Element("TravelAndLivingCosts").Element("Flights").Attribute("Qty").Value);
            this.TrainingCosts.TravelAndLivingCosts.Summary.Precentage = Convert.ToInt32(plan.Element("TrainingCosts").Element("TravelAndLivingCosts").Element("Flights").Attribute("Precentage").Value);

            this.TrainingCosts.TravelAndLivingCosts.Instructors.Summary.Value = Convert.ToDecimal(plan.Element("TrainingCosts").Element("TravelAndLivingCosts").Element("Flights").Element("Instructors").Attribute("Value").Value);
            this.TrainingCosts.TravelAndLivingCosts.Instructors.Summary.Qty = Convert.ToInt16(plan.Element("TrainingCosts").Element("TravelAndLivingCosts").Element("Flights").Element("Instructors").Attribute("Qty").Value);
            this.TrainingCosts.TravelAndLivingCosts.Instructors.Summary.Precentage = Convert.ToInt32(plan.Element("TrainingCosts").Element("TravelAndLivingCosts").Element("Flights").Element("Instructors").Attribute("Precentage").Value);

            this.TrainingCosts.TravelAndLivingCosts.Instructors.Planned.Value = Convert.ToDecimal(Convert.ToInt32(plan.Element("TrainingCosts").Element("TravelAndLivingCosts").Element("Flights").Element("Instructors").Element("Planned").Value));
            this.TrainingCosts.TravelAndLivingCosts.Instructors.Planned.Qty = Convert.ToInt16(plan.Element("TrainingCosts").Element("TravelAndLivingCosts").Element("Flights").Element("Instructors").Element("Planned").Attribute("Qty").Value);
            this.TrainingCosts.TravelAndLivingCosts.Instructors.Planned.Precentage = Convert.ToInt32(plan.Element("TrainingCosts").Element("TravelAndLivingCosts").Element("Flights").Element("Instructors").Element("Planned").Attribute("Precentage").Value);

            this.TrainingCosts.TravelAndLivingCosts.Instructors.Dedicated.Value = Convert.ToDecimal(plan.Element("TrainingCosts").Element("TravelAndLivingCosts").Element("Flights").Element("Instructors").Element("Dedicated").Value);
            this.TrainingCosts.TravelAndLivingCosts.Instructors.Dedicated.Qty = Convert.ToInt16(plan.Element("TrainingCosts").Element("TravelAndLivingCosts").Element("Flights").Element("Instructors").Element("Dedicated").Attribute("Qty").Value);
            this.TrainingCosts.TravelAndLivingCosts.Instructors.Dedicated.Precentage =Convert.ToInt32(plan.Element("TrainingCosts").Element("TravelAndLivingCosts").Element("Flights").Element("Instructors").Element("Dedicated").Attribute("Precentage").Value);

            this.TrainingCosts.TravelAndLivingCosts.Evaluators.Summary.Value = Convert.ToDecimal(plan.Element("TrainingCosts").Element("TravelAndLivingCosts").Element("Flights").Element("Evaluators").Attribute("Value").Value);
            this.TrainingCosts.TravelAndLivingCosts.Evaluators.Summary.Qty = Convert.ToInt16(plan.Element("TrainingCosts").Element("TravelAndLivingCosts").Element("Flights").Element("Evaluators").Attribute("Qty").Value);
            this.TrainingCosts.TravelAndLivingCosts.Evaluators.Summary.Precentage = Convert.ToInt32(plan.Element("TrainingCosts").Element("TravelAndLivingCosts").Element("Flights").Element("Evaluators").Attribute("Precentage").Value);
            
            this.TrainingCosts.TravelAndLivingCosts.Evaluators.Planned.Value = Convert.ToDecimal(plan.Element("TrainingCosts").Element("TravelAndLivingCosts").Element("Flights").Element("Evaluators").Element("Planned").Value);
            this.TrainingCosts.TravelAndLivingCosts.Evaluators.Planned.Qty = Convert.ToInt16(plan.Element("TrainingCosts").Element("TravelAndLivingCosts").Element("Flights").Element("Evaluators").Element("Planned").Attribute("Qty").Value);
            this.TrainingCosts.TravelAndLivingCosts.Evaluators.Planned.Precentage = Convert.ToInt32(plan.Element("TrainingCosts").Element("TravelAndLivingCosts").Element("Flights").Element("Evaluators").Element("Planned").Attribute("Precentage").Value);

            this.TrainingCosts.TravelAndLivingCosts.Evaluators.Dedicated.Value = Convert.ToDecimal(plan.Element("TrainingCosts").Element("TravelAndLivingCosts").Element("Flights").Element("Evaluators").Element("Dedicated").Value);
            this.TrainingCosts.TravelAndLivingCosts.Evaluators.Dedicated.Qty =Convert.ToInt16(plan.Element("TrainingCosts").Element("TravelAndLivingCosts").Element("Flights").Element("Evaluators").Element("Dedicated").Attribute("Qty").Value);
            this.TrainingCosts.TravelAndLivingCosts.Evaluators.Dedicated.Precentage =Convert.ToInt32(plan.Element("TrainingCosts").Element("TravelAndLivingCosts").Element("Flights").Element("Evaluators").Element("Dedicated").Attribute("Precentage").Value);

            this.TrainingCosts.TravelAndLivingCosts.Pilots.Summary.Value = Convert.ToDecimal(plan.Element("TrainingCosts").Element("TravelAndLivingCosts").Element("Flights").Element("Pilots").Attribute("Value").Value);
            this.TrainingCosts.TravelAndLivingCosts.Pilots.Summary.Qty = Convert.ToInt16(plan.Element("TrainingCosts").Element("TravelAndLivingCosts").Element("Flights").Element("Pilots").Attribute("Qty").Value);
            this.TrainingCosts.TravelAndLivingCosts.Pilots.Summary.Precentage = Convert.ToInt32(plan.Element("TrainingCosts").Element("TravelAndLivingCosts").Element("Flights").Element("Pilots").Attribute("Precentage").Value);

            this.TrainingCosts.TravelAndLivingCosts.Pilots.Planned.Value = Convert.ToDecimal(plan.Element("TrainingCosts").Element("TravelAndLivingCosts").Element("Flights").Element("Pilots").Element("Planned").Value);
            this.TrainingCosts.TravelAndLivingCosts.Pilots.Planned.Qty = Convert.ToInt16(plan.Element("TrainingCosts").Element("TravelAndLivingCosts").Element("Flights").Element("Pilots").Element("Planned").Attribute("Qty").Value);
            this.TrainingCosts.TravelAndLivingCosts.Pilots.Planned.Precentage = Convert.ToInt32(plan.Element("TrainingCosts").Element("TravelAndLivingCosts").Element("Flights").Element("Pilots").Element("Planned").Attribute("Precentage").Value);

            this.TrainingCosts.TravelAndLivingCosts.Pilots.Dedicated.Value = Convert.ToDecimal(plan.Element("TrainingCosts").Element("TravelAndLivingCosts").Element("Flights").Element("Pilots").Element("Dedicated").Value);
            this.TrainingCosts.TravelAndLivingCosts.Pilots.Dedicated.Qty =Convert.ToInt16(plan.Element("TrainingCosts").Element("TravelAndLivingCosts").Element("Flights").Element("Pilots").Element("Dedicated").Attribute("Qty").Value);
            this.TrainingCosts.TravelAndLivingCosts.Pilots.Dedicated.Precentage =Convert.ToInt32(plan.Element("TrainingCosts").Element("TravelAndLivingCosts").Element("Flights").Element("Pilots").Element("Dedicated").Attribute("Precentage").Value);

            // Hotels
            this.TrainingCosts.Hotels.Summary.Value = Convert.ToDecimal(plan.Element("TrainingCosts").Element("TravelAndLivingCosts").Element("Hotels").Attribute("Value").Value);
            this.TrainingCosts.Hotels.Summary.Qty = Convert.ToInt16(plan.Element("TrainingCosts").Element("TravelAndLivingCosts").Element("Hotels").Attribute("Qty").Value);
            this.TrainingCosts.Hotels.Summary.Precentage = Convert.ToInt32(plan.Element("TrainingCosts").Element("TravelAndLivingCosts").Element("Hotels").Attribute("Precentage").Value);

            this.TrainingCosts.Hotels.Instructors.Summary.Value = Convert.ToDecimal(plan.Element("TrainingCosts").Element("TravelAndLivingCosts").Element("Hotels").Element("Instructors").Attribute("Value").Value);
            this.TrainingCosts.Hotels.Instructors.Summary.Qty = Convert.ToInt16(plan.Element("TrainingCosts").Element("TravelAndLivingCosts").Element("Hotels").Element("Instructors").Attribute("Qty").Value);
            this.TrainingCosts.Hotels.Instructors.Summary.Precentage = Convert.ToInt32(plan.Element("TrainingCosts").Element("TravelAndLivingCosts").Element("Hotels").Element("Instructors").Attribute("Precentage").Value);

            this.TrainingCosts.Hotels.Instructors.Requirednights.Value = Convert.ToDecimal(plan.Element("TrainingCosts").Element("TravelAndLivingCosts").Element("Hotels").Element("Instructors").Element("RequiredNights").Value);
            this.TrainingCosts.Hotels.Instructors.Requirednights.Qty = Convert.ToInt16(plan.Element("TrainingCosts").Element("TravelAndLivingCosts").Element("Hotels").Element("Instructors").Element("RequiredNights").Attribute("Qty").Value);
            this.TrainingCosts.Hotels.Instructors.Requirednights.Precentage = Convert.ToInt32(plan.Element("TrainingCosts").Element("TravelAndLivingCosts").Element("Hotels").Element("Instructors").Element("RequiredNights").Attribute("Precentage").Value);

            this.TrainingCosts.Hotels.Instructors.Forcednights.Value = Convert.ToDecimal(plan.Element("TrainingCosts").Element("TravelAndLivingCosts").Element("Hotels").Element("Instructors").Element("ForcedNights").Value);
            this.TrainingCosts.Hotels.Instructors.Forcednights.Qty = Convert.ToInt16(plan.Element("TrainingCosts").Element("TravelAndLivingCosts").Element("Hotels").Element("Instructors").Element("ForcedNights").Attribute("Qty").Value);
            this.TrainingCosts.Hotels.Instructors.Forcednights.Precentage = Convert.ToInt32(plan.Element("TrainingCosts").Element("TravelAndLivingCosts").Element("Hotels").Element("Instructors").Element("ForcedNights").Attribute("Precentage").Value);

            this.TrainingCosts.Hotels.Evaluators.Summary.Value = Convert.ToDecimal(plan.Element("TrainingCosts").Element("TravelAndLivingCosts").Element("Hotels").Element("Evaluators").Attribute("Value").Value);
            this.TrainingCosts.Hotels.Evaluators.Summary.Qty = Convert.ToInt16(plan.Element("TrainingCosts").Element("TravelAndLivingCosts").Element("Hotels").Element("Evaluators").Attribute("Qty").Value);
            this.TrainingCosts.Hotels.Evaluators.Summary.Precentage = Convert.ToInt32(plan.Element("TrainingCosts").Element("TravelAndLivingCosts").Element("Hotels").Element("Evaluators").Attribute("Precentage").Value);

            this.TrainingCosts.Hotels.Evaluators.Requirednights.Value = Convert.ToInt16(plan.Element("TrainingCosts").Element("TravelAndLivingCosts").Element("Hotels").Element("Evaluators").Element("RequiredNights").Value);
            this.TrainingCosts.Hotels.Evaluators.Requirednights.Qty = Convert.ToInt16(plan.Element("TrainingCosts").Element("TravelAndLivingCosts").Element("Hotels").Element("Evaluators").Element("RequiredNights").Attribute("Qty").Value);
            this.TrainingCosts.Hotels.Evaluators.Requirednights.Precentage = Convert.ToInt32(plan.Element("TrainingCosts").Element("TravelAndLivingCosts").Element("Hotels").Element("Evaluators").Element("RequiredNights").Attribute("Precentage").Value);

            this.TrainingCosts.Hotels.Evaluators.Forcednights.Value = Convert.ToInt16(plan.Element("TrainingCosts").Element("TravelAndLivingCosts").Element("Hotels").Element("Evaluators").Element("ForcedNights").Value);
            this.TrainingCosts.Hotels.Evaluators.Forcednights.Qty = Convert.ToInt16(plan.Element("TrainingCosts").Element("TravelAndLivingCosts").Element("Hotels").Element("Evaluators").Element("ForcedNights").Attribute("Qty").Value);
            this.TrainingCosts.Hotels.Evaluators.Forcednights.Precentage = Convert.ToInt32(plan.Element("TrainingCosts").Element("TravelAndLivingCosts").Element("Hotels").Element("Evaluators").Element("ForcedNights").Attribute("Precentage").Value);

            this.TrainingCosts.Hotels.Pilots.Summary.Value = Convert.ToDecimal(plan.Element("TrainingCosts").Element("TravelAndLivingCosts").Element("Hotels").Element("Pilots").Attribute("Value").Value);
            this.TrainingCosts.Hotels.Pilots.Summary.Qty = Convert.ToInt16(plan.Element("TrainingCosts").Element("TravelAndLivingCosts").Element("Hotels").Element("Pilots").Attribute("Qty").Value);
            this.TrainingCosts.Hotels.Pilots.Summary.Precentage = Convert.ToInt32(plan.Element("TrainingCosts").Element("TravelAndLivingCosts").Element("Hotels").Element("Pilots").Attribute("Precentage").Value);

            this.TrainingCosts.Hotels.Pilots.Requirednights.Value = Convert.ToDecimal(plan.Element("TrainingCosts").Element("TravelAndLivingCosts").Element("Hotels").Element("Pilots").Element("RequiredNights").Value);
            this.TrainingCosts.Hotels.Pilots.Requirednights.Qty = Convert.ToInt16(plan.Element("TrainingCosts").Element("TravelAndLivingCosts").Element("Hotels").Element("Pilots").Element("RequiredNights").Attribute("Qty").Value);
            this.TrainingCosts.Hotels.Pilots.Requirednights.Precentage = Convert.ToInt32(plan.Element("TrainingCosts").Element("TravelAndLivingCosts").Element("Hotels").Element("Pilots").Element("RequiredNights").Attribute("Precentage").Value);

            this.TrainingCosts.Hotels.Pilots.Forcednights.Value = Convert.ToDecimal(plan.Element("TrainingCosts").Element("TravelAndLivingCosts").Element("Hotels").Element("Pilots").Element("ForcedNights").Value);
            this.TrainingCosts.Hotels.Pilots.Forcednights.Qty = Convert.ToInt16(plan.Element("TrainingCosts").Element("TravelAndLivingCosts").Element("Hotels").Element("Pilots").Element("ForcedNights").Attribute("Qty").Value);
            this.TrainingCosts.Hotels.Pilots.Forcednights.Precentage = Convert.ToInt32(plan.Element("TrainingCosts").Element("TravelAndLivingCosts").Element("Hotels").Element("Pilots").Element("ForcedNights").Attribute("Precentage").Value);

            // OtherCosts
            this.TrainingCosts.OtherCosts.Summary.Value = Convert.ToDecimal(plan.Element("TrainingCosts").Element("OtherCosts").Attribute("Value").Value);
            this.TrainingCosts.OtherCosts.Summary.Qty = Convert.ToInt16(plan.Element("TrainingCosts").Element("OtherCosts").Attribute("Qty").Value);
            this.TrainingCosts.OtherCosts.Summary.Precentage = Convert.ToInt32(plan.Element("TrainingCosts").Element("OtherCosts").Attribute("Precentage").Value);

            this.TrainingCosts.OtherCosts.SimulatorSetup.Value = Convert.ToDecimal(plan.Element("TrainingCosts").Element("OtherCosts").Element("SimulatorSetup").Value);
            this.TrainingCosts.OtherCosts.SimulatorSetup.Qty = Convert.ToInt16(plan.Element("TrainingCosts").Element("OtherCosts").Element("SimulatorSetup").Attribute("Qty").Value);
            this.TrainingCosts.OtherCosts.SimulatorSetup.Precentage = Convert.ToInt32(plan.Element("TrainingCosts").Element("OtherCosts").Element("SimulatorSetup").Attribute("Precentage").Value);

            this.TrainingCosts.OtherCosts.Maintenance.Value = Convert.ToDecimal(plan.Element("TrainingCosts").Element("OtherCosts").Element("Maintenance").Value);
            this.TrainingCosts.OtherCosts.Maintenance.Qty = Convert.ToInt16(plan.Element("TrainingCosts").Element("OtherCosts").Element("Maintenance").Attribute("Qty").Value);
            this.TrainingCosts.OtherCosts.Maintenance.Precentage = Convert.ToInt32(plan.Element("TrainingCosts").Element("OtherCosts").Element("Maintenance").Attribute("Precentage").Value);

            this.TrainingCosts.OtherCosts.SimulatorIdleTtime.Value = Convert.ToDecimal(plan.Element("TrainingCosts").Element("OtherCosts").Element("SimulatorIdleTime").Value);
            this.TrainingCosts.OtherCosts.SimulatorIdleTtime.Qty = Convert.ToInt16(plan.Element("TrainingCosts").Element("OtherCosts").Element("SimulatorIdleTime").Attribute("Qty").Value);
            this.TrainingCosts.OtherCosts.SimulatorIdleTtime.Precentage = Convert.ToInt32(plan.Element("TrainingCosts").Element("OtherCosts").Element("SimulatorIdleTime").Attribute("Precentage").Value);

            // TrainingValueAndRevalue
            this.TrainingValueAndRevalue.Summary.Value = Convert.ToDecimal(plan.Element("TrainingValueAndRevenue").Attribute("Value").Value);
            this.TrainingValueAndRevalue.Summary.Qty = Convert.ToInt16(plan.Element("TrainingValueAndRevenue").Attribute("Qty").Value);
            this.TrainingValueAndRevalue.Summary.Precentage = Convert.ToInt32(plan.Element("TrainingValueAndRevenue").Attribute("Precentage").Value);

            // Physical Resource
            this.TrainingValueAndRevalue.PhysicalResources.Summary.Value = Convert.ToDecimal(plan.Element("TrainingValueAndRevenue").Element("PhysicalResources").Attribute("Value").Value);
            this.TrainingValueAndRevalue.PhysicalResources.Summary.Qty = Convert.ToInt16(plan.Element("TrainingValueAndRevenue").Element("PhysicalResources").Attribute("Qty").Value);
            this.TrainingValueAndRevalue.PhysicalResources.Summary.Precentage = Convert.ToInt32(plan.Element("TrainingValueAndRevenue").Element("PhysicalResources").Attribute("Precentage").Value);

            this.TrainingValueAndRevalue.PhysicalResources.Simulator.Value = Convert.ToDecimal(plan.Element("TrainingValueAndRevenue").Element("PhysicalResources").Element("Simulators").Value);
            this.TrainingValueAndRevalue.PhysicalResources.Simulator.Qty = Convert.ToInt16(plan.Element("TrainingValueAndRevenue").Element("PhysicalResources").Element("Simulators").Attribute("Qty").Value);
            this.TrainingValueAndRevalue.PhysicalResources.Simulator.Precentage = Convert.ToInt32(plan.Element("TrainingValueAndRevenue").Element("PhysicalResources").Element("Simulators").Attribute("Precentage").Value);

            this.TrainingValueAndRevalue.PhysicalResources.OtherTrainingDevices.Value = Convert.ToDecimal(plan.Element("TrainingValueAndRevenue").Element("PhysicalResources").Element("OtherTrainingDevices").Value);
            this.TrainingValueAndRevalue.PhysicalResources.OtherTrainingDevices.Qty = Convert.ToInt16(plan.Element("TrainingValueAndRevenue").Element("PhysicalResources").Element("OtherTrainingDevices").Attribute("Qty").Value);
            this.TrainingValueAndRevalue.PhysicalResources.OtherTrainingDevices.Precentage = Convert.ToInt32(plan.Element("TrainingValueAndRevenue").Element("PhysicalResources").Element("OtherTrainingDevices").Attribute("Precentage").Value);

            this.TrainingValueAndRevalue.PhysicalResources.Classrooms.Value = Convert.ToDecimal(plan.Element("TrainingValueAndRevenue").Element("PhysicalResources").Element("Classrooms").Value);
            this.TrainingValueAndRevalue.PhysicalResources.Classrooms.Qty = Convert.ToInt16(plan.Element("TrainingValueAndRevenue").Element("PhysicalResources").Element("Classrooms").Attribute("Qty").Value);
            this.TrainingValueAndRevalue.PhysicalResources.Classrooms.Precentage = Convert.ToInt32(plan.Element("TrainingValueAndRevenue").Element("PhysicalResources").Element("Classrooms").Attribute("Precentage").Value);

            // HumanResources
            this.TrainingValueAndRevalue.HumanResources.Summary.Value = Convert.ToDecimal(plan.Element("TrainingValueAndRevenue").Element("HumanResources").Attribute("Value").Value);
            this.TrainingValueAndRevalue.HumanResources.Summary.Qty = Convert.ToInt16(plan.Element("TrainingValueAndRevenue").Element("HumanResources").Attribute("Qty").Value);
            this.TrainingValueAndRevalue.HumanResources.Summary.Precentage = Convert.ToInt32(plan.Element("TrainingValueAndRevenue").Element("HumanResources").Attribute("Precentage").Value);

            this.TrainingValueAndRevalue.HumanResources.Instrctors.Summary.Value = Convert.ToDecimal(plan.Element("TrainingValueAndRevenue").Element("HumanResources").Element("Instructors").Attribute("Value").Value);
            this.TrainingValueAndRevalue.HumanResources.Instrctors.Summary.Qty = Convert.ToInt16(plan.Element("TrainingValueAndRevenue").Element("HumanResources").Element("Instructors").Attribute("Qty").Value);
            this.TrainingValueAndRevalue.HumanResources.Instrctors.Summary.Precentage = Convert.ToInt32(plan.Element("TrainingValueAndRevenue").Element("HumanResources").Element("Instructors").Attribute("Precentage").Value);

            this.TrainingValueAndRevalue.HumanResources.Instrctors.StandardRate.Value = Convert.ToDecimal(plan.Element("TrainingValueAndRevenue").Element("HumanResources").Element("Instructors").Element("StandardRate").Value);
            this.TrainingValueAndRevalue.HumanResources.Instrctors.StandardRate.Qty = Convert.ToInt16(plan.Element("TrainingValueAndRevenue").Element("HumanResources").Element("Instructors").Element("StandardRate").Attribute("Qty").Value);
            this.TrainingValueAndRevalue.HumanResources.Instrctors.StandardRate.Precentage = Convert.ToInt32(plan.Element("TrainingValueAndRevenue").Element("HumanResources").Element("Instructors").Element("StandardRate").Attribute("Precentage").Value);

            this.TrainingValueAndRevalue.HumanResources.Instrctors.SpecialRate.Value = Convert.ToDecimal(plan.Element("TrainingValueAndRevenue").Element("HumanResources").Element("Instructors").Element("SpecialRate").Value);
            this.TrainingValueAndRevalue.HumanResources.Instrctors.SpecialRate.Qty = Convert.ToInt16(plan.Element("TrainingValueAndRevenue").Element("HumanResources").Element("Instructors").Element("SpecialRate").Attribute("Qty").Value);
            this.TrainingValueAndRevalue.HumanResources.Instrctors.SpecialRate.Precentage = Convert.ToInt32(plan.Element("TrainingValueAndRevenue").Element("HumanResources").Element("Instructors").Element("SpecialRate").Attribute("Precentage").Value);

            this.TrainingValueAndRevalue.HumanResources.Evaluators.Summary.Value = Convert.ToDecimal(plan.Element("TrainingValueAndRevenue").Element("HumanResources").Element("Evaluators").Attribute("Value").Value);
            this.TrainingValueAndRevalue.HumanResources.Evaluators.Summary.Qty = Convert.ToInt16(plan.Element("TrainingValueAndRevenue").Element("HumanResources").Element("Evaluators").Attribute("Qty").Value);
            this.TrainingValueAndRevalue.HumanResources.Evaluators.Summary.Precentage = Convert.ToInt32(plan.Element("TrainingValueAndRevenue").Element("HumanResources").Element("Evaluators").Attribute("Precentage").Value);

            this.TrainingValueAndRevalue.HumanResources.Evaluators.StandardRate.Value = Convert.ToDecimal(plan.Element("TrainingValueAndRevenue").Element("HumanResources").Element("Evaluators").Element("StandardRate").Value);
            this.TrainingValueAndRevalue.HumanResources.Evaluators.StandardRate.Qty = Convert.ToInt16(plan.Element("TrainingValueAndRevenue").Element("HumanResources").Element("Evaluators").Element("StandardRate").Attribute("Qty").Value);
            this.TrainingValueAndRevalue.HumanResources.Evaluators.StandardRate.Precentage = Convert.ToInt32(plan.Element("TrainingValueAndRevenue").Element("HumanResources").Element("Evaluators").Element("StandardRate").Attribute("Precentage").Value);

            this.TrainingValueAndRevalue.HumanResources.Evaluators.SpecialRate.Value = Convert.ToDecimal(plan.Element("TrainingValueAndRevenue").Element("HumanResources").Element("Evaluators").Element("SpecialRate").Value);
            this.TrainingValueAndRevalue.HumanResources.Evaluators.SpecialRate.Qty = Convert.ToInt16(plan.Element("TrainingValueAndRevenue").Element("HumanResources").Element("Evaluators").Element("SpecialRate").Attribute("Qty").Value);
            this.TrainingValueAndRevalue.HumanResources.Evaluators.SpecialRate.Precentage = Convert.ToInt32(plan.Element("TrainingValueAndRevenue").Element("HumanResources").Element("Evaluators").Element("SpecialRate").Attribute("Precentage").Value);

            //Trainees
            this.TrainingValueAndRevalue.TraineesRevenue.Summary.Value = Convert.ToDecimal(plan.Element("TrainingValueAndRevenue").Element("Trainees").Attribute("Value").Value);
            this.TrainingValueAndRevalue.TraineesRevenue.Summary.Qty = Convert.ToInt16(plan.Element("TrainingValueAndRevenue").Element("Trainees").Attribute("Qty").Value);
            this.TrainingValueAndRevalue.TraineesRevenue.Summary.Precentage = Convert.ToInt32(plan.Element("TrainingValueAndRevenue").Element("Trainees").Attribute("Precentage").Value);

            this.TrainingValueAndRevalue.TraineesRevenue.PilotsContinuingQualifications.Value = Convert.ToDecimal(plan.Element("TrainingValueAndRevenue").Element("Trainees").Element("PilotsContinuingQualification").Value);
            this.TrainingValueAndRevalue.TraineesRevenue.PilotsContinuingQualifications.Qty = Convert.ToInt16(plan.Element("TrainingValueAndRevenue").Element("Trainees").Element("PilotsContinuingQualification").Attribute("Qty").Value);
            this.TrainingValueAndRevalue.TraineesRevenue.PilotsContinuingQualifications.Precentage = Convert.ToInt32(plan.Element("TrainingValueAndRevenue").Element("Trainees").Element("PilotsContinuingQualification").Attribute("Precentage").Value);

            this.TrainingValueAndRevalue.TraineesRevenue.PilotsQualificationTraining.Value = Convert.ToDecimal(plan.Element("TrainingValueAndRevenue").Element("Trainees").Element("PilotsQualificationTraining").Value);
            this.TrainingValueAndRevalue.TraineesRevenue.PilotsQualificationTraining.Qty = Convert.ToInt16(plan.Element("TrainingValueAndRevenue").Element("Trainees").Element("PilotsQualificationTraining").Attribute("Qty").Value);
            this.TrainingValueAndRevalue.TraineesRevenue.PilotsQualificationTraining.Precentage = Convert.ToInt32(plan.Element("TrainingValueAndRevenue").Element("Trainees").Element("PilotsQualificationTraining").Attribute("Precentage").Value);

            // Bidding
            this.TrainingValueAndRevalue.Summary.Value = Convert.ToDecimal(plan.Element("TrainingValueAndRevenue").Attribute("Value").Value);
            this.TrainingValueAndRevalue.Summary.Qty = Convert.ToInt16(plan.Element("TrainingValueAndRevenue").Attribute("Qty").Value);
            this.TrainingValueAndRevalue.Summary.Precentage = Convert.ToInt32(plan.Element("TrainingValueAndRevenue").Attribute("Precentage").Value);

            // BiddingRequestsMet
            this.BiddingRequestsMetSummary.Value = Convert.ToDecimal(plan.Element("Bidding").Attribute("Value").Value);
            this.BiddingRequestsMetSummary.Qty = Convert.ToInt16(plan.Element("Bidding").Attribute("Qty").Value);
            this.BiddingRequestsMetSummary.Precentage = Convert.ToInt32(plan.Element("Bidding").Element("BiddingRequestsMet").Attribute("Precentage").Value);

            this.BiddingRequestsMet.Value = Convert.ToInt16(plan.Element("Bidding").Element("BiddingRequestsMet").Value);
            this.BiddingRequestsMet.Qty = Convert.ToInt16(plan.Element("Bidding").Element("BiddingRequestsMet").Attribute("Qty").Value);
            this.BiddingRequestsMet.Precentage = Convert.ToInt32(plan.Element("Bidding").Element("BiddingRequestsMet").Attribute("Precentage").Value);
        }
    }

    public class CostValues
    {
        [DisplayFormat(DataFormatString = "{0:c0}")]
        public decimal Value { get; set; }
        public int Qty { get; set; }
        public int Precentage { get; set; }
    }

    public class TrainingCosts
    {
        public PhysicalResourcesTrainingCosts PhysicalResourcesTrainingCosts { get; set; }
        public HumanResourcesTrainingCosts HumanResourcesTrainingCosts { get; set; }
        public TraineesTrainingCosts TraineesTrainingCosts { get; set; }
        public TravelAndLivingCosts TravelAndLivingCosts { get; set; }
        public Hotels Hotels { get; set; }
        public OtherCosts OtherCosts { get; set; }

        public CostValues Summary { get; set; }
        public CostValues Flight { get; set; }

        public TrainingCosts()
        {
            PhysicalResourcesTrainingCosts = new PhysicalResourcesTrainingCosts();
            HumanResourcesTrainingCosts = new HumanResourcesTrainingCosts();
            TraineesTrainingCosts = new TraineesTrainingCosts();
            TravelAndLivingCosts = new TravelAndLivingCosts();
            Hotels = new Hotels();
            OtherCosts = new OtherCosts();

            Summary = new CostValues();
            Flight = new CostValues();
        }

        //public void CalculateSummary()
        //{
        //    Summary.Value = this.PhysicalResourcesTrainingCosts.Sumarry.Value +
        //                    HumanResourcesTrainingCosts.Summary.Value + TraineesTrainingCosts.Summary.Value +
        //                    TravelAndLivingCosts.Summary.Value + Hotels.Summary.Value + OtherCosts.Summary.Value;

        //    Summary.Qty = this.PhysicalResourcesTrainingCosts.Sumarry.Qty +
        //                    HumanResourcesTrainingCosts.Summary.Qty + TraineesTrainingCosts.Summary.Qty +
        //                    TravelAndLivingCosts.Summary.Qty + Hotels.Summary.Qty + OtherCosts.Summary.Qty; 

        //    Summary.Precentage = (this.PhysicalResourcesTrainingCosts.Sumarry.Precentage +
        //                    HumanResourcesTrainingCosts.Summary.Precentage + TraineesTrainingCosts.Summary.Precentage +
        //                    TravelAndLivingCosts.Summary.Precentage + Hotels.Summary.Precentage + OtherCosts.Summary.Precentage) / 6 ;

        //    Flight.Value = TravelAndLivingCosts.Summary.Value + Hotels.Summary.Value;
        //    Flight.Qty = TravelAndLivingCosts.Summary.Qty + Hotels.Summary.Qty;
        //    Flight.Value = TravelAndLivingCosts.Summary.Value + Hotels.Summary.Value / 2;
        //}
    }

    public class PhysicalResourcesTrainingCosts
    {
        public CostValues Simulators { get; set; }
        public CostValues OtherTrainingDevices { get; set; }
        public CostValues Classrooms { get; set; }

        public CostValues Sumarry { get; set; }

        public string Unit { get; set; }

        public PhysicalResourcesTrainingCosts()
        {
            Unit = "Hours";
            Simulators = new CostValues();
            OtherTrainingDevices = new CostValues();
            Classrooms = new CostValues();
            Sumarry = new CostValues();
        }


        //public void CalculateSumarry()
        //{
        //    this.Sumarry.Value = this.Simulators.Value + this.OtherTrainingDevices.Value + this.Classrooms.Value;
        //    this.Sumarry.Qty = this.Simulators.Qty + this.OtherTrainingDevices.Qty + this.Classrooms.Qty;
        //    this.Sumarry.Precentage = (this.Simulators.Precentage + this.OtherTrainingDevices.Precentage +
        //                               this.Classrooms.Precentage) / 3;
        //}
    }

    public class HumanResourcesTrainingCosts
    {
        public HourlyCosts Instrucotrs { get; set; }
        public HourlyCosts Evaluators { get; set; }

        public CostValues Summary { get; set; }


        public HumanResourcesTrainingCosts()
        {
            Instrucotrs = new HourlyCosts();
            Evaluators = new HourlyCosts();

            Summary = new CostValues();

        }

        //public void CalculateSummary()
        //{
        //    this.Summary.Value = this.Instrucotrs.Summary.Value + this.Evaluators.Summary.Value;
        //    this.Summary.Qty = this.Instrucotrs.Summary.Qty + this.Evaluators.Summary.Qty;
        //    this.Summary.Precentage = (this.Instrucotrs.Summary.Precentage + this.Evaluators.Summary.Precentage) / 2;
        //}
    }

    public class TraineesTrainingCosts
    {
        public HourlyCosts Pilots { get; set; }

        public CostValues Summary { get; set; }

        public TraineesTrainingCosts()
        {
            Pilots = new HourlyCosts();
            Summary = new CostValues();
        }

        //public void CalculateSummary()
        //{
        //    Summary.Value = Pilots.Summary.Value;
        //    Summary.Qty = Pilots.Summary.Qty;
        //    Summary.Precentage = Pilots.Summary.Precentage;
        //}
    }
    public class HourlyCosts
    {
        public CostValues StandardRate { get; set; }
        public CostValues SpecialRate { get; set; }

        public string Unit { get; set; }

        public CostValues Summary { get; set; }


        public HourlyCosts()
        {
            Unit = "Hours";
            StandardRate = new CostValues();
            SpecialRate = new CostValues();
            Summary = new CostValues();
        }

        //public void CalculateSummary()
        //{
        //    this.Summary.Value = this.StandardRate.Value + this.SpecialRate.Value;
        //    this.Summary.Qty = this.StandardRate.Qty + this.SpecialRate.Qty;
        //    this.Summary.Precentage = (this.StandardRate.Precentage + this.SpecialRate.Precentage) / 2 ;

        //}
    }

    public class TravelAndLivingCosts
    {
        public LivingCosts Instructors { get; set; }
        public LivingCosts Evaluators { get; set; }
        public LivingCosts Pilots { get; set; }

        public CostValues Summary { get; set; }

        public TravelAndLivingCosts()
        {
            Instructors = new LivingCosts();
            Pilots = new LivingCosts();
            Evaluators = new LivingCosts();

            Summary = new CostValues();
        }

        //public void calculateSummary()
        //{
        //    Summary.Value = this.Instructors.Summary.Value + this.Evaluators.Summary.Value + this.Pilots.Summary.Value;
        //    Summary.Qty = this.Instructors.Summary.Qty + this.Evaluators.Summary.Qty + this.Pilots.Summary.Qty;
        //    Summary.Precentage = (this.Instructors.Summary.Precentage + this.Evaluators.Summary.Precentage + this.Pilots.Summary.Precentage) / 3 ;
        //}
    }

    public class LivingCosts
    {
        public CostValues Planned { get; set; }
        public CostValues Dedicated { get; set; }

        public string Unit { get; set; }

        public CostValues Summary { get; set; }

        public LivingCosts()
        {
            Unit = "Miles";
            Planned = new CostValues();
            Dedicated = new CostValues();

            Summary = new CostValues();
        }

        //public void CalculateSummary()
        //{
        //    Summary.Value = this.Planned.Value + this.Dedicated.Value;
        //    Summary.Qty = this.Planned.Qty + this.Dedicated.Qty;
        //    Summary.Precentage = (this.Planned.Precentage + this.Dedicated.Precentage) / 2 ;
        //}
    }

    public class Hotels
    {
        public HotelCosts Instructors { get; set; }
        public HotelCosts Evaluators { get; set; }
        public HotelCosts Pilots { get; set; }

        public CostValues Summary { get; set; }

        public Hotels()
        {
            Instructors = new HotelCosts();
            Evaluators = new HotelCosts();
            Pilots = new HotelCosts();
            Summary = new CostValues();
        }

        //public void CalculatePrice()
        //{
        //    Summary.Value = this.Instructors.Summary.Value + this.Evaluators.Summary.Value + this.Pilots.Summary.Value;
        //    Summary.Qty = this.Instructors.Summary.Qty + this.Evaluators.Summary.Qty + this.Pilots.Summary.Qty;
        //    Summary.Precentage = (this.Instructors.Summary.Precentage + this.Evaluators.Summary.Precentage + this.Pilots.Summary.Precentage) / 3 ;
        //}
    }

    public class HotelCosts
    {
        public CostValues Requirednights { get; set; }
        public CostValues Forcednights { get; set; }

        public string Unit { get; set; }

        public CostValues Summary { get; set; }

        public HotelCosts()
        {
            Unit = "Nights";
            Requirednights = new CostValues();
            Forcednights = new CostValues();
            Summary = new CostValues();
        }

        //public void CalculatePrice()
        //{
        //    Summary.Value = this.Requirednights.Value + this.Forcednights.Value;
        //    Summary.Qty = this.Requirednights.Qty + this.Forcednights.Qty;
        //    Summary.Precentage = (this.Requirednights.Precentage + this.Forcednights.Precentage) / 2 ;
        //}
    }

    public class OtherCosts
    {
        public CostValues SimulatorSetup { get; set; }
        public CostValues Maintenance { get; set; }
        public CostValues SimulatorIdleTtime { get; set; }

        public string Unit { get; set; }

        public CostValues Summary { get; set; }

        public OtherCosts()
        {
            Unit = "Hours";
            SimulatorIdleTtime = new CostValues();
            Maintenance = new CostValues();
            SimulatorSetup = new CostValues();
            Summary = new CostValues();
        }

        //public void CalculateSummary()
        //{
        //    Summary.Value = SimulatorSetup.Value + Maintenance.Value + SimulatorIdleTtime.Value;
        //    Summary.Qty = SimulatorSetup.Qty + Maintenance.Qty + SimulatorIdleTtime.Qty;
        //    Summary.Precentage = (SimulatorSetup.Precentage + Maintenance.Precentage + SimulatorIdleTtime.Precentage) / 3 ;
        //}
    }

    public class TrainingValueAndRevalue
    {
        public PhysicalResourcesRevenue PhysicalResources { get; set; }
        public HumanResourcesTrainingRevenue HumanResources { get; set; }
        public TraineesRevenue TraineesRevenue { get; set; }

        public CostValues Summary { get; set; }

        public TrainingValueAndRevalue()
        {
            PhysicalResources = new PhysicalResourcesRevenue();
            HumanResources = new HumanResourcesTrainingRevenue();
            TraineesRevenue = new TraineesRevenue();

            Summary = new CostValues();
        }

        //public void CalculateSummary()
        //{
        //    Summary.Value = PhysicalResources.Summary.Value + this.HumanResources.Summary.Value +
        //                    this.TraineesRevenue.Summary.Value;
        //    Summary.Qty = PhysicalResources.Summary.Qty + this.HumanResources.Summary.Qty +
        //                    this.TraineesRevenue.Summary.Qty;
        //    Summary.Precentage = (PhysicalResources.Summary.Precentage + this.HumanResources.Summary.Precentage +
        //                    this.TraineesRevenue.Summary.Precentage) / 3 ;
        //}
    }

    public class PhysicalResourcesRevenue
    {
        public CostValues Simulator { get; set; }
        public CostValues OtherTrainingDevices { get; set; }
        public CostValues Classrooms { get; set; }

        public string Unit { get; set; }

        public CostValues Summary { get; set; }

        public PhysicalResourcesRevenue()
        {
            Unit = "Hours";
            Simulator = new CostValues();
            OtherTrainingDevices = new CostValues();
            Classrooms = new CostValues();

            Summary = new CostValues();
        }

        //public void CalculateSummary()
        //{
        //    Summary.Value = Simulator.Value + OtherTrainingDevices.Value + Classrooms.Value;
        //    Summary.Qty = Simulator.Qty + OtherTrainingDevices.Qty + Classrooms.Qty;
        //    Summary.Precentage = (Simulator.Precentage + OtherTrainingDevices.Precentage + Classrooms.Precentage) / 3 ;
        //}
    }

    public class HumanResourcesTrainingRevenue
    {
        public HourlyCosts Instrctors { get; set; }
        public HourlyCosts Evaluators { get; set; }

        public CostValues Summary { get; set; }

        public HumanResourcesTrainingRevenue()
        {
            Instrctors = new HourlyCosts();
            Evaluators = new HourlyCosts();

            Summary = new CostValues();

        }

        //public void CalculateSummary()
        //{
        //    Summary.Value = Instrctors.Summary.Value + Evaluators.Summary.Value;
        //    Summary.Qty = Instrctors.Summary.Qty + Evaluators.Summary.Qty;
        //    Summary.Precentage = (Instrctors.Summary.Precentage + Evaluators.Summary.Precentage) / 2 ;
        //}
    }

    public class TraineesRevenue
    {
        public CostValues PilotsContinuingQualifications { get; set; }
        public CostValues PilotsQualificationTraining { get; set; }

        public CostValues Summary { get; set; }

        public string Unit { get; set; }

        public TraineesRevenue()
        {
            Unit = "Hours";
            PilotsContinuingQualifications = new CostValues();
            PilotsQualificationTraining = new CostValues();

            Summary = new CostValues();
        }

        //public void CalculateSummary()
        //{
        //    Summary.Value = PilotsContinuingQualifications.Value + PilotsQualificationTraining.Value;
        //    Summary.Qty = PilotsContinuingQualifications.Qty + PilotsQualificationTraining.Qty;
        //    Summary.Precentage = (PilotsContinuingQualifications.Precentage + PilotsQualificationTraining.Precentage) / 2 ;
        //}
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace FoxConcept.Models.ViewModels.Optimization
{
	public class OptimizationSettings
	{
		public int OptimizationScheduleResolution { get; set; }
		public List<OptimizationGoal> OptimizationGoals { get; set; }
	}

    [Serializable]
	public class OptimizationGoal
	{
		public int GoalTypeID { get; set; }
		public string GoalTypeDescription { get; set; }
		public int? GoalConstraintTypeID { get; set; }
		public string GoalConstraintTypeDescription { get; set; }
		public string GoalConstraintTargetValue { get; set; }
		public int[] ResourceTypeIDs { get; set; }
		public int Priority { get; set; }
		public string OptimizationStartDateTimeFrame { get; set; }
		public string OptimizationEndDateTimeFrame { get; set; }
		public bool Selected { get; set; }
        public string Value { get; set; }
		public SelectListItem[] Operators { get; set; }

	}
}

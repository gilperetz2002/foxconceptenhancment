﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace FoxConcept.Models.ViewModels.Optimization
{

	public class AdditioanlTrainingRequest
	{
		private string DATE_FORMAT = "MM/dd/yyyy";

		public int RequiredMasterPlan { get; set; }
		public int RequiredQuantity { get; set; }
		public int Priorety { get; set; }
		public string RequestEndDate { get; set; }
		public string RequestStartDate { get; set; }


		public DateTime RequestStartDateObject
		{
			get { return RequestEndDate != null ? DateTime.ParseExact(RequestStartDate, DATE_FORMAT, null): new DateTime(); }
		}
		public DateTime RequestEndDateObject
		{
			get { return RequestEndDate != null ? DateTime.ParseExact(RequestEndDate, DATE_FORMAT, null): new DateTime(); }
		}
	}
}

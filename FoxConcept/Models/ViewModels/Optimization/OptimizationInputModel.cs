﻿using FoxConcept.Models.ViewModels.Optimization;


namespace FoxConcept.Models
{
	using System;
	using System.Data.Entity;
	using System.Linq;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using FoxConcept.Models.DALModels;
	using FoxConcept.Models.ViewModels.Shared;

	public class OptimizationInputModel
	{
		public OptimizationSettings OptimizationSettings { get; set; }
		public List<TrainingRequest> TrainingRequests { get; set; }
		public List<Resource> Resources { get; set; }
		public List<UserUnavalability> UsersUnavailability { get; set; }
		public List<MasterPlan> MasterPlans { get; set; }
	}
}

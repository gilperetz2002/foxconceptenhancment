﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using FoxConcept.Models.ViewModels.CostSettings;
using FoxConcept.Models.ViewModels.Shared;
using FoxConcept.Infrastructure;

namespace FoxConcept.Models.ViewModels.Optimization
{
    public class OptimizationSettingsViewModel
    {
        public IEnumerable<OptimizationGoal> Goals { get; set; }
        public IEnumerable<TrainingRequest> TrainingRequests { get; set; }
        public IEnumerable<ResourceType> RelevantResourceTypes { get; set; }
        public IEnumerable<MasterPlan> RelevantMasterPlans { get; set; }
        public DateTime OptimizationStartDate { get; set; }
        public DateTime OptimizationEndDate { get; set; }
        public int[] RelevantRequestIDs { get; set; }
        public IEnumerable<AdditioanlTrainingRequest> AdditioanlTrainingRequests { get; set; }
        public IEnumerable<SchedulingBusinessRule> SchedulingBusinessRules { get; set; }

        public bool UseEarlyGracePeriod { get; set; }



        public List<OptimizationGoal> GetAvailableGoals()
        {
            List<OptimizationGoal> goals = new List<OptimizationGoal>();
            int goalID = 1;
            foreach (var value in Enum.GetValues(typeof(eCostType)))
            {
                goals.Add(new OptimizationGoal()
                {
                    GoalTypeID = goalID,
                    GoalTypeDescription = ((eCostType)value).GetDisplayName(),
                    Priority = 1,
                    Value = "50",
                    Operators = new SelectListItem[]{
					new SelectListItem{
						Selected= false,
						Text = "Maximize",
						Value = "100"
					}, new SelectListItem{
						Selected= true,
						Text = "Minimize",
						Value = "1"
					}
				}
                });

                goalID++;
            }
            goals.Add(new OptimizationGoal()
            {
                GoalTypeID = goalID,
                GoalTypeDescription = "Bidding Satisfaction",
                Priority = 1,
                Value = "50",
                Operators = new SelectListItem[]{
					new SelectListItem{
						Selected= false,
						Text = "Maximize",
						Value = "100"
					}, new SelectListItem{
						Selected= true,
						Text = "Minimize",
						Value = "1"
					}
				}
            });

            //goals.Add(new OptimizationGoal()
            //{
            //    GoalTypeID = 2,
            //    GoalTypeDescription = "Instructors travel time",
            //    Priority = 2,
            //    Operators = new SelectListItem[]{
            //        new SelectListItem{
            //            Selected=true,
            //            Text = "Minimize",
            //            Value = "1"
            //        }
            //    }

            //});

            //goals.Add(new OptimizationGoal()
            //{
            //    GoalTypeID = 4,
            //    GoalTypeDescription = "Revenue from external training",
            //    Priority = 2,
            //    Operators = new SelectListItem[]{
            //        new SelectListItem{
            //            Selected=true,
            //            Text = "Maximize",
            //            Value = "1"
            //        }
            //    }

            //});

            //goals.Add(new OptimizationGoal()
            //{
            //    GoalTypeID = 5,
            //    GoalTypeDescription = "Instructor bidding satisfaction",
            //    Priority = 1,
            //    Operators = new SelectListItem[]{
            //        new SelectListItem{
            //            Selected= false,
            //            Text = "Maximize",
            //            Value = "1"
            //        }, new SelectListItem{
            //            Selected= true,
            //            Text = "Minimize",
            //            Value = "1"
            //        }
            //    }

            //});

            //goals.Add(new OptimizationGoal()
            //{
            //    GoalTypeID = 6,
            //    GoalTypeDescription = "Usage of external resources",
            //    Priority = 1,
            //    Operators = new SelectListItem[]{
            //        new SelectListItem{
            //            Selected= true,
            //            Text = "Minimize",
            //            Value = "1"
            //        }, new SelectListItem{
            //            Selected= false,
            //            Text = "Maximize",
            //            Value = "1"
            //        }
            //    }

            //});



            return goals;
        }

    }



}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FoxConcept.Models.ViewModels.Optimization
{
	public class RunOptimizationViewModel
	{
		public string OptimizationInputFileName { get; set; }
		public string OptimizationOutputFileName { get; set; }
	}
}

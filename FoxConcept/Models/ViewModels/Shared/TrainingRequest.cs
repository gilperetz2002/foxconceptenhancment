﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FoxConcept.Controllers;

namespace FoxConcept.Models.ViewModels.Shared
{
	public class TrainingRequest
	{
		public int? TrainingRequestID { get; set; }
		public string TrainingRequestTitle { get; set; }
		public string TrainingRequestStartDate
		{
			get { return TrainingRequestStartDateObject == null ? null : TrainingRequestStartDateObject.Value.ToString(OptimizationController.GLOBAL_DATE_TIME_FORMAT); }
		}
		public string TrainingRequestEndDate
		{
			get { return TrainingRequestEndDateObject == null ? null : TrainingRequestEndDateObject.Value.ToString(OptimizationController.GLOBAL_DATE_TIME_FORMAT); }
		}
		public DateTime? TrainingRequestStartDateObject { get; set; }
		public DateTime? TrainingRequestEndDateObject { get; set; }
		public int? PreferredLocationID { get; set; }
		public int? RequestedForUserID { get; set; }
		public string PreferredCrewID { get; set; }
		public string RequestedPositionName { get; set; }
		public int? RequestedPositionID { get; set; }
		public int? MasterPlanID { get; set; }
		public string MasterPlanName { get; set; }
		public int RequestPriority { get; set; }
		public int TrainingExpectedIncome { get; set; }

		public List<ParticipantRequirement> ParticipantRequirements { get; set; }
		public List<Activity> ActivitiesToSchedule { get; set; }


		public string RequestedForUserName { get; set; }

		public long MasterPlanCost { get; set; }

		public long MasterPlanRevenue { get; set; }
	}

	public class Event
	{
		public int EventID { get; set; }
		public string EventName { get; set; }
		public string EventStartTime { get; set; }
		public string EventEndTime { get; set; }
	}

	public class ParticipantRequirement
	{
		public int ParticipantRequirementQuota { get; set; }
		public int ParticipantRequirementPositionID { get; set; }
		public string ParticipantRequirementPositionName { get; set; }
	}
}

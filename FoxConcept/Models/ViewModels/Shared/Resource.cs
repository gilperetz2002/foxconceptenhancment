﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FoxConcept.Models.ViewModels.Shared
{
	public class Resource
	{
		public int ResourceID { get; set; }
		public String ResourceName { get; set; }
		public int ResourceTypeID { get; set; }
		public string ResourceTypeName { get; set; }
		public int ResourceLocationID { get; set; }
		public List<ResourceProperties> ResourceProperties { get; set; }
		public List<ResourceCertifications> ResourceCertifications { get; set; }
		public List<Event> ResourceUnavailability { get; set; }


	}

	public class ResourceProperties
	{
		public int? PropertyID { get; set; }
		public string PropertyName { get; set; }
		public int? PropertyTypeID { get; set; }
		public string PropertyTypeName { get; set; }
		public int? IsMultipleChoice { get; set; }
		public string Value { get; set; }
		public string[] SelectedOptionIDs { get; set; }
	}

	public class ResourceCertifications
	{
		public int CertificationID { get; set; }
		public string CretificationName { get; set; }
		public string ExpirationDate { get; set; }
	}
}

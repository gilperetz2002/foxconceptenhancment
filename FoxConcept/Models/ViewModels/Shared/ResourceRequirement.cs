﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FoxConcept.Models.ViewModels.Shared
{
	public class ResourceRequirement
	{
		public int RequirementID { get; set; }
		public string RequirementResourceTypeName { get; set; }
		public int RequirementResourceTypeID { get; set; }
		public int RequirementStartTimeDelta { get; set; }
		public int RequirementEndTimeDelta { get; set; }

		public List<RequirementProperties> RequirementProperties { get; set; }
		public int[] CertificationRequirementIDs { get; set; }


	}


	public class RequirementProperties
	{
		public int PropertyID { get; set; }
		public string PropertyName { get; set; }
		public int PropertyTypeID { get; set; }
		public string PropertyTypeName { get; set; }
		public int OperatorID { get; set; }
		public int? IsMultipleChoice { get; set; }
		public string Value { get; set; }
		public string MinValue { get; set; }
		public string MaxValue { get; set; }
		public string[] SelectedOptionIDs { get; set; }
	}

}

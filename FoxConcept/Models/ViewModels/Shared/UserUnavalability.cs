﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FoxConcept.Models.ViewModels.Shared
{
	public class UserUnavalability
	{
		public int UserID { get; set; }
		public string UserName { get; set; }
		public List<Event> UserUnavailability { get; set; }
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FoxConcept.Models.ViewModels.Shared
{
	public class Activity
	{
		public int ActivityID { get; set; }
		public string ActivityName { get; set; }
		public int ActivityDuration { get; set; }

		public List<Prerequisite> ActivityPrerequisites { get; set; }
		public List<ParticipantRequirement> ActivityParticipantRequirements  { get; set; }
		public List<ResourceRequirement> ActivityResourceRequirements { get; set; }



	}

	public class Prerequisite
	{
		public int PrerequisiteActivityID { get; set; }
		public string PrerequisiteActivityName {get;set;}
		public int? PrerequisiteCompletionsMaxDays { get; set; }
		public int? PrerequisiteCompletionsMinDays { get; set; }

	}

	
}

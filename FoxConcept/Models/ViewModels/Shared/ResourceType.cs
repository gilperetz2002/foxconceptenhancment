﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FoxConcept.Models.ViewModels.Shared
{
	public class ResourceType
	{
		public int TypeID { get; set; }
		public string TypeName { get; set; }

		public ResourceType() { }

		public ResourceType(int i_ResTypeID, string i_ResName)
		{
			TypeID = i_ResTypeID;
			TypeName = i_ResName;
		}

		public bool isSelected { get; set; }


		public class ResourceTypeComparer : IEqualityComparer<ResourceType>
		{
			public bool Equals(ResourceType x, ResourceType y)
			{
				return x.TypeID == y.TypeID;
			}

			public int GetHashCode(ResourceType obj)
			{
				return obj.TypeID;
			}
		}
	}
}

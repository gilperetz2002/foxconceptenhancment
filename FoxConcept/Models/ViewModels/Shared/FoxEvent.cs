﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FoxConcept.Models.ViewModels.Shared
{
	public class FoxEvent
	{
		public int ActivityID { get; set; }
		public DateTime StartDate { get; set; }
		public DateTime EndDate { get; set; }
		public List<ResourceAllocation> AllocatedResources { get; set; }
		public int DurationInMinutes
		{
			get { return (int)(EndDate - StartDate).TotalMinutes; }
		}

	}

	public class ResourceAllocation
	{
		public int AllocationID { get; set; }
		public DateTime StartTime { get; set; }
		public DateTime EndTime { get; set; }
		public int ResourceID { get; set; }
		public int ResourceTypeID { get; set; }
		public string ResourceTypeName { get; set; }
		public int DurationInMinutes
		{
			get { return (int)(EndTime - StartTime).TotalMinutes; }
		}

		public int OriginalRequirementID { get; set; }
		public int Origin { get; set; }

	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FoxConcept.Models.ViewModels.Shared
{
	public class TrainingProgram
	{
		public int TrainingProgramID { get; set; }
		public int? MasterPlanID { get; set; }
		public string MasterPlanName { get; set; }
		public string TrainingProgramPlanName { get; set; }
		public DateTime? StartDate { get; set; }
		public DateTime? EndDate { get; set; }
		public List<TrainingRequest> AnsweredRequests { get; set; }
		public List<FoxEvent> Events { get; set; }
		public bool isScheduled { get; set; }
		public int OriginalRequestID { get; set; }

		public long MasterPlanCost { get; set; }
		public long MasterPlanRevenue { get; set; }
	}
}

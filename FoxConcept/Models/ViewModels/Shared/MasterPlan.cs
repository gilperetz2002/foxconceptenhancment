﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FoxConcept.Models.ViewModels.Shared
{
	public class MasterPlan
	{
		public int MasterPlanID { get; set; }
		public string MasterPlanName { get; set; }
		public List<Activity> ActivitiesToSchedule { get; set; }
	    public int SelectedMasterPlan { get; set; }
	    public string RequiredQuatity { get; set; }
	}
}

﻿
namespace FoxConcept.Models
{
	using System;
	using System.Data.Entity;
	using System.Linq;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using FoxConcept.Models.DALModels;

	public class CertificationStatusViewModel
	{
		public IEnumerable<DWH_Certification_Details_Main> AvailableCertifications { get; set; }
		public IEnumerable<DWH_User_Certification> UserCertificationData { get; set; }
		public CertificationStatusFilter FilterData { get; set; }

		public CertificationStatusViewModel()
		{
			AvailableCertifications = new List<DWH_Certification_Details_Main>();
			UserCertificationData = new List<DWH_User_Certification>();
			FilterData = new CertificationStatusFilter();
		}

		public class CertificationStatusFilter
		{
			public int? CertificationIDsFilter { get; set; }
			public DateTime? ExpirationStartDateFilter { get; set; }
			public DateTime? ExpirationEndDateFilter { get; set; }

			public CertificationStatusFilter()
			{
				CertificationIDsFilter = null;
				ExpirationEndDateFilter = null;
				ExpirationStartDateFilter = null;
			}
		}

	}
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using FoxConcept.Models.ViewModels.CostSettings;

namespace FoxConcept.Models.ViewModels.BiddingRequests
{

    public class BiddingRequestsViewMode
    {
        public int InstractorID { get; set; }
        public Instructor Instructor { get; set; }
        public IEnumerable<BiddingRequestViewModel> BiddingRequests { get; set; }
        public IEnumerable<BiddingRule> BiddingRules { get; set; }

        void Foo(){} 

        public BiddingRequestsViewMode()
        {
            BiddingRequests = new List<BiddingRequestViewModel>();
            Instructor = new Instructor();
            BiddingRules = new List<BiddingRule>();
        }
    }

    public class BiddingRequestViewModel
    {
        public int ID { get; set; }

        [Display(Name = "Instructor")]
        public int InstructorID { get; set; }

        [Display(Name = "Instructor")]
        public string InstructorName { get; set; }

        [Display(Name = "Bidding Rule")]
        public int BiddingRuleID { get; set; }

        [Display(Name = "Bidding Type")]
        public BiddingRule BiddingRule { get; set; }

        [Display(Name = "Bidding Period")]
        public int BiddingPeriodID { get; set; }

        [Display(Name = "Bidding Period")]
        public BiddingPeriod BiddingPeriod { get; set; }

        public IEnumerable<BiddingPeriod> BiddingPeriods { get; set; }

        [Display(Name = "Start Date")]
        public DateTime StartDate { get; set; }

        [Display(Name = "End Date")]
        public DateTime? EndDate { get; set; }

        [Display(Name = "Priority")]
        public int Priority { get; set; }

        [Display(Name = "Requested days left")]
        public int RequestsLeft { get; set; }

        public string Comments { get; set; }

        public BiddingRequest.eStatusRequest Status { get; set; }

        public BiddingRequestViewModel()
        {
            BiddingPeriods = new List<BiddingPeriod>();
        }

    }

    public class Instructor
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public int EntityTypeID { get; set; }
    }

    public class BiddingManagementViewModel
    {
        //Filters
        [Display(Name = "Instructor")]
        public List<EntityType> Instructors { get; set; }
        [Display(Name = "Instructor")]
        public int InstructorID { get; set; }

        [Display(Name = "Bidding Periods")]
        public List<EntityType> BiddingPeriods { get; set; }
        [Display(Name = "Bidding Period")]
        public int BiddingPeriodID { get; set; }

        public IEnumerable<BiddingRequestViewModel> BiddingRequests { get; set; }

        public BiddingManagementViewModel()
        {
            BiddingRequests = new List<BiddingRequestViewModel>();
            this.BiddingPeriods = new List<EntityType>();
            BiddingPeriods.Add(new EntityType()
                {
                    ID = -1,
                    Name = "Select a Value"
                });

            this.Instructors = new List<EntityType>();
            this.Instructors.Add(new EntityType()
                {
                    ID = -1,
                    Name = "Select a Value"
                });
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FoxConcept.Models;
using FoxConcept.Models.ViewModels.Shared;
using FoxConcept.Models.DALModels;
using FoxConcept.Models.ViewModels.Optimization;
using System.Web.Script.Serialization;
using System.Diagnostics;
using FoxConcept.Infrastructure.Attributes;
using System.IO;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;


namespace FoxConcept.Controllers
{
	public class AutomaticSchedulingController : Controller
	{

		public const int ADDIRIOAN_REQUEST_START_ID = 100000000;
		public const string GLOBAL_DATE_TIME_FORMAT = "yyy-MM-dd HH:mm";
		public const string GLOBAL_DATE_ONLY_FORMAT = "yyy-MM-dd";

		[HttpPost]
		public ActionResult Settings(IEnumerable<string> UserCertifications, string ActionType)
		{
			var userCertificationData = UserCertifications.Select(s => System.Web.Helpers.Json.Decode<DWH_User_Certification>(s)).ToList();
			AutomaticSchedulingSettingsViewModel model = new AutomaticSchedulingSettingsViewModel();
			using (var _context = new FoxModelContainer())
			{
				var requirements = getSchedulingRequirements(userCertificationData, _context);
				// Add more empty rows for configuration
				requirements.Add(new AutomaticSchedulingSettingsViewModel.AutomaticScheduleRequirement());
				requirements.Add(new AutomaticSchedulingSettingsViewModel.AutomaticScheduleRequirement());
				requirements.Add(new AutomaticSchedulingSettingsViewModel.AutomaticScheduleRequirement());
				model.SchedulingRequirements = requirements.ToArray();

				var masterPlans = new List<MasterPlan>();
				// Add empty value for select list
				masterPlans.Add(new MasterPlan() { MasterPlanID = -1 });
				masterPlans.AddRange(_context.DWH_MasterPlansBasicData.Select(m => new MasterPlan { MasterPlanID = m.MasterPlanID, MasterPlanName = m.MasterPlanName }).ToList());
				model.AvailableMasterPlans = masterPlans;
				var relevantMasterPlanIDs = model.SchedulingRequirements.Select(s => s.MasterPlan).Select(m => m.MasterPlanID).ToList();
				model.ResourceTypesToConcider = getRelevantResourceTypes(relevantMasterPlanIDs, _context).ToArray();
			}

			return ActionType == "ScheduleTraining" ? View("AutomaticTrainingScheduleSettings", model) : View("AutomaticUserAssignmentSettings", model);
		}

		[HttpPost]
		public ActionResult RunAutomation(AutomaticSchedulingSettingsViewModel i_InputData)
		{
			OptimizationSettingsViewModel optimizationModel = new OptimizationSettingsViewModel();
			optimizationModel.Goals = new List<OptimizationGoal>();
			optimizationModel.RelevantMasterPlans = i_InputData.SchedulingRequirements.Select(sr => sr.MasterPlan);
			optimizationModel.RelevantResourceTypes = i_InputData.ResourceTypesToConcider;
			optimizationModel.UseEarlyGracePeriod = false;
			var trainingRequests = new List<AdditioanlTrainingRequest>();

			using (var _context = new FoxModelContainer())
			{
				foreach (var sr in i_InputData.SchedulingRequirements)
				{
					var mp = sr.MasterPlan;
					var req = new AdditioanlTrainingRequest();
					req.RequiredQuantity = sr.RequiredQuantity.HasValue ? sr.RequiredQuantity.Value : 0;
					req.RequestStartDate = sr.RequiredStartDate.HasValue ? sr.RequiredStartDate.Value.ToString("MM/dd/yyy") : default(DateTime).ToString("MM/dd/yyy");
					req.RequestEndDate = sr.RequiredEndDate.HasValue ? sr.RequiredEndDate.Value.ToString("MM/dd/yyy") : default(DateTime).ToString("MM/dd/yyy");
					req.RequiredMasterPlan = mp.MasterPlanID;
					trainingRequests.Add(req);
				}
				optimizationModel.AdditioanlTrainingRequests = trainingRequests;
			}

			RunOptimizationViewModel model = new RunOptimizationViewModel();
			var inputModel = getOptimizationModel(optimizationModel);
			var jsonInput = new JavaScriptSerializer().Serialize(inputModel.Data);
			var jobID = DateTime.Now.ToString("yyy-MM-ddTHH-mm-fffffff");

			var inputfilePath = ControllerContext.HttpContext.Server.MapPath(string.Format("~/App_Data/input_Automation_{0}.json", jobID));
			saveInputFile(inputfilePath, jsonInput);
			var outputfilePath = ControllerContext.HttpContext.Server.MapPath(string.Format("~/App_Data/output_Automation_{0}.json", jobID));

			var maxOffDays = System.Configuration.ConfigurationManager.AppSettings["MaxOffDays"];
			var maxHoursPerDay = System.Configuration.ConfigurationManager.AppSettings["MaxHoursPerDay"];
			var debugLevel = System.Configuration.ConfigurationManager.AppSettings["OptimizationLogLevel"];
			var optimizationScriptPath = System.Configuration.ConfigurationManager.AppSettings["OptimizationUtility"];
			var pythonPath = System.Configuration.ConfigurationManager.AppSettings["PythonCompilerPath"];
			var psi = getOptimizationUtilityProcessStartInfo(inputfilePath, outputfilePath, optimizationScriptPath, pythonPath, maxHoursPerDay, maxOffDays, debugLevel);

			startOptimizationProcess(psi);
			model.OptimizationInputFileName = inputfilePath;
			model.OptimizationOutputFileName = outputfilePath;
			return View("RunAutomation", model);
		}

		[HttpGet]
		public ActionResult ShowAutomationResult(string InputFilePath)
		{
			ShowPlanViewModel model = new ShowPlanViewModel();
			model.InputFilePath = InputFilePath;
			model.PlanID = DateTime.Now.Ticks;
			model.PlanName = InputFilePath;
			using (StreamReader reader = System.IO.File.OpenText(InputFilePath))
			{
				JObject jsonPlan = (JObject)JToken.ReadFrom(new JsonTextReader(reader));

				JArray jsonTrainingPrograms = (JArray)jsonPlan["TrainingPrograms"];
				model.TrainingPrograms = getTrainingProgramsFromJSON(jsonTrainingPrograms).OrderBy(tp => tp.OriginalRequestID);

			}
			return View(model);
		}

		private List<TrainingProgram> getTrainingProgramsFromJSON(JArray jsonTrainingPrograms)
		{
			List<TrainingProgram> trainingProgramsToReturn = new List<TrainingProgram>();
			using (var _context = new FoxModelContainer())
			{
				foreach (var tp in jsonTrainingPrograms)
				{
					TrainingProgram itemToAdd = new TrainingProgram();

					itemToAdd.isScheduled = tp["ScheduledStatus"].Value<bool>();
					itemToAdd.OriginalRequestID = tp["TrainingRequestID"].Value<int>();
					itemToAdd.MasterPlanID = tp["MasterPlanID"].ToObject<Nullable<int>>();
					itemToAdd.MasterPlanName = _context.DWH_MasterPlansBasicData.FirstOrDefault(mp => mp.MasterPlanID == itemToAdd.MasterPlanID).MasterPlanName;
					itemToAdd.MasterPlanCost = 0;
					itemToAdd.MasterPlanRevenue = 0;
					itemToAdd.StartDate = itemToAdd.isScheduled ? tp["TrainingProgramStartDate"].ToObject<DateTime?>() : null;
					itemToAdd.EndDate = itemToAdd.isScheduled ? tp["TrainingProgramEndDate"].ToObject<DateTime?>() : null;
					itemToAdd.Events = itemToAdd.isScheduled ? getEventsFromJSON((JArray)tp["Events"], _context) : null;
					itemToAdd.AnsweredRequests = getTrainingRequestsFromJSON((JArray)tp["AnsweredRequests"], _context);

					trainingProgramsToReturn.Add(itemToAdd);
				}
			}

			return trainingProgramsToReturn;
		}

		private List<TrainingRequest> getTrainingRequestsFromJSON(JArray i_Data, FoxModelContainer _context)
		{
			List<TrainingRequest> requestsToReturn = new List<TrainingRequest>();

			foreach (var item in i_Data)
			{
				TrainingRequest request = new TrainingRequest();
				request.TrainingRequestID = item["TrainingRequestID"].Value<int?>();
				request.RequestedForUserID = item["ParticipantID"].ToObject<int?>();
				if (request.RequestedForUserID != null && request.RequestedForUserID >= 0)
				{
					var userFromDB = _context.DWH_Users.FirstOrDefault(u => u.UserID == request.RequestedForUserID);
					request.RequestedForUserName = userFromDB.UserFirstName + " " + userFromDB.UserLastName;
				}
				request.RequestedPositionID = item["PositionID"].ToObject<int?>();
				request.RequestedPositionName = item["PositionName"].Value<string>();

				requestsToReturn.Add(request);
			}

			return requestsToReturn;
		}

		private List<FoxEvent> getEventsFromJSON(JArray i_EventsData, FoxModelContainer _context)
		{
			List<FoxEvent> eventsToReturn = new List<FoxEvent>();
			foreach (var ev in i_EventsData)
			{
				var eventItem = new FoxEvent();
				eventItem.ActivityID = ev["ActivityID"].Value<int>();
				eventItem.StartDate = ev["ActivityStartDate"].Value<DateTime>();
				eventItem.EndDate = ev["ActivityStartDate"].Value<DateTime>(); //TODO: get event end date
				eventItem.AllocatedResources = getAllocatedResourcesFromJSON((JArray)ev["ResourceAllocations"]);

				eventsToReturn.Add(eventItem);
			}

			return eventsToReturn;
		}

		private List<ResourceAllocation> getAllocatedResourcesFromJSON(JArray i_AllocatedResourceData)
		{
			List<ResourceAllocation> resourcesToReturn = new List<ResourceAllocation>();
			foreach (var alloc in i_AllocatedResourceData)
			{
				var allocation = new ResourceAllocation();
				allocation.AllocationID = alloc["ResourceRequirementID"].Value<int>(); //TODO: Generate AllocationID
				allocation.OriginalRequirementID = alloc["ResourceRequirementID"].Value<int>();
				allocation.StartTime = alloc["AllocationStartTime"].Value<DateTime>();
				allocation.EndTime = alloc["AllocationEndTime"].Value<DateTime>();
				allocation.ResourceID = alloc["AssignedResourceID"].Value<int>();

				resourcesToReturn.Add(allocation);
			}

			return resourcesToReturn;

		}

		private JsonResult getOptimizationModel(OptimizationSettingsViewModel i_Settings)
		{
			var requestIDs = i_Settings.RelevantRequestIDs;
			var additioanlTrainingRequests = i_Settings.AdditioanlTrainingRequests;
			var useEralyGracePeriod = i_Settings.UseEarlyGracePeriod;
			var revenueGoal = i_Settings.Goals.FirstOrDefault(g => g.GoalTypeID == 4);
			var maximizeRevenue = revenueGoal == null ? false : revenueGoal.Selected;

			OptimizationInputModel model = new OptimizationInputModel();
			using (var _context = new FoxModelContainer())
			{
				model.OptimizationSettings = getOptimizationSettings(_context);
				model.TrainingRequests = getAdditioanlTrainingRequest(additioanlTrainingRequests, _context);
				model.MasterPlans = getRelevantMasterPlansOfTrainingRequests(model.TrainingRequests, _context);
				model.Resources = getResources(_context);

			}

			return Json(model, JsonRequestBehavior.AllowGet);
		}

		private List<Resource> getResources(FoxModelContainer _context)
		{
			List<Resource> resourcesToReturn = new List<Resource>();
			int[] repositoriesToDisplay = { 7909, 7907, 8255, 7908, 160, 161 };
			foreach (var res in _context.DWH_ResourcesBasicData.Where(r => repositoriesToDisplay.Contains(r.ResourceRepositoryID)))
			{
				Resource resource = new Resource();
				resource.ResourceID = res.ResourceID;
				resource.ResourceName = res.ResourceName;
				resource.ResourceTypeID = res.ResourceMetaDataTypeID;
				resource.ResourceTypeName = res.ResourceMetaDataTypeName;
				resource.ResourceLocationID = 0; //TODO: Get Resource LocationID;
				resource.ResourceProperties = getResourcePropeties(resource.ResourceID, _context);
				resource.ResourceUnavailability = getResourceUnvalability(resource.ResourceID, _context);
				resource.ResourceCertifications = getResourceCerttifications(res.ResourceUserID, _context);

				resourcesToReturn.Add(resource);
			}

			return resourcesToReturn;
		}

		private List<ResourceCertifications> getResourceCerttifications(int? i_UserID, FoxModelContainer _context)
		{

			List<ResourceCertifications> certificationOfResource = new List<ResourceCertifications>();

			var certsFromDB = _context.DWH_User_Certifications.Where(c => c.User_ID == i_UserID);
			foreach (var cert in certsFromDB)
			{
				ResourceCertifications resourceCert = new ResourceCertifications();
				resourceCert.CertificationID = cert.Certification_ID;
				resourceCert.CretificationName = cert.Certification_Name;
				resourceCert.ExpirationDate = cert.User_Certification_Final_Expiration_Date.HasValue ? cert.User_Certification_Final_Expiration_Date.Value.ToString(GLOBAL_DATE_TIME_FORMAT) : "";
			}

			return certificationOfResource;


		}

		private List<UserUnavalability> getUserUnavalabilityEvents(List<TrainingRequest> trainingRequests, FoxModelContainer _context)
		{
			List<UserUnavalability> usersToReturn = new List<UserUnavalability>();
			var userIDsRequestedTraining = trainingRequests.Where(tr => tr.RequestedForUserID >= 0).Select(tr => tr.RequestedForUserID).Distinct();
			foreach (var userID in userIDsRequestedTraining)
			{
				if (userID.HasValue)
				{
					UserUnavalability ua = new UserUnavalability();
					ua.UserID = userID.Value;
					DWH_User userBasicData = _context.DWH_Users.FirstOrDefault(u => ua.UserID == u.UserID);
					ua.UserName = userBasicData.UserFirstName + " " + userBasicData.UserLastName;
					ua.UserUnavailability = getUserUnavalabilityEvents(userID, _context);

					usersToReturn.Add(ua);
				}
			}
			return usersToReturn;
		}

		private List<Event> getUserUnavalabilityEvents(int? i_UserID, FoxModelContainer _context)
		{
			List<Event> unavalabilityEvets = new List<Event>();
			if (i_UserID == null) return unavalabilityEvets;

			foreach (var uea in _context.DWH_UserEventAssociations.Where(ue => ue.User_ID == i_UserID))
			{
				DWH_EventBasicData eventData = _context.DWH_EventsBasicData.FirstOrDefault(e => e.Event_ID == uea.Event_ID);
				if (eventData != null)
				{
					Event userEvent = new Event();
					userEvent.EventID = eventData.Event_ID;
					userEvent.EventName = eventData.Event_Name;
					userEvent.EventStartTime = eventData.Event_StartTime.Value.ToString(GLOBAL_DATE_TIME_FORMAT);
					userEvent.EventEndTime = eventData.Event_EndTime.Value.ToString(GLOBAL_DATE_TIME_FORMAT);

					unavalabilityEvets.Add(userEvent);
				}
			}

			return unavalabilityEvets;
		}

		private List<Event> getResourceUnvalability(int i_ResourceID, FoxModelContainer _context)
		{
			List<Event> events = new List<Event>();
			foreach (var alloc in _context.DWH_ResourcesAllocations.Where(a => a.ResourceID == i_ResourceID))
			{
				Event ev = new Event();
				ev.EventID = alloc.AllocationID;
				DWH_EventBasicData dbEvent = _context.DWH_EventsBasicData.FirstOrDefault(e => e.Event_ID == alloc.EventID);
				ev.EventName = dbEvent == null ? "" : dbEvent.Event_Name;
				//Ignore allocation time is null (View BUG)
				ev.EventStartTime = alloc.ResourceStartAllocationTime == null ? "" : alloc.ResourceStartAllocationTime.Value.ToString(GLOBAL_DATE_TIME_FORMAT);
				ev.EventEndTime = alloc.ResourceEndAllocationTime == null ? "" : alloc.ResourceEndAllocationTime.Value.ToString(GLOBAL_DATE_TIME_FORMAT);

				events.Add(ev);
			}

			return events;
		}

		private List<ResourceProperties> getResourcePropeties(int i_ResourceID, FoxModelContainer _context)
		{
			List<ResourceProperties> props = new List<ResourceProperties>();
			var propsList = _context.DWH_ResourcesExtraDetails.Where(ed => ed.ResourceID == i_ResourceID).Select(ed => new
			{
				ResourceMetaDataFieldID = ed.ResourceMetaDataFieldID,
				ResourceMetaDataFieldName = ed.ResourceMetaDataFieldName,
				ResourceMetaDataFieldTypeID = ed.ResourceMetaDataFieldTypeID,
				ResourceMetaDataTypeName = ed.ResourceMetaDataTypeName,
				ResourceMetaDataFieldValue = ed.ResourceMetaDataFieldValue,
				ResourceMetaDataFieldIsMultipleSelection = ed.ResourceMetaDataFieldIsMultipleSelection,
			});
			foreach (var prop in propsList)
			{
				ResourceProperties property = new ResourceProperties();
				property.PropertyID = prop.ResourceMetaDataFieldID;
				property.PropertyName = prop.ResourceMetaDataFieldName;
				property.PropertyTypeID = prop.ResourceMetaDataFieldTypeID;
				property.PropertyTypeName = prop.ResourceMetaDataTypeName;
				property.SelectedOptionIDs = prop.ResourceMetaDataFieldValue == null ? null : prop.ResourceMetaDataFieldValue.Split(',');
				property.IsMultipleChoice = prop.ResourceMetaDataFieldIsMultipleSelection;
				property.Value = prop.ResourceMetaDataFieldValue;

				if (property.PropertyID != null) //Workaround for some wierd bug where an extra details row is returned with nulls
				{
					props.Add(property);
				}

			}

			return props;

		}


		private List<MasterPlan> getRelevantMasterPlansOfTrainingRequests(List<TrainingRequest> i_RelevantTrainingRequests, FoxModelContainer _context)
		{
			List<MasterPlan> masterPlansToReturn = new List<MasterPlan>();
			var relevantMasterPlanIDs = i_RelevantTrainingRequests.Select(tr => tr.MasterPlanID).Distinct().ToList();
			var masterPlansList = _context.DWH_MasterPlansBasicData.Where(mp => relevantMasterPlanIDs.Contains(mp.MasterPlanID));

			foreach (var mp in masterPlansList)
			{
				MasterPlan masterPlan = new MasterPlan();
				masterPlan.MasterPlanID = mp.MasterPlanID;
				masterPlan.MasterPlanName = mp.MasterPlanName;
				masterPlan.ActivitiesToSchedule = getActivitiesToSchedule(mp.MasterPlanID, _context);

				masterPlansToReturn.Add(masterPlan);
			}

			return masterPlansToReturn;
		}

		private List<ResourceRequirement> getActivityResourceRequirements(int i_ActivityID, FoxModelContainer _context)
		{
			List<ResourceRequirement> resourceRequirementsToReturn = new List<ResourceRequirement>();
			foreach (var req in _context.DWH_ResourcesRequirements.Where(rr => rr.ActivityID == i_ActivityID))
			{
				ResourceRequirement requirement = new ResourceRequirement();
				requirement.RequirementID = req.RequirementResourceID;
				requirement.RequirementResourceTypeName = req.RequirementResourceTypeName;
				requirement.RequirementResourceTypeID = req.RequirementResourceTypeID;
				requirement.RequirementStartTimeDelta = 0; // TODO Add delta start time
				requirement.RequirementEndTimeDelta = 0; // TODO Add delta end time
				requirement.RequirementProperties = getResourceRequirementProperties(requirement.RequirementID, _context);
				requirement.CertificationRequirementIDs = null; // TODO Add requirement certifications

				resourceRequirementsToReturn.Add(requirement);

			}

			return resourceRequirementsToReturn;

		}

		private List<RequirementProperties> getResourceRequirementProperties(int i_RequirementID, FoxModelContainer _context)
		{
			List<RequirementProperties> properties = new List<RequirementProperties>();
			foreach (var prop in _context.DWH_ResourcesRequirementProperty.Where(p => p.ResourceRequirementID == i_RequirementID))
			{
				RequirementProperties property = new RequirementProperties();
				property.PropertyID = prop.ResourceRequirementPropertyID;
				property.PropertyName = prop.ResourceRequirementPropertyName;
				property.IsMultipleChoice = prop.IsMultipleChoice;
				property.MaxValue = prop.ResourceRequirementPropertyMaxValue;
				property.MinValue = prop.ResourceRequirementPropertyMinValue;
				property.OperatorID = //TODO: find the operator ID
				property.PropertyTypeID = prop.ResourceRequirementPropertyTypeID;
				property.SelectedOptionIDs = prop.ResourceRequirementPropertyValue == null ? null : prop.ResourceRequirementPropertyValue.Split(',');
				property.PropertyID = prop.ResourceRequirementPropertyID;
				property.PropertyTypeName = ""; //TODO: Get property type name

				properties.Add(property);
			}

			return properties;

		}

		private List<Prerequisite> getActivityPrerequisites(int i_ActivityID, FoxModelContainer _context)
		{
			List<Prerequisite> prerequisistes = new List<Prerequisite>();
			foreach (var prereq in _context.DWH_ActivityPrerequisit.Where(pr => pr.ActivityID == i_ActivityID))
			{
				Prerequisite prerequisite = new Prerequisite();
				prerequisite.PrerequisiteActivityID = prereq.PrerequisiteID;
				prerequisite.PrerequisiteActivityName = prereq.PrerequisiteName;
				prerequisite.PrerequisiteCompletionsMaxDays = prereq.PrerequisiteMaxDays;
				prerequisite.PrerequisiteCompletionsMinDays = prereq.PrerequisiteMinDays;

				prerequisistes.Add(prerequisite);
			}

			return prerequisistes;
		}

		private List<ParticipantRequirement> getActivitieParticipantRequirements(int p, FoxModelContainer _context)
		{
			//TODO: Add relevant requirements
			List<ParticipantRequirement> participantRequirements = new List<ParticipantRequirement>();
			ParticipantRequirement requirement1 = new ParticipantRequirement();
			requirement1.ParticipantRequirementPositionID = 1;
			requirement1.ParticipantRequirementPositionName = "Captain";
			requirement1.ParticipantRequirementQuota = 1;
			ParticipantRequirement requirement2 = new ParticipantRequirement();
			requirement2.ParticipantRequirementPositionID = 2;
			requirement2.ParticipantRequirementPositionName = "First Officer";
			requirement2.ParticipantRequirementQuota = 1;
			participantRequirements.Add(requirement1);
			participantRequirements.Add(requirement2);

			return participantRequirements;
		}

		private List<ParticipantRequirement> getParticipantRequirements(int? i_MasterPlanID, FoxModelContainer _context)
		{
			//TODO: Add relevant requirements
			List<ParticipantRequirement> participantRequirements = new List<ParticipantRequirement>();
			ParticipantRequirement requirement1 = new ParticipantRequirement();
			requirement1.ParticipantRequirementPositionID = 1;
			requirement1.ParticipantRequirementPositionName = "Captain";
			requirement1.ParticipantRequirementQuota = 1;
			ParticipantRequirement requirement2 = new ParticipantRequirement();
			requirement2.ParticipantRequirementPositionID = 2;
			requirement2.ParticipantRequirementPositionName = "First Officer";
			requirement2.ParticipantRequirementQuota = 1;
			participantRequirements.Add(requirement1);
			participantRequirements.Add(requirement2);

			return participantRequirements;
		}

		private List<Activity> getActivitiesToSchedule(int? i_MasterPlanID, FoxModelContainer _context)
		{

			List<Activity> activitiesToReturn = new List<Activity>();
			if (i_MasterPlanID == null) return activitiesToReturn;

			foreach (var act in _context.DWH_Activities.Where(a => a.Activity_EntityID == i_MasterPlanID))
			{
				Activity activity = new Activity();
				activity.ActivityID = act.Activity_ID;
				activity.ActivityName = act.Activity_Name;
				activity.ActivityDuration = act.Activity_Duration == null ? 0 : act.Activity_Duration.Value;
				activity.ActivityParticipantRequirements = getActivitieParticipantRequirements(act.Activity_ID, _context);
				activity.ActivityPrerequisites = getActivityPrerequisites(act.Activity_ID, _context);
				activity.ActivityResourceRequirements = getActivityResourceRequirements(act.Activity_ID, _context);

				activitiesToReturn.Add(activity);
			}

			return activitiesToReturn;

		}

		private List<TrainingRequest> getAdditioanlTrainingRequest(IEnumerable<AdditioanlTrainingRequest> i_AdditioanlRequests, FoxModelContainer _context)
		{
			//int trIDHash = ADDIRIOAN_REQUEST_START_ID;
			int trIDHash = new Random(DateTime.Now.Millisecond).Next(1000, 5000);
			var runningID = 0;

			List<TrainingRequest> requestsToReturn = new List<TrainingRequest>();

			foreach (var req in i_AdditioanlRequests)
			{
				for (int i = 1; i <= req.RequiredQuantity; i++)
				{
					TrainingRequest request = new TrainingRequest();
					request.TrainingRequestID = trIDHash + runningID;
					runningID++;
					request.MasterPlanID = req.RequiredMasterPlan;
					request.ParticipantRequirements = null;
					request.PreferredLocationID = 0;
					request.RequestedForUserID = null;
					request.RequestedPositionID = null;
					request.RequestPriority = req.Priorety + 1; //Ensure lower priorety than original requests
					request.RequestedPositionName = null;
					request.TrainingExpectedIncome = 0;
					request.TrainingRequestStartDateObject = req.RequestStartDateObject;
					request.TrainingRequestEndDateObject = req.RequestEndDateObject;
					request.TrainingRequestTitle = "Additioanl Requirement - " + i;
					request.MasterPlanCost = 0;
					request.MasterPlanRevenue = 0;

					requestsToReturn.Add(request);
				}
			}

			return requestsToReturn;
		}

		private OptimizationSettings getOptimizationSettings(FoxModelContainer _context)
		{
			OptimizationSettings settings = new OptimizationSettings();
			settings.OptimizationScheduleResolution = 60;
			settings.OptimizationGoals = new List<OptimizationGoal>();
			settings.OptimizationGoals.Add(new OptimizationGoal()
			{
				GoalTypeID = 1,
				GoalTypeDescription = "Maximize participant quota (pairing)",
				Priority = 1,

			});
			settings.OptimizationGoals.Add(new OptimizationGoal()
			{
				GoalTypeID = 2,
				GoalTypeDescription = "Maximize training troughtput",
				Priority = 2

			});
			settings.OptimizationGoals.Add(new OptimizationGoal()
			{
				GoalTypeID = 3,
				GoalTypeDescription = "Minimize target end date of training",
				GoalConstraintTypeID = 1,
				GoalConstraintTypeDescription = "Do not exceed",
				GoalConstraintTargetValue = "2014-04-30", // TODO: Get actual target end date.
				Priority = 3

			});
			settings.OptimizationGoals.Add(new OptimizationGoal()
			{
				GoalTypeID = 4,
				GoalTypeDescription = "Minimize resource utilization",
				GoalConstraintTypeID = 1,
				GoalConstraintTypeDescription = "Do not exceed",
				GoalConstraintTargetValue = "30%", // TODO: Get actual target end date.
				Priority = 4

			});


			return settings;
		}

		private void saveInputFile(string inputfilePath, string i_jsonData)
		{
			using (var inputFile = System.IO.File.CreateText(inputfilePath))
			{
				inputFile.Write(i_jsonData);
			}
		}

		private ProcessStartInfo getOptimizationUtilityProcessStartInfo(string i_InputfilePath, string i_OutputfilePath, string i_OptimizationScriptPath, string i_PythonPath, string i_MaxHoursPerDay, string i_MaxOffDays, string i_LogLevel)
		{
			//var psi = new ProcessStartInfo(i_PythonPath, String.Format("{0} --log_level {1} --max_hours_per_day {2} --max_off_days {3} {4} {5}", i_OptimizationScriptPath,i_LogLevel, i_MaxHoursPerDay, i_MaxOffDays, i_InputfilePath, i_OutputfilePath))
			var psi = new ProcessStartInfo(i_PythonPath, String.Format("{0} {1} {2}", i_OptimizationScriptPath, i_InputfilePath, i_OutputfilePath))
			{
				WorkingDirectory = Environment.CurrentDirectory,
				UseShellExecute = false,
				RedirectStandardOutput = true,
				RedirectStandardError = true,
				CreateNoWindow = true
			};
			return psi;
		}


		private static Dictionary<int, string> s_OptimizationVerbosProgress;
		private static int s_VerbosLineNumber;
		private static bool s_OptimizationProcessHasEnded;
		private object SyncRoot = new object();

		private void startOptimizationProcess(ProcessStartInfo psi)
		{
			var process = new Process { StartInfo = psi };
			process.EnableRaisingEvents = true;
			process.OutputDataReceived += new DataReceivedEventHandler(process_OutputDataReceived);
			process.ErrorDataReceived += new DataReceivedEventHandler(process_OutputDataReceived);
			process.Exited += new EventHandler(process_Exited);

			s_OptimizationProcessHasEnded = false;
			s_VerbosLineNumber = 0;
			s_OptimizationVerbosProgress = new Dictionary<int, string>();
			process.Start();
			process.BeginErrorReadLine();
			process.BeginOutputReadLine();
		}

		void process_OutputDataReceived(object sender, DataReceivedEventArgs e)
		{
			if (e.Data != null) // sometimes a random event is received with null data, not sure why - I prefer to leave it out
			{
				lock (SyncRoot)
				{
					Process p = sender as Process;
					s_VerbosLineNumber++;
					s_OptimizationVerbosProgress.Add(s_VerbosLineNumber, string.Format("<b>ProcessID({0})</b> {1}<br />", p.Id, e.Data));
				}
			}
		}

		void process_Exited(object sender, EventArgs e)
		{
			lock (SyncRoot)
			{
				s_OptimizationProcessHasEnded = true;
				Process p = sender as Process;
				p.Close();
			}
		}

		[NoCache]
		public JsonResult getAutomationProgress(int LastLineRead)
		{
			string retVal = "";
			int lastLineNumToReturn = 0;
			bool processHasEnded = false;
			lock (SyncRoot)
			{
				if (s_OptimizationVerbosProgress.Count > 0)
				{
					var linesToReturn = s_OptimizationVerbosProgress.Where(v => v.Key > LastLineRead).Select(l => string.Format("<b>{0:000}</b>:&nbsp;&nbsp;{1}", l.Key, l.Value));
					lastLineNumToReturn = s_OptimizationVerbosProgress.Keys.Last();
					retVal = string.Join("", linesToReturn);
					processHasEnded = s_OptimizationProcessHasEnded;
				}
			}
			return Json(new { verbosNewLine = retVal, processEnded = processHasEnded, verbosLastLineNum = lastLineNumToReturn }, JsonRequestBehavior.AllowGet);
		}

		private List<AutomaticSchedulingSettingsViewModel.AutomaticScheduleRequirement> getSchedulingRequirements(List<DWH_User_Certification> userCertificationData, FoxModelContainer _context)
		{
			var userCertIDs = userCertificationData.Select(uc => uc.Certification_ID).Distinct().ToList();
			var requiredCertifications = _context.DWH_Certification_Details_Main.Where(c => userCertIDs.Contains(c.Cert_ID)).ToList();
			var requiredTrainingForUsers = userCertificationData
				.Join(requiredCertifications, uc => uc.Certification_ID, c => c.Cert_ID, (uc, c) => new
				{
					UserID = uc.User_ID,
					UserName = uc.User_Name,
					CertificationID = uc.Certification_ID,
					PlannedAcquiringOrRenewalDate = uc.User_Certification_Final_Expiration_Date.HasValue ? (Nullable<DateTime>)uc.User_Certification_Final_Expiration_Date.Value.Date : null,
					CertificationStatus = uc.User_Certification_Status,
					ExpirationDate = uc.User_Certification_Final_Expiration_Date,
					MasterPlanID = ((new[] { 1, 8, 9, 10, 13 }).Contains(uc.User_Certification_Status.Value)) ? c.Cert_Acquiring_Master_Plan_ID : c.Cert_Renewal_Master_Plan_ID,
					MasterPlanName = ((new[] { 1, 8, 9, 10, 13 }).Contains(uc.User_Certification_Status.Value)) ? c.Cert_Acquiring_Master_Plan_Name : c.Cert_Renewal_Master_Plan_Name,
				}).ToList();

			var schedulingRequirements = requiredTrainingForUsers
				.Where(uc => uc.MasterPlanID.HasValue)
				.GroupBy(t => new { MasterPlanID = t.MasterPlanID, MasterPlanName = t.MasterPlanName },
				(key, g) => new AutomaticSchedulingSettingsViewModel.AutomaticScheduleRequirement
				{
					RequiringUsers = g.Select(u => new FoxUser { UserID = u.UserID, UserFirstName = u.UserName, UserLoginName = u.UserName }).ToList(),
					MasterPlan = new MasterPlan { MasterPlanID = key.MasterPlanID.Value, MasterPlanName = key.MasterPlanName },
					RequiredEndDate = g.Max(uc => uc.PlannedAcquiringOrRenewalDate),
					RequiredStartDate = g.Min(uc => uc.PlannedAcquiringOrRenewalDate),
					RequiredQuantity = g.Count()
				}).ToList();

			foreach (var item in schedulingRequirements)
			{
				item.RequiredStartDate = item.RequiredStartDate.HasValue ? new Nullable<DateTime>(new DateTime(item.RequiredStartDate.Value.Year, item.RequiredStartDate.Value.Month, 1)) : null;
				item.RequiredEndDate = item.RequiredEndDate.HasValue ? new Nullable<DateTime>(new DateTime(item.RequiredEndDate.Value.Year, item.RequiredEndDate.Value.Month, DateTime.DaysInMonth(item.RequiredEndDate.Value.Year, item.RequiredEndDate.Value.Month))) : null;
			}

			foreach (var req in schedulingRequirements)
			{
				var allTPs = _context.DWH_TrainingProgramsBasicData.ToList();
				var availableTrainingPrograms = new List<TrainingProgram>();
				availableTrainingPrograms.AddRange(allTPs
					.Where(tp => tp.TrainingProgramStartDate >= req.RequiredStartDate && tp.TrainingProgramEndDate <= req.RequiredEndDate)
					//.Where(tp=> tp.TrainingProgram_MasterPlanID.Value == req.MasterPlan.MasterPlanID) need to consider versioning of the master plans
					.Select(tp => new TrainingProgram
					{
						TrainingProgramID = tp.TrainingProgramID,
						MasterPlanID = tp.TrainingProgram_MasterPlanID,
						StartDate = tp.TrainingProgramStartDate,
						EndDate = tp.TrainingProgramEndDate,
						MasterPlanName = tp.TrainingProgram_MasterPlanName,
						TrainingProgramPlanName = tp.TrainingProgramName
					}).OrderBy(tp => tp.StartDate).ToList());
				req.AvailableTrainingPrograms = availableTrainingPrograms;

			}

			return schedulingRequirements;
		}

		private IEnumerable<ResourceType> getRelevantResourceTypes(IEnumerable<int> i_MasterPlanIDs, FoxModelContainer _context)
		{
			var allActivityIDs = _context.DWH_Activities.Where(a => i_MasterPlanIDs.Contains(a.Activity_EntityID)).Select(a => a.Activity_ID).ToList();
			var allResourceRequirements = _context.DWH_ResourcesRequirements.Where(rr => allActivityIDs.Contains(rr.ActivityID.Value)).ToList();
			var resourceTypesToReturn = allResourceRequirements
				.Select(rr => new ResourceType { TypeID = rr.RequirementResourceTypeID, TypeName = rr.RequirementResourceTypeName })
				.Distinct(new ResourceType.ResourceTypeComparer()).ToList();
			return resourceTypesToReturn;
		}

		public ActionResult RunAutomaticAssignemnt(AutomaticSchedulingSettingsViewModel i_Model)
		{
			AutomaticAssignemntsResultsViewModel model = new AutomaticAssignemntsResultsViewModel();
			model.UsersTrainingProgramRegistrations = new List<AutomaticAssignemntsResultsViewModel.UserTrainingProgramRegistration>();
			model.UsersWithNoTrainingProgram = new List<TrainingRequest>();

			var usedTPIDs = new List<int>();
			foreach (var req in i_Model.SchedulingRequirements)
			{
				if (req.RequiringUsers != null)
				{
					foreach (var user in req.RequiringUsers)
					{
						if (req.AvailableTrainingPrograms != null)
						{
							var unusedTPs = req.AvailableTrainingPrograms.Where(tp => !usedTPIDs.Contains(tp.TrainingProgramID)).ToList();
							if (unusedTPs.Count() > 0)
							{
								var UserTPPair = new AutomaticAssignemntsResultsViewModel.UserTrainingProgramRegistration();
								var tp = unusedTPs.First();
								UserTPPair.TrainingProgram = tp;
								UserTPPair.Users = user;

								usedTPIDs.Add(tp.TrainingProgramID);
								model.UsersTrainingProgramRegistrations.Add(UserTPPair);
							}
							else
							{
								model.UsersWithNoTrainingProgram.Add(new TrainingRequest
								{
									RequestedForUserID = user.UserID,
									RequestedForUserName = user.UserLoginName,
									MasterPlanID = req.MasterPlan.MasterPlanID,
									MasterPlanName = req.MasterPlan.MasterPlanName
								});
							}
						}
						else
						{
							model.UsersWithNoTrainingProgram.Add(new TrainingRequest
							{
								RequestedForUserID = user.UserID,
								RequestedForUserName = user.UserLoginName,
								MasterPlanID = req.MasterPlan.MasterPlanID,
								MasterPlanName = req.MasterPlan.MasterPlanName
							});
						}
					}
				}

			}

			using (var _context = new FoxModelContainer())
			{
				var unregisteredUserIDs = model.UsersWithNoTrainingProgram.Select(u => u.RequestedForUserID).ToList();
				var masterPlanIDs = model.UsersWithNoTrainingProgram.Select(u => u.MasterPlanID).ToList();
				var relevantCrtificationIDs = _context.DWH_Certification_Details_Main.Where(c => masterPlanIDs.Contains(c.Cert_Acquiring_Master_Plan_ID) || masterPlanIDs.Contains(c.Cert_Renewal_Master_Plan_ID)).Select(c => c.Cert_ID);

				model.UsersWithNoTrainingProgramCertifciationInfo = _context.DWH_User_Certifications
					.Where(uc => unregisteredUserIDs.Contains(uc.User_ID) && relevantCrtificationIDs.Contains(uc.Certification_ID))
					.ToList();
			}

			return View("AutomaticAssignemntsResults", model);
		}


	}
}

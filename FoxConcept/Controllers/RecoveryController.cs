﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using FoxConcept.Infrastructure;
using FoxConcept.Models;
using FoxConcept.Models.DALModels;
using FoxConcept.Models.ViewModels.Recovery;
using LinqToExcel;

namespace FoxConcept.Controllers
{
    public class RecoveryController : Controller
    {
        //
        // GET: /Recovery/

        FoxConceptEntities _FoxConceptDB = new FoxConceptEntities();
        FoxModelContainer _FoxDB = new FoxModelContainer();

        protected override void Dispose(bool disposing)
        {
            _FoxConceptDB.Dispose();
            _FoxDB.Dispose();
            base.Dispose(disposing);
        }

        public ActionResult SelectEventsForRecovery()
        {
            EventTableListViewModel model = new EventTableListViewModel();

            model.events = GetEvents(model.StartDate, model.EndDate);


            model.Tps = model.events.Select(e => new
            {
                e.TrainingProgramID,
                e.TrainingProgramName
            }).Distinct().Select(e => new EntityDropDown()
            {
                ID = e.TrainingProgramID,
                Name = e.TrainingProgramName
            }).ToList();

            model.Ous = model.events.Select(e => new { e.OUID, e.OUName }).Distinct().Select(e => new EntityDropDown()
            {
                ID = e.OUID,
                Name = e.OUName
            }).ToList();

            model.ActivityTypes =
                model.events.Select(e => new { e.EventType, e.EventTypeName }).Distinct().Select(e => new EntityDropDown()
                {
                    ID = e.EventType,
                    Name = e.EventTypeName
                }).ToList();


            model.HumanResouceTypes = GetHumanResources(model.events);

            model.PhysicalResourceTypes = GetPhysicalResources(model.events);


            return View(model);
        }


        [HttpPost]
        public ActionResult FilterEvents(EventTableListViewModel filters)
        {

            EventTableListViewModel model = new EventTableListViewModel();

            model.StartDate = filters.StartDate;
            model.EndDate = filters.EndDate;

            model.events = GetEvents(model.StartDate, model.EndDate);

            //Search by name
            if (filters.EventNameSearch != null)
            {
                model.events = model.events.Where(e => e.Name.Contains(filters.EventNameSearch)).ToList();
            }

            //Filter by OU
            if (filters.ou_codes != null && filters.ou_codes.Count() > 0)
            {
                model.events = model.events.Where(e => filters.ou_codes.Any(o => o == e.OUID)).ToList();
            }

            //Filter by TP
            if (filters.tps_code != null && filters.tps_code.Count() > 0)
            {
                model.events = model.events.Where(e => filters.tps_code.Any(o => o == e.TrainingProgramID)).ToList();
            }

            //Filter by Activity Type
            if (filters.act_code != null && filters.act_code.Count() > 0)
            {
                model.events = model.events.Where(e => filters.act_code.Any(o => o == e.EventType)).ToList();
            }


            model.Tps = model.events.Select(e => new
            {
                e.TrainingProgramID,
                e.TrainingProgramName
            }).Distinct().Select(e => new EntityDropDown()
            {
                ID = e.TrainingProgramID,
                Name = e.TrainingProgramName
            }).ToList();

            model.Ous = model.events.Select(e => new { e.OUID, e.OUName }).Distinct().Select(e => new EntityDropDown()
            {
                ID = e.OUID,
                Name = e.OUName
            }).ToList();

            model.ActivityTypes =
                model.events.Select(e => new { e.EventType, e.EventTypeName }).Distinct().Select(e => new EntityDropDown()
                {
                    ID = e.EventType,
                    Name = e.EventTypeName
                }).ToList();


            model.HumanResouceTypes = GetHumanResources(model.events);

            model.PhysicalResourceTypes = GetPhysicalResources(model.events);

            return View("SelectEventsForRecovery", model);
        }

        private List<EntityDropDown> GetHumanResources(IEnumerable<EventDetailsViewModel> events)
        {
            return (from rr in _FoxDB.DWH_ResourcesRequirements
                    join rs in _FoxDB.Bks_DWH_ResourceTypes on rr.RequirementResourceTypeID equals rs.ResourceMetaDataTypeID
                    join evt in _FoxDB.DWH_EventsBasicData on rr.EventID equals evt.Event_ID
                    where rs.ResourceTypeID == 2
                    select new { rr.RequirementResourceTypeID, rr.RequirementResourceTypeName, evt.Event_ID }).AsEnumerable().Where(r => events.Any(e => e.ID == r.Event_ID)).Select(r => new { r.RequirementResourceTypeID, r.RequirementResourceTypeName }).Distinct().Select(r => new EntityDropDown()
                    {
                        ID = r.RequirementResourceTypeID,
                        Name = r.RequirementResourceTypeName
                    }).ToList();
        }

        private List<EntityDropDown> GetPhysicalResources(IEnumerable<EventDetailsViewModel> events)
        {
            return (from rr in _FoxDB.DWH_ResourcesRequirements
                    join rs in _FoxDB.Bks_DWH_ResourceTypes on rr.RequirementResourceTypeID equals rs.ResourceMetaDataTypeID
                    join evt in _FoxDB.DWH_EventsBasicData on rr.EventID equals evt.Event_ID
                    where rs.ResourceTypeID == 1
                    select new { rr.RequirementResourceTypeID, rr.RequirementResourceTypeName, evt.Event_ID }).AsEnumerable().Where(r => events.Any(e => e.ID == r.Event_ID)).Select(r => new { r.RequirementResourceTypeID, r.RequirementResourceTypeName }).Distinct().Select(r => new EntityDropDown()
                    {
                        ID = r.RequirementResourceTypeID,
                        Name = r.RequirementResourceTypeName
                    }).ToList();
        }

        private List<EventDetailsViewModel> GetEvents(DateTime StartDate, DateTime EndDate)
        {
            return (from ev in _FoxDB.DWH_EventsBasicData
                    join act in _FoxDB.DWH_Activities on ev.EventActivity_ID
                        equals act.Activity_ID
                    join tp in _FoxDB.DWH_TrainingProgramsBasicData on ev.EventTrainingProgram_ID equals
                        tp.TrainingProgramID
                    join units in _FoxDB.Bks_DWH_OrganizationUnitsBasicData on tp.TrainingProgram_OrganizationalUnitID
                        equals units.OrganizationalUnitID
                    where ev.Event_StartTime >= StartDate && ev.Event_EndTime <= EndDate
                    orderby ev.Event_StartTime
                    select new EventDetailsViewModel()
                    {
                        ID = ev.Event_ID,
                        Name = ev.Event_Name,
                        EndTime = (DateTime)ev.Event_EndTime,
                        StartTime = (DateTime)ev.Event_StartTime,
                        OUID = units.OrganizationalUnitID,
                        OUName = units.OrganizationalUnitName,
                        TrainingProgramID = tp.TrainingProgramID,
                        TrainingProgramName = tp.TrainingProgramName,
                        EventType = (int)act.Activity_MetaDataTypeID,
                        EventTypeName = act.Activity_MetaDataTypeName,
                        Duration = (int)ev.Event_Duration
                    }).ToList();
        }
            
       [HttpPost]
        public ActionResult RecoverySettings(DateTime StartDate , DateTime EndDate, List<EventDetailsViewModel> events)
        {
           RecoverySettingsViewModel model = new RecoverySettingsViewModel();

           model.TargetStartDate = StartDate;
           model.TargetEndTime = EndDate;

           foreach (var resourceType in GetPhysicalResources(events).ToList())
           {
               ResouceAllocationSettings settings = new ResouceAllocationSettings();
               settings.ResourceID = resourceType.ID;
               settings.ResourceName = resourceType.Name;

               settings.PreferedResource =
                   _FoxDB.Resources_tbl.Where(r => r.MDTypeID == settings.ResourceID && r.ObjectID != null).Select(r => new EntityDropDown()
                   {

                       ID = (int)r.ObjectID,
                       Name = r.Name
                   }).ToList();

               settings.AlternativeResources = settings.PreferedResource.ToList();

               model.ResourceAllocations.Add(settings);
           }

           foreach (var resourceType in GetHumanResources(events).ToList())
           {
               ResouceAllocationSettings settings = new ResouceAllocationSettings();
               settings.ResourceID = resourceType.ID;
               settings.ResourceName = resourceType.Name;
               settings.PreferedResource =
                   _FoxDB.Resources_tbl.Where(r => r.MDTypeID == settings.ResourceID && r.ObjectID != null).Select(r => new EntityDropDown()
                   {
                       ID = (int)r.ObjectID,
                       Name = r.Name
                   }).ToList();

               settings.AlternativeResources = settings.PreferedResource.ToList();

               model.ResourceAllocations.Add(settings);
           }

           model.Goals = model.GetAvailableGoals();
           model.SchedulingBusinessRules = _FoxConceptDB.SchedulingBusinessRules.Where(b => b.Optional == true).ToList();

            return View(model);
        }


        public ActionResult GetRecoveryResults()
        {
            // Need to install: https://www.microsoft.com/en-us/download/confirmation.aspx?id=23734
            RecoveryResultsViewModel model = new RecoveryResultsViewModel();

            var excel = new ExcelQueryFactory();
            excel.FileName = AppConfig.RecoveryDB;

            model.results = (from x in excel.Worksheet<RecoveryResult>("Events") select x).ToList();

            return View(model);
        }
    }


}

﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FoxConcept.Models;
using FoxConcept.Models.DALModels;
using FoxConcept.Models.ViewModels.CostSettings;

namespace FoxConcept.Controllers
{
    public class CostSettingsController : Controller
    {
        //
        // GET: /CostSettings/

        FoxConceptEntities _FoxConceptDB = new FoxConceptEntities();
        FoxModelContainer _FoxDB = new FoxModelContainer();

        protected override void Dispose(bool disposing)
        {
            _FoxConceptDB.Dispose();
            _FoxDB.Dispose();
            base.Dispose(disposing);
        }

        public ActionResult GetCosts(int? EntityType)
        {
            if (EntityType != null)
            {
                switch ((eEntityType)EntityType)
                {
                    case eEntityType.Activity:
                        {
                            return RedirectToAction("GetActivitiesCosts");
                            break;
                        }
                    case eEntityType.HumanResource:
                        {
                            return RedirectToAction("GetHumanResourcesCosts");
                            break;
                        }
                    case eEntityType.PhysicalResource:
                        {
                            return RedirectToAction("GetPhysicalResourcesCosts");
                            break;
                        }
                    case eEntityType.Participant:
                        {
                            return RedirectToAction("GetParticipantsCosts");
                            break;
                        }
                }
            }
            return View();
        }

        public ActionResult GetActivitiesCosts()
        {
            return View();
        }

        public ActionResult GetHumanResourcesCosts()
        {
            CostSettingsViewModel model = new CostSettingsViewModel();
            model.CostSettingViewModels =
                _FoxConceptDB.EntitiesCostSettings.Where(c => c.EntityType == ((int)eEntityType.HumanResource)).AsEnumerable()
                             .Select(c => new CostSettingViewModel
                                 {
                                     ID = c.ID,
                                     StateType = (eStateType)c.StateType,
                                     EntityType = eEntityType.HumanResource,
                                     Name = c.Name,
                                     TargetGroupType = (eTargetGroupType)c.TargetGroupType,
                                     ConsiderInOptimization = (bool)c.ConsiderInOptimization,
                                     Cost = (decimal)c.Cost,
                                     CostPer = (eCostPer)c.CostPer,
                                     CostType = (eCostType)c.CostType,
                                     EntityTypeName = c.EntityTypeName,
                                     EntityTypeID = c.EntityTypeID,
                                     Comments = c.Comments
                                 }).ToList();

            return View(model);
        }

        public ActionResult GetPhysicalResourcesCosts()
        {
            CostSettingsViewModel model = new CostSettingsViewModel();
            model.CostSettingViewModels =
                _FoxConceptDB.EntitiesCostSettings.Where(c => c.EntityType == ((int)eEntityType.PhysicalResource)).AsEnumerable()
                             .Select(c => new CostSettingViewModel
                             {
                                 ID = c.ID,
                                 StateType = (eStateType)c.StateType,
                                 EntityType = eEntityType.HumanResource,
                                 Name = c.Name,
                                 TargetGroupType = (eTargetGroupType)c.TargetGroupType,
                                 ConsiderInOptimization = (bool)c.ConsiderInOptimization,
                                 Cost = (decimal)c.Cost,
                                 CostPer = (eCostPer)c.CostPer,
                                 CostType = (eCostType)c.CostType,
                                 EntityTypeName = c.EntityTypeName,
                                 EntityTypeID = c.EntityTypeID,
                                 Comments = c.Comments
                             }).ToList();

            return View(model);
        }

        public ActionResult GetParticipantsCosts()
        {
            CostSettingsViewModel model = new CostSettingsViewModel();
            model.CostSettingViewModels =
                _FoxConceptDB.EntitiesCostSettings.Where(c => c.EntityType == ((int)eEntityType.Participant)).AsEnumerable()
                             .Select(c => new CostSettingViewModel
                             {
                                 ID = c.ID,
                                 StateType = (eStateType)c.StateType,
                                 EntityType = eEntityType.HumanResource,
                                 Name = c.Name,
                                 TargetGroupType = (eTargetGroupType) c.TargetGroupType,
                                 ConsiderInOptimization = (bool)c.ConsiderInOptimization,
                                 Cost = (decimal)c.Cost,
                                 CostPer = (eCostPer)c.CostPer,
                                 CostType = (eCostType)c.CostType,
                                 EntityTypeName = c.EntityTypeName,
                                 LocationID = (int)c.LocationID,
                                 EntityTypeID = c.EntityTypeID,
                                 Comments = c.Comments
                             }).ToList();

            return View(model);
        }

        public ActionResult GetCostTypes()
        {
            IEnumerable<CostTypeViewModel> model = _FoxConceptDB.CostTypesSettings.Select(c => new CostTypeViewModel()
                {
                    ID = c.ID,
                    Name = c.Name,
                    CostType = (eCostType)c.CostType,
                    Cost = c.Cost,
                    LocationID = (int)c.LocationID,
                    LocationName = c.LocationName,
                    CostPer = (eCostPer)c.CostPer
                }).ToList();

            return View(model);
        }

        #region Humanresource
        public ActionResult AddHumanResourceCost()
        {
            CostSettingViewModel model = new CostSettingViewModel();
            model.EntityType = eEntityType.HumanResource;
            model.EntityTypes =
                _FoxDB.DWH_ResourcesBasicData.Where(r => r.ResourceType == 2).Select(r => new EntityType()
                          {
                              ID = r.ResourceMetaDataTypeID,
                              Name = r.ResourceMetaDataTypeName
                          }).Distinct().OrderBy(r => r.Name).ToList();

            model.Locations = GetLocations();



            return View(model);
        }

        private List<EntityType> GetLocations()
        {
            return _FoxDB.BKS_DWH_Locations.Select(l => new EntityType()
            {
                ID = l.LocationID,
                Name = l.LocationName
            }).ToList();
        }

        public ActionResult EditHumanResourceCost(int ID)
        {
            CostSettingViewModel model = new CostSettingViewModel();
            model.EntityType = eEntityType.HumanResource;
            model.EntityTypes =
                _FoxDB.DWH_ResourcesBasicData.Where(r => r.ResourceType == 2).Select(r => new EntityType()
                {
                    ID = r.ResourceMetaDataTypeID,
                    Name = r.ResourceMetaDataTypeName
                }).Distinct().OrderBy(r => r.Name).ToList();

            model.Locations = GetLocations();

            var _entity = _FoxConceptDB.EntitiesCostSettings.FirstOrDefault(e => e.ID == ID);

            model.ID = _entity.ID;
            model.StateType = (eStateType)_entity.StateType;
            model.Name = _entity.Name;
            model.TargetGroupType = (eTargetGroupType)_entity.TargetGroupType;
            model.Cost = (decimal)_entity.Cost;
            model.Comments = _entity.Comments;
            model.ConsiderInOptimization = (bool)_entity.ConsiderInOptimization;
            model.CostPer = (eCostPer)_entity.CostPer;
            model.CostType = (eCostType)_entity.CostType;
            model.EntityTypeID = _entity.EntityTypeID;
            model.EntityTypeName = _entity.EntityTypeName;
            model.LocationID = (int)_entity.LocationID;
            model.LocationName = _entity.LocationName;

            return View(model);
        }

        [HttpPost]
        public ActionResult EditHumanResourceCost(CostSettingViewModel model)
        {
            var _entity = _FoxConceptDB.EntitiesCostSettings.FirstOrDefault(e => e.ID == model.ID);

            _entity.EntityType = (int)eEntityType.HumanResource;
            _entity.EntityTypeID = model.EntityTypeID;

            _entity.EntityTypeName =
                _FoxDB.DWH_ResourcesBasicData.Where(r => r.ResourceMetaDataTypeID == model.EntityTypeID)
                      .FirstOrDefault()
                      .ResourceMetaDataTypeName;
            _entity.StateType = (int)model.StateType;
            _entity.TargetGroupType = (int)model.TargetGroupType;
            _entity.Name = model.Name;
            _entity.Cost = model.Cost;
            _entity.CostType = (int)model.CostType;
            _entity.CostPer = (int)model.CostPer;
            _entity.ConsiderInOptimization = model.ConsiderInOptimization;
            _entity.Comments = model.Comments;
            _entity.LocationID = model.LocationID;
            _entity.LocationName = _FoxDB.BKS_DWH_Locations.FirstOrDefault(l => l.LocationID == model.LocationID).LocationName;

            _FoxConceptDB.EntitiesCostSettings.AddOrUpdate(_entity);

            int resutls = _FoxConceptDB.SaveChanges();

            return RedirectToAction("EditHumanResourceCost", new { ID = _entity.ID });
        }

        [HttpPost]
        public ActionResult AddHumanResourceCost(CostSettingViewModel model)
        {
            if (ModelState.IsValid)
            {
                EntitiesCostSetting _entity = new EntitiesCostSetting();
                _entity.EntityType = (int)eEntityType.HumanResource;
                _entity.EntityTypeID = model.EntityTypeID;
                _entity.StateType = (int) model.StateType;
                _entity.EntityTypeName =
                    _FoxDB.DWH_ResourcesBasicData.Where(r => r.ResourceMetaDataTypeID == model.EntityTypeID)
                          .FirstOrDefault()
                          .ResourceMetaDataTypeName;

                _entity.Name = model.Name;
                _entity.TargetGroupType = (int) model.TargetGroupType;
                _entity.Cost = model.Cost;
                _entity.CostType = (int)model.CostType;
                _entity.CostPer = (int)model.CostPer;
                _entity.ConsiderInOptimization = model.ConsiderInOptimization;
                _entity.Comments = model.Comments;
                _entity.LocationID = model.LocationID;
                _entity.LocationName = _FoxDB.BKS_DWH_Locations.FirstOrDefault(l => l.LocationID == model.LocationID).LocationName;


                _FoxConceptDB.EntitiesCostSettings.Add(_entity);

                int results = _FoxConceptDB.SaveChanges();

                return RedirectToAction("EditHumanResourceCost", new { ID = _entity.ID });
            }

            model.EntityType = eEntityType.HumanResource;
            model.EntityTypes =
                _FoxDB.DWH_ResourcesBasicData.Where(r => r.ResourceType == 2).Select(r => new EntityType()
                {
                    ID = r.ResourceMetaDataTypeID,
                    Name = r.ResourceMetaDataTypeName
                }).Distinct().OrderBy(r => r.Name).ToList();

            return View(model);
        }

        public ActionResult DeleteHumanResourceCost(int ID)
        {
            EntitiesCostSetting _entity = _FoxConceptDB.EntitiesCostSettings.FirstOrDefault(e => e.ID == ID);

            _FoxConceptDB.EntitiesCostSettings.Remove(_entity);

            int results = _FoxConceptDB.SaveChanges();

            return RedirectToAction("GetHumanResourcesCosts");


        }

        # endregion

        #region Physicalresource
        public ActionResult AddPhysicalResourceCost()
        {
            CostSettingViewModel model = new CostSettingViewModel();
            model.EntityType = eEntityType.PhysicalResource;
            model.EntityTypes =
                _FoxDB.DWH_ResourcesBasicData.Where(r => r.ResourceType == 1).Select(r => new EntityType()
                {
                    ID = r.ResourceMetaDataTypeID,
                    Name = r.ResourceMetaDataTypeName
                }).Distinct().OrderBy(r => r.Name).ToList();

            return View(model);
        }

        public ActionResult EditPhysicalResourceCost(int ID)
        {
            CostSettingViewModel model = new CostSettingViewModel();
            model.EntityType = eEntityType.PhysicalResource;
            model.EntityTypes =
                _FoxDB.DWH_ResourcesBasicData.Where(r => r.ResourceType == 1).Select(r => new EntityType()
                {
                    ID = r.ResourceMetaDataTypeID,
                    Name = r.ResourceMetaDataTypeName
                }).Distinct().OrderBy(r => r.Name).ToList();

            var _entity = _FoxConceptDB.EntitiesCostSettings.FirstOrDefault(e => e.ID == ID);

            model.ID = _entity.ID;
            model.StateType =(eStateType) _entity.StateType;
            model.Name = _entity.Name;
            model.TargetGroupType = (eTargetGroupType)_entity.TargetGroupType;
            model.Cost = (decimal)_entity.Cost;
            model.Comments = _entity.Comments;
            model.ConsiderInOptimization = (bool)_entity.ConsiderInOptimization;
            model.CostPer = (eCostPer)_entity.CostPer;
            model.CostType = (eCostType)_entity.CostType;
            model.EntityTypeID = _entity.EntityTypeID;
            model.EntityTypeName = _entity.EntityTypeName;

            return View(model);
        }

        [HttpPost]
        public ActionResult EditPhysicalResourceCost (CostSettingViewModel model)
        {
            var _entity = _FoxConceptDB.EntitiesCostSettings.FirstOrDefault(e => e.ID == model.ID);

            _entity.EntityType = (int)eEntityType.PhysicalResource;
            _entity.EntityTypeID = model.EntityTypeID;
            _entity.EntityTypeName =
                _FoxDB.DWH_ResourcesBasicData.Where(r => r.ResourceMetaDataTypeID == model.EntityTypeID)
                      .FirstOrDefault()
                      .ResourceMetaDataTypeName;

            _entity.StateType = (int)model.StateType;
            _entity.TargetGroupType = (int)model.TargetGroupType;
            _entity.Name = model.Name;
            _entity.Cost = model.Cost;
            _entity.CostType = (int)model.CostType;
            _entity.CostPer = (int)model.CostPer;
            _entity.ConsiderInOptimization = model.ConsiderInOptimization;
            _entity.Comments = model.Comments;

            _FoxConceptDB.EntitiesCostSettings.AddOrUpdate(_entity);

            int resutls = _FoxConceptDB.SaveChanges();

            return RedirectToAction("EditPhysicalResourceCost", new { ID = _entity.ID });
        }

        [HttpPost]
        public ActionResult AddPhysicalResourceCost(CostSettingViewModel model)
        {
            if (ModelState.IsValid)
            {
                EntitiesCostSetting _entity = new EntitiesCostSetting();
                _entity.EntityType = (int)eEntityType.PhysicalResource;
                _entity.EntityTypeID = model.EntityTypeID;

                _entity.EntityTypeName =
                    _FoxDB.DWH_ResourcesBasicData.Where(r => r.ResourceMetaDataTypeID == model.EntityTypeID)
                          .FirstOrDefault()
                          .ResourceMetaDataTypeName;
                _entity.StateType = (int)model.StateType;
                _entity.Name = model.Name;
                _entity.TargetGroupType = (int) model.TargetGroupType;
                _entity.Cost = model.Cost;
                _entity.CostType = (int)model.CostType;
                _entity.CostPer = (int)model.CostPer;
                _entity.ConsiderInOptimization = model.ConsiderInOptimization;
                _entity.Comments = model.Comments;

                _FoxConceptDB.EntitiesCostSettings.Add(_entity);

                int results = _FoxConceptDB.SaveChanges();

                return RedirectToAction("EditPhysicalResourceCost", new { ID = _entity.ID });
            }

            model.EntityType = eEntityType.PhysicalResource;
            model.EntityTypes =
                _FoxDB.DWH_ResourcesBasicData.Where(r => r.ResourceType == 1).Select(r => new EntityType()
                {
                    ID = r.ResourceMetaDataTypeID,
                    Name = r.ResourceMetaDataTypeName
                }).Distinct().OrderBy(r => r.Name).ToList();

            return View(model);
        }

        public ActionResult DeletePhisicalResourceCost(int ID)
        {
            EntitiesCostSetting _entity = _FoxConceptDB.EntitiesCostSettings.FirstOrDefault(e => e.ID == ID);

            _FoxConceptDB.EntitiesCostSettings.Remove(_entity);

            int results = _FoxConceptDB.SaveChanges();

            return RedirectToAction("GetPhysicalResourcesCosts");


        }

        # endregion

        #region Participant
        public ActionResult AddParticipantCost()
        {
            CostSettingViewModel model = new CostSettingViewModel();
            model.EntityType = eEntityType.Participant;
            model.EntityTypes = GetParticipantType();
            model.Locations.AddRange(GetLocations());


            return View(model);
        }

        private List<EntityType> GetParticipantType()
        {
            return _FoxDB.DWH_UsersExtraDetails.Where(u => u.UserMetaDataTypeID > 0).Select(u => new EntityType()
            {
                ID = (int)u.UserMetaDataTypeID,
                Name = u.UserMetaDataTypeName
            }).Distinct().OrderBy(r => r.Name).ToList();
        }

        public ActionResult EditParticipantCost(int ID)
        {
            CostSettingViewModel model = new CostSettingViewModel();
            model.EntityType = eEntityType.Participant;
            model.EntityTypes = GetParticipantType();
            model.Locations.AddRange(GetLocations());
            var _entity = _FoxConceptDB.EntitiesCostSettings.FirstOrDefault(e => e.ID == ID);

            model.ID = _entity.ID;
            model.StateType = (eStateType)_entity.StateType;
            model.Name = _entity.Name;
            model.TargetGroupType = (eTargetGroupType) _entity.TargetGroupType;
            model.Cost = (decimal)_entity.Cost;
            model.Comments = _entity.Comments;
            model.ConsiderInOptimization = (bool)_entity.ConsiderInOptimization;
            model.CostPer = (eCostPer)_entity.CostPer;
            model.CostType = (eCostType)_entity.CostType;
            model.EntityTypeID = _entity.EntityTypeID;
            model.EntityTypeName = _entity.EntityTypeName;
            model.LocationID = (int)_entity.LocationID;

            return View(model);
        }

        [HttpPost]
        public ActionResult EditParticipantCost(CostSettingViewModel model)
        {
            var _entity = _FoxConceptDB.EntitiesCostSettings.FirstOrDefault(e => e.ID == model.ID);

            _entity.EntityType = (int)eEntityType.Participant;
            _entity.EntityTypeID = model.EntityTypeID;
            _entity.EntityTypeName = _FoxDB.DWH_UsersExtraDetails.Where(u => u.UserMetaDataTypeID == model.EntityTypeID).FirstOrDefault().UserMetaDataTypeName;
            _entity.StateType = (int)model.StateType;
            _entity.TargetGroupType = (int)model.TargetGroupType;
            _entity.Name = model.Name;
            _entity.Cost = model.Cost;
            _entity.CostType = (int)model.CostType;
            _entity.CostPer = (int)model.CostPer;
            _entity.ConsiderInOptimization = model.ConsiderInOptimization;
            _entity.Comments = model.Comments;
            _entity.LocationID = model.LocationID;
            _entity.LocationName = _FoxDB.BKS_DWH_Locations.FirstOrDefault(l => l.LocationID == model.LocationID).LocationName;

            _FoxConceptDB.EntitiesCostSettings.AddOrUpdate(_entity);

            int resutls = _FoxConceptDB.SaveChanges();

            return RedirectToAction("EditParticipantCost", new { ID = _entity.ID });
        }

        [HttpPost]
        public ActionResult AddParticipantCost(CostSettingViewModel model)
        {
            if (ModelState.IsValid)
            {
                EntitiesCostSetting _entity = new EntitiesCostSetting();
                _entity.EntityType = (int)eEntityType.Participant;
                _entity.EntityTypeID = model.EntityTypeID;
                _entity.StateType = (int)model.StateType;
                _entity.EntityTypeName =
                    _FoxDB.DWH_UsersExtraDetails.Where(r => r.UserMetaDataTypeID == model.EntityTypeID)
                          .FirstOrDefault()
                          .UserMetaDataTypeName;
                _entity.TargetGroupType = (int)model.TargetGroupType;
                _entity.Name = model.Name;
                _entity.Cost = model.Cost;
                _entity.CostType = (int)model.CostType;
                _entity.CostPer = (int)model.CostPer;
                _entity.ConsiderInOptimization = model.ConsiderInOptimization;
                _entity.Comments = model.Comments;
                _entity.LocationID = model.LocationID;
                _entity.LocationName =
                    _FoxDB.BKS_DWH_Locations.FirstOrDefault(l => l.LocationID == model.LocationID).LocationName;

                _FoxConceptDB.EntitiesCostSettings.Add(_entity);

                int results = _FoxConceptDB.SaveChanges();

                return RedirectToAction("EditParticipantCost", new { ID = _entity.ID });
            }

            model.EntityType = eEntityType.Participant;
            model.EntityTypes = GetParticipantType();

            return View(model);
        }

        public ActionResult DeleteParticipantCost(int ID)
        {
            EntitiesCostSetting _entity = _FoxConceptDB.EntitiesCostSettings.FirstOrDefault(e => e.ID == ID);

            _FoxConceptDB.EntitiesCostSettings.Remove(_entity);

            int results = _FoxConceptDB.SaveChanges();

            return RedirectToAction("GetParticipantsCosts");


        }

        # endregion

        #region CostTypes

        public ActionResult AddCostType()
        {
            CostTypeViewModel model = new CostTypeViewModel();

            model.Locations.AddRange(GetLocations());

            return View(model);
        }

        [HttpPost]
        public ActionResult AddCostType(CostTypeViewModel model)
        {
            if (ModelState.IsValid)
            {
                CostTypesSetting _cost = new CostTypesSetting();
                _cost.Name = model.Name;
                _cost.CostType = (int) model.CostType;
                _cost.Cost = model.Cost;
                _cost.CostPer = (int) model.CostPer;
                _cost.LocationID = model.LocationID;
                _cost.LocationName =
                    _FoxDB.BKS_DWH_Locations.FirstOrDefault(l => l.LocationID == model.LocationID).LocationName;
                
                _FoxConceptDB.CostTypesSettings.Add(_cost);

                int results = _FoxConceptDB.SaveChanges();

                return RedirectToAction("EditCostType", new {ID = _cost.ID});
            }

            model.Locations.AddRange(GetLocations());

            return View(model);
        }

        public ActionResult EditCostType(int ID)
        {
            var _cost = _FoxConceptDB.CostTypesSettings.FirstOrDefault(c => c.ID == ID);

            CostTypeViewModel model = new CostTypeViewModel();
            model.Locations.AddRange(GetLocations());

            model.ID = _cost.ID;
            model.Cost = _cost.Cost;
            model.CostType = (eCostType) _cost.CostType;
            model.LocationID = (int)_cost.LocationID;
            model.CostPer = (eCostPer) _cost.CostPer;
            model.Name = _cost.Name;

            return View(model);


        }

        public ActionResult DeleteCostType(int ID)
        {
            var _cost = _FoxConceptDB.CostTypesSettings.FirstOrDefault(c => c.ID == ID);
            _FoxConceptDB.CostTypesSettings.Remove(_cost);

            int results = _FoxConceptDB.SaveChanges();

            return RedirectToAction("GetCostTypes");
        }

        #endregion

    }
}

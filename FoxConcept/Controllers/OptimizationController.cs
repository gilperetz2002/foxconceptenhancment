﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Http;

using System.Net.Http.Headers;
using FoxConcept.Models;
using FoxConcept.Models.DALModels;
using FoxConcept.Models.ViewModels.Optimization;
using Newtonsoft.Json.Linq;
using System.Xml;
using System.IO;
using Newtonsoft.Json;
using System.Diagnostics;
using System.Reflection;
using System.Web.Script.Serialization;
using FoxConcept.Infrastructure.Attributes;
using FoxConcept.Models.ViewModels.Shared;

namespace FoxConcept.Controllers
{
	public class OptimizationController : Controller
	{
		public const int ADDIRIOAN_REQUEST_START_ID = 100000000;
		public const string GLOBAL_DATE_TIME_FORMAT = "yyy-MM-dd HH:mm";
		public const string GLOBAL_DATE_ONLY_FORMAT = "yyy-MM-dd";

		public ActionResult SelectPlansToCompare()
		{
			IEnumerable<string> model;

			model = Directory.EnumerateFiles(ControllerContext.HttpContext.Server.MapPath("~/App_Data/"))
				.Select(f=>Path.GetFileName(f)).Where(f=>f.Contains("output"));

			return View(model);
		}
		public ActionResult OptimizationSettingsRelevantResources(IEnumerable<int?> RelevantMasterPlanIDs)
		{
            IEnumerable<ResourceType> model;
			if (RelevantMasterPlanIDs == null) return PartialView("_OptimizationSettingsResourceList", new List<ResourceType>());
			using (var _context = new FoxModelContainer())
			{
				model = getRelevantResourceTypes(RelevantMasterPlanIDs.Distinct(), _context);
                
			}
			return PartialView("_OptimizationSettingsResourceList", model);
		}

        public ActionResult OptimizationSettingsRelevantResourcesList(IEnumerable<int> RelevantMasterPlanIDs)
        {
            List<int?> list = new List<int?>();

            foreach (var item in RelevantMasterPlanIDs)
            {
                list.Add(item);
            }
            IEnumerable<ResourceType> model;
            if (RelevantMasterPlanIDs == null) return PartialView("_OptimizationSettingsResourceList", new List<ResourceType>());
            using (var _context = new FoxModelContainer())
            {
                model = getRelevantResourceTypes(list.Distinct(), _context);
            }
            return PartialView("_OptimizationSettingsResourceList", model);
        }

		[HttpPost]
		public ActionResult RunOptimizator(OptimizationSettingsViewModel i_Settings)
		{
            Session.Remove("OptimizationGoals");
            //Save in sesstion the Optimization Goals
            Session.Add("OptimizationGoals",i_Settings.Goals);

            

			RunOptimizationViewModel model = new RunOptimizationViewModel();
			var inputModel = getOptimizationModel(i_Settings);
			var jsonInput = new JavaScriptSerializer().Serialize(inputModel.Data);
			var jobID = DateTime.Now.ToString("yyy-MM-ddTHH-mm-fffffff");

			var inputfilePath = ControllerContext.HttpContext.Server.MapPath(string.Format("~/App_Data/input_{0}.json", jobID));
			saveInputFile(inputfilePath, jsonInput);
			var outputfilePath = ControllerContext.HttpContext.Server.MapPath(string.Format("~/App_Data/output_{0}.json", jobID));

			var maxOffDays = System.Configuration.ConfigurationManager.AppSettings["MaxOffDays"];
			var maxHoursPerDay = System.Configuration.ConfigurationManager.AppSettings["MaxHoursPerDay"];
			var debugLevel = System.Configuration.ConfigurationManager.AppSettings["OptimizationLogLevel"];
			var optimizationScriptPath = System.Configuration.ConfigurationManager.AppSettings["OptimizationUtility"];
			var pythonPath = System.Configuration.ConfigurationManager.AppSettings["PythonCompilerPath"];
			var psi = getOptimizationUtilityProcessStartInfo(inputfilePath, outputfilePath, optimizationScriptPath, pythonPath, maxHoursPerDay, maxOffDays, debugLevel);

			startOptimizationProcess(psi);
			model.OptimizationInputFileName = inputfilePath;
			model.OptimizationOutputFileName = outputfilePath;
			return View(model);
		}

		public ActionResult CompareOptimizedPlan(IEnumerable<string> InputFilePaths)
		{
			ShowPlanViewModel[] model = new ShowPlanViewModel[2];
			var pathToFile = ControllerContext.HttpContext.Server.MapPath("~/App_Data/");
			model[0] = getPlanViewModel(pathToFile + InputFilePaths.ElementAt(0));
			model[1] = getPlanViewModel(pathToFile + InputFilePaths.ElementAt(1));

            //Load Table summary for each plan
		    model[0].SummaryTableStatistic = GetPlanSummary(1, ref model[0]);
		    model[1].SummaryTableStatistic = GetPlanSummary(2, ref model[1]);

			return View(model);
		}

        private PlanSummaryTable GetPlanSummary(int PlanID, ref ShowPlanViewModel showPlanViewModel)
        {
            PlanSummaryTable model = new PlanSummaryTable();

            model.LoadOptimizationPlanSummary(PlanID);

            //model.ScheduledRequests =
            //    showPlanViewModel.RequiredMasterPlanOptimizationStatistics.Sum(m => m.ScheduledRequests);

            //model.UnscheduledRequests =
            //    showPlanViewModel.RequiredMasterPlanOptimizationStatistics.Sum(m => m.UnscheduledRequests);

            //model.Revenue = showPlanViewModel.RequiredMasterPlanOptimizationStatistics.Sum(m => m.TotalRevenue);

            //model.TotalCost = showPlanViewModel.RequiredMasterPlanOptimizationStatistics.Sum(m => m.TotalCost);

            

            return model;
        }

	    public ActionResult ShowPlanSummary(int PlanID)
	    {
	        PlanSummaryTable model = new PlanSummaryTable();

	        model.LoadOptimizationPlanSummary(PlanID);

	        return View(model);
	    }

		public ActionResult ShowOptimizedPlan(string InputFilePath, string DisplayMode)
		{
            

			ShowPlanViewModel model = getPlanViewModel(InputFilePath);
			ViewResult viewToReturn = DisplayMode == "Table" ? View("ShowOptimizedPlanTable", model) : View("ShowOptimizedPlan", model);

            // Get Optimization Goals
            if (Session["OptimizationGoals"] != null)
		    {
		        model.Goals = new List<OptimizationGoal>();
		        model.Goals.AddRange((Session["OptimizationGoals"]) as IEnumerable<OptimizationGoal>);
		    }

			return viewToReturn;
		}

		private ShowPlanViewModel getPlanViewModel(string InputFilePath)
		{
			ShowPlanViewModel modelToReturn = new ShowPlanViewModel();
			modelToReturn.InputFilePath = InputFilePath;
			modelToReturn.PlanID = DateTime.Now.Ticks;
			modelToReturn.PlanName = InputFilePath;
			using (StreamReader reader = System.IO.File.OpenText(InputFilePath))
			{
				JObject jsonPlan = (JObject)JToken.ReadFrom(new JsonTextReader(reader));

				JArray jsonTrainingPrograms = (JArray)jsonPlan["TrainingPrograms"];
				modelToReturn.TrainingPrograms = getTrainingProgramsFromJSON(jsonTrainingPrograms).OrderBy(tp=>tp.OriginalRequestID);
													
			}

			using (var _context = new FoxModelContainer())
			{
				modelToReturn.RequiredMasterPlanOptimizationStatistics = getMasterPlanOptimizationStatistics(modelToReturn.TrainingPrograms.Where(tr => tr.OriginalRequestID < ADDIRIOAN_REQUEST_START_ID));
				modelToReturn.AdditionalMasterPlanOptimizationStatistics = getMasterPlanOptimizationStatistics(modelToReturn.TrainingPrograms.Where(tr => tr.OriginalRequestID >= ADDIRIOAN_REQUEST_START_ID));

				modelToReturn.GanntItems = modelToReturn.TrainingPrograms.Where(tp => tp.isScheduled).OrderBy(tr=>tr.AnsweredRequests.Count()).Select(tp => new GanttItem()
				{
					GroupName = getTPGroupName(tp),
					Content = string.Format("<div data-tooltip='{3}'><div class='GantItemMainData'>{0}<div/><div class='GanttItemSecondaryData'></div></div>", tp.MasterPlanName, getRequestPosition(tp, 1), getRequestPosition(tp, 2), getEventsTooltip(tp.Events, _context)),
					StartDate = tp.StartDate,
					EndDate = tp.EndDate,
					ClassName = "mp-color-" + tp.MasterPlanID.ToString()
				}).ToList();

				modelToReturn.UtilizationItems = getResourceUtilizationForPlan(modelToReturn);
			}

			return modelToReturn;
		}

		private string getTPGroupName(TrainingProgram tp)
		{
			string groupName;
			if (tp.OriginalRequestID >= ADDIRIOAN_REQUEST_START_ID)
				groupName = string.Format("<i style='display:none;'>({0})</i> {1} {2}", tp.OriginalRequestID,tp.MasterPlanName, (tp.OriginalRequestID - ADDIRIOAN_REQUEST_START_ID)+1);
			else
				groupName = string.Format("<i style='display:none;'>({0})</i> {1} {2}", tp.OriginalRequestID, getRequestPosition(tp, 1), getRequestPosition(tp, 2));
				
			return groupName;
		}

		private string getEventsTooltip(List<FoxEvent> i_Events, FoxModelContainer _context)
		{
			string eventsTooltip = "";
			var requiredActivityIDs = i_Events.Select(ev => ev.ActivityID).ToList();
			var relevantActivities = _context.DWH_Activities.Where(ac => requiredActivityIDs.Contains(ac.Activity_ID)).ToList();
			eventsTooltip += "<table>";
			eventsTooltip += "<thead><tr><th>Schedled Time</th><th>Event Name</th></tr></thead>";
			eventsTooltip += "<tbody>";
			foreach (var ev in i_Events.OrderBy(ev => ev.StartDate))
			{
				var activityName = relevantActivities.SingleOrDefault(ac => ac.Activity_ID == ev.ActivityID).Activity_Name;
				eventsTooltip += string.Format("<tr><td>{1} - {2}</td> <td>{0}</td></tr>", activityName, ev.StartDate.ToString("MM/dd/yyy HH:mm"), ev.EndDate.ToString("HH:mm"));
			}
			eventsTooltip += "</tbody>";
			eventsTooltip += "</table>";

			return eventsTooltip;
		}

		private IEnumerable<MasterPlanOptimizationStatistics> getMasterPlanOptimizationStatistics(IEnumerable<TrainingProgram> i_TrainingPrograms)
		{
			var optimizationStatistics = i_TrainingPrograms.GroupBy(
				t => new { MasterPlanID = t.MasterPlanID.Value, MasterPlanName = t.MasterPlanName },
				tr => new { isScheduled = tr.isScheduled, answeredRequests = tr.AnsweredRequests, trainingCost = tr.MasterPlanCost, trainingRevenue = tr.MasterPlanRevenue },
				(key, g) => new MasterPlanOptimizationStatistics()
				{
					MasterPlanID = key.MasterPlanID,
					MasterPlanName = key.MasterPlanName,
					ScheduledRequests = g.Where(i => i.isScheduled).SelectMany(t => t.answeredRequests).Count(),
					UnscheduledRequests = g.Where(i => !i.isScheduled).SelectMany(t => t.answeredRequests).Count(),
					PairedRequests = g.Where(i => i.isScheduled).Where(t => t.answeredRequests.Count() == 2).Count() * 2,
					UnpairedRequests = g.Where(i => i.isScheduled).Where(t => t.answeredRequests.Count() == 1).Count(),
					UnpairedCaptains = g.Where(i => i.isScheduled).Where(t => t.answeredRequests.Count() == 1).SelectMany(t => t.answeredRequests).Where(r => r.RequestedPositionID == 1).Count(),
					UnpairedFirstOfficers = g.Where(i => i.isScheduled).Where(t => t.answeredRequests.Count() == 1).SelectMany(t => t.answeredRequests).Where(r => r.RequestedPositionID == 2).Count(),
					ScheduledCaptains = g.Where(i => i.isScheduled).SelectMany(t => t.answeredRequests).Where(r => r.RequestedPositionID == 1).Count(),
					ScheduledFirstOfficers = g.Where(i => i.isScheduled).SelectMany(t => t.answeredRequests).Where(r => r.RequestedPositionID == 2).Count(),
					TotalCost = g.Where(i => i.isScheduled).Select(i=>i.trainingCost).Sum(),
					TotalRevenue = g.Where(i => i.isScheduled).Select(i => i.trainingRevenue).Sum()

				}).ToList();

			return optimizationStatistics;

		}

		private IEnumerable<DayliUtilizationItem> getResourceUtilizationForPlan(ShowPlanViewModel i_PlanData)
		{
			List<DayliUtilizationItem> utilizationDataToReturn = new List<DayliUtilizationItem>();

			using (var _context = new FoxModelContainer())
			{
				var resourceTypesFromDB = _context.DWH_ResourcesBasicData.ToList();
				var numOfResourcesPerType = resourceTypesFromDB
					.GroupBy(rt => rt.ResourceMetaDataTypeID,
							 rt => rt.ResourceID,
							 (key, g) => new
							 {
								 ResourceTypeID = key,
								 CountOfResources = g.Count()
							 }).ToList();

				var allocationsFromPlan = i_PlanData.TrainingPrograms.Where(tp => tp.isScheduled)
					.SelectMany(tp => tp.Events).Where(ev => ev.AllocatedResources != null)
					.SelectMany(ev => ev.AllocatedResources)
					.Join(resourceTypesFromDB, al => al.ResourceID, at => at.ResourceID, (al, at) => new ResourceAllocation()
					{
						AllocationID = al.AllocationID,
						StartTime = al.StartTime,
						EndTime = al.EndTime,
						OriginalRequirementID = al.OriginalRequirementID,
						ResourceID = at.ResourceID,
						ResourceTypeID = at.ResourceMetaDataTypeID,
						ResourceTypeName = at.ResourceMetaDataTypeName,
						Origin = 1
					})
					.OrderBy(a => a.StartTime).ToList();

				var minStartDate = allocationsFromPlan.Count() > 0 ? allocationsFromPlan.Min(r => r.StartTime) : new DateTime() ;
				var maxEndDate =  allocationsFromPlan.Count() > 0 ? allocationsFromPlan.Max(r => r.EndTime): new DateTime();
				var allocationsFromDB = _context.DWH_ResourcesAllocations.Where(a => a.ResourceStartAllocationTime >= minStartDate && a.ResourceEndAllocationTime <= maxEndDate)
					.Where(a => a.ResourceStartAllocationTime.HasValue && a.ResourceEndAllocationTime.HasValue)
					.Select(a => new ResourceAllocation()
					{
						AllocationID = a.AllocationID,
						StartTime = a.ResourceStartAllocationTime.Value,
						EndTime = a.ResourceEndAllocationTime.Value,
						OriginalRequirementID = a.ResourceRequirementID,
						ResourceID = a.ResourceID,
					}).ToList().Join(resourceTypesFromDB, 
					al => al.ResourceID, rt => rt.ResourceID,
					(al, at) => new ResourceAllocation()
					{
						AllocationID = al.AllocationID,
						StartTime = al.StartTime,
						EndTime = al.EndTime,
						OriginalRequirementID = al.OriginalRequirementID,
						ResourceID = at.ResourceID,
						ResourceTypeID = at.ResourceMetaDataTypeID,
						ResourceTypeName = at.ResourceMetaDataTypeName,
						Origin = 2
					}).ToList();

				var allResourceAllocations = allocationsFromPlan.Union(allocationsFromDB);
				utilizationDataToReturn = allResourceAllocations
					.GroupBy(a => new { StartTime = a.StartTime.Date, ResourceTypeID = a.ResourceTypeID, ResourceTypeName = a.ResourceTypeName, a.Origin },
							 a => new { DuartionInMinutes = a.DurationInMinutes, ResourceID = a.ResourceID },
							 (key, g) => new DayliUtilizationItem()
							 {
								 StartDate = key.StartTime,
								 ResourceTypeID = key.ResourceTypeID,
								 Origin = key.Origin,
								 ResourceTypeName = key.ResourceTypeName,
								 SumAllocatedMinutes = g.Sum(gr => gr.DuartionInMinutes)
							 }).ToList();
				

				var groupedDataPerDate = utilizationDataToReturn.GroupBy(u => new { Date = u.StartDate.Date, ResourceTypeID = u.ResourceTypeID, ResourceTypeName = u.ResourceTypeName });
				utilizationDataToReturn = groupedDataPerDate.Select(u=> new DayliUtilizationItem() { 
					ResourceTypeID = u.Key.ResourceTypeID,
					ResourceTypeName = u.Key.ResourceTypeName,
					StartDate = u.Key.Date,
					SumAllocatedMinutesBefore = u.Where(g=>g.Origin == 2).Select(g=>g.SumAllocatedMinutes).Sum(),
					SumAllocatedMinutesAfter = u.Where(g => g.Origin == 2).Select(g => g.SumAllocatedMinutes).Sum() + u.Where(g => g.Origin == 1).Select(g => g.SumAllocatedMinutes).Sum(),
					Origin = 0,
				}).ToList();

				utilizationDataToReturn = utilizationDataToReturn.Join(numOfResourcesPerType, ut => ut.ResourceTypeID, nr => nr.ResourceTypeID,
					(ut, nr) => new DayliUtilizationItem()
					{
						ResourceTypeID = ut.ResourceTypeID,
						ResourceTypeName = ut.ResourceTypeName,
						StartDate = ut.StartDate,
						SumAllocatedMinutes = ut.SumAllocatedMinutes,
						SumAllocatedMinutesBefore = ut.SumAllocatedMinutesBefore,
						SumAllocatedMinutesAfter = ut.SumAllocatedMinutesAfter,
						TotalAvailability = nr.CountOfResources * 60 * 24
					}).ToList();

			}

			return utilizationDataToReturn;
		}

		private string getRequestPosition(TrainingProgram tp, int positionID)
		{
			var request = tp.AnsweredRequests.SingleOrDefault(r => r.RequestedPositionID == positionID);
			var positionName = positionID == 1 ? "CA" : "FO";
			return request == null ? "" : string.Format("{0}:{1}<span class='text-muted'>({2})</span>", positionName, request.RequestedForUserName, request.TrainingRequestID);
		}

		[NoCache]
		public JsonResult getOptimizationProcessProgress(int LastLineRead)
		{
			string retVal = "";
			int lastLineNumToReturn = 0;
			bool processHasEnded = false;
			lock (SyncRoot)
			{
				if (s_OptimizationVerbosProgress.Count > 0)
				{
					var linesToReturn = s_OptimizationVerbosProgress.Where(v => v.Key > LastLineRead).Select(l => string.Format("<b>{0:000}</b>:&nbsp;&nbsp;{1}", l.Key, l.Value));
					lastLineNumToReturn = s_OptimizationVerbosProgress.Keys.Last();
					retVal = string.Join("", linesToReturn);
					processHasEnded = s_OptimizationProcessHasEnded;
				}
			}
			return Json(new { verbosNewLine = retVal, processEnded = processHasEnded, verbosLastLineNum = lastLineNumToReturn }, JsonRequestBehavior.AllowGet);
		}

		[HttpGet]
		public ActionResult OptimizationSettings(IEnumerable<int> RequestID)
		{
			OptimizationSettingsViewModel model = new OptimizationSettingsViewModel();
			using (var _context = new FoxModelContainer())
			{
				if (RequestID != null)
				{
					model.TrainingRequests = getTrainingRequests(_context, RequestID, false);
					model.RelevantResourceTypes = getRelevantResourceTypes(model.TrainingRequests.Select(tr => tr.MasterPlanID).Distinct(), _context);
				}

				model.RelevantMasterPlans = _context.DWH_MasterPlansBasicData.Where(mp => mp.MasterPlan_VersionStatusID == 1).Select(mp => new MasterPlan()
				{
					MasterPlanID = mp.MasterPlanID,
					MasterPlanName = mp.MasterPlanName
				}).OrderBy(mp => mp.MasterPlanName).ToList();

				if (RequestID != null)
					model.RelevantRequestIDs = RequestID.ToArray();

			}

		    using (var _db = new FoxConceptEntities())
		    {
		        model.SchedulingBusinessRules = _db.SchedulingBusinessRules.Where(b => b.Optional == true).ToList();
		    }

			return View(model);
		}

        public ActionResult OptimizarionSettings(string MasterPlanIDs, string MasterPlansQtys)
        {
            OptimizationSettingsViewModel model = new OptimizationSettingsViewModel();

            
            using (var _context = new FoxModelContainer())
            {
                if (MasterPlanIDs != null)
                {
                    List<int> MPsInt = new List<int>();
                    //List<int?> list = new List<int?>();

                    foreach (var masterPlanID in MasterPlanIDs.Split(',').ToList())
                    {
                        MPsInt.Add(Convert.ToInt32(masterPlanID));
                        //list.Add(Convert.ToInt32(masterPlanID));
                    }

                    List<MasterPlan> mps = _context.DWH_MasterPlansBasicData.Where(m => MPsInt.Any(i => i == m.MasterPlanID))
                                .Select(m => new MasterPlan()
                                    {
                                        MasterPlanID = m.MasterPlanID,
                                        MasterPlanName = m.MasterPlanName,
                                        SelectedMasterPlan = m.MasterPlanID
                                    }).ToList();

                    var mpQyt = MasterPlansQtys.Split(',').ToList();
                    for (int i = 0; i < mps.Count(); i++)
                    {
                        mps[i].RequiredQuatity = mpQyt[i];
                    }
                    
                    
                    //model.RelevantResourceTypes = getRelevantResourceTypes(list, _context);

                    mps.AddRange(_context.DWH_MasterPlansBasicData.Where(mp => mp.MasterPlan_VersionStatusID == 1).Select(mp => new MasterPlan()
                    {
                        MasterPlanID = mp.MasterPlanID,
                        MasterPlanName = mp.MasterPlanName
                    }).OrderBy(mp => mp.MasterPlanName).ToList());

                    model.RelevantMasterPlans = mps;
                }

                
                
            }

            using (var _db = new FoxConceptEntities())
            {
                model.SchedulingBusinessRules = _db.SchedulingBusinessRules.Where(b => b.Optional == true).ToList();
            }

            return View("OptimizationSettings",model);
        }

		private IEnumerable<ResourceType> getRelevantResourceTypes(IEnumerable<int?> i_MasterPlanIDs, FoxModelContainer _context)
		{
			var allActivities = getActivitiesToSchedule(i_MasterPlanIDs, _context);
			var allResourceRequirements = allActivities.Where(ac => ac.ActivityResourceRequirements != null).SelectMany(ac => ac.ActivityResourceRequirements);
			var resourceTypesToReturn = allResourceRequirements
				.Select(rr => new ResourceType { TypeID = rr.RequirementResourceTypeID, TypeName = rr.RequirementResourceTypeName })
				.Distinct(new ResourceType.ResourceTypeComparer()).ToList();
			return resourceTypesToReturn;
		}

		private IEnumerable<Activity> getActivitiesToSchedule(IEnumerable<int?> allMasterPlans, FoxModelContainer _context)
		{
			List<Activity> activitiesToReturn = new List<Activity>();
			foreach (var mpID in allMasterPlans.Where(mp => mp.HasValue))
			{
				activitiesToReturn.AddRange(getActivitiesToSchedule(mpID, _context));
			}

			return activitiesToReturn;
		}

		private JsonResult getOptimizationModel(OptimizationSettingsViewModel i_Settings)
		{
			var requestIDs = i_Settings.RelevantRequestIDs;
			var additioanlTrainingRequests = i_Settings.AdditioanlTrainingRequests;
			var useEralyGracePeriod = i_Settings.UseEarlyGracePeriod;
			var revenueGoal = i_Settings.Goals.FirstOrDefault(g => g.GoalTypeID == 4);
			var maximizeRevenue = revenueGoal == null ? false : revenueGoal.Selected;

			OptimizationInputModel model = new OptimizationInputModel();
			using (var _context = new FoxModelContainer())
			{
				model.OptimizationSettings = getOptimizationSettings(_context);

				model.TrainingRequests = requestIDs != null ? getTrainingRequests(_context, requestIDs, useEralyGracePeriod) : null;
				if (model.TrainingRequests != null)
					model.TrainingRequests.AddRange(getAdditioanlTrainingRequest(additioanlTrainingRequests, _context));
				else
					model.TrainingRequests = getAdditioanlTrainingRequest(additioanlTrainingRequests, _context);

				if (maximizeRevenue)
				{
					model.TrainingRequests.Where(tr => tr.MasterPlanCost > 0).ToList().ForEach(tr => tr.RequestPriority = tr.RequestPriority + 20);
					model.TrainingRequests.Where(tr => tr.MasterPlanRevenue > 0).ToList().ForEach(tr => tr.RequestPriority = tr.RequestPriority - 10);
				}


				if (model.TrainingRequests.Count > 0)
				{
					var requestsWithStartDate = model.TrainingRequests.Where(r => r.TrainingRequestEndDateObject.HasValue).ToList();
					var minRequestStartDate = requestsWithStartDate.Min(r => r.TrainingRequestStartDateObject.Value);
					var maxRequestStartDate = model.TrainingRequests.Where(r => r.TrainingRequestStartDateObject.HasValue).Max(r => r.TrainingRequestEndDateObject.Value);
					model.Resources = getResources(_context);
					model.UsersUnavailability = getUserUnavalabilityEvents(model.TrainingRequests, _context);
				}


				model.MasterPlans = getRelevantMasterPlansOfTrainingRequests(model.TrainingRequests, _context);
			}

			return Json(model, JsonRequestBehavior.AllowGet);
		}

		private List<TrainingRequest> getTrainingRequests(FoxModelContainer _context, IEnumerable<int> i_RequestIDs, bool i_UseEarlyGracePerios)
		{
			List<TrainingRequest> requestsToReturn = new List<TrainingRequest>();
			//TODO: Filter Request to display according to actual data (now filtered only for fox airlines)
			IEnumerable<int> requestToDisplay;
			requestToDisplay = i_RequestIDs;
			if (i_RequestIDs == null) return null;

			var trainingRequests = _context.DWH_TrainingRequests.Where(tr => requestToDisplay.Contains(tr.ProductRequestID));
			foreach (var tr in trainingRequests)
			{
				//TODO: Talk to David about grace Time and sholud it be part of the request date range or a seperate definition
				TrainingRequest request = new TrainingRequest();
				request.TrainingRequestID = tr.ProductRequestID; // The request is actualy every product requested not a training request.
				request.TrainingRequestTitle = tr.TrainingRequestName;
				request.RequestPriority = 0; //TODO: Get actual request priorety from the system.
				request.TrainingRequestStartDateObject = tr.ProductRequestStartDate;
				request.TrainingRequestEndDateObject = tr.ProductRequestEndDate;
				request.PreferredLocationID = tr.ProductRequest_LocationID;
				request.RequestedForUserID = tr.TrainingRequestOwnerID;
				request.TrainingExpectedIncome = 0; //TODO: Add Income Calculation
				


				var userExtraDetails = _context.DWH_UsersExtraDetails.Where(u => u.UserID == tr.TrainingRequestOwnerID).Select(ued => new
				{
					User_MetaDataFieldExternalID = ued.User_MetaDataFieldExternalID,
					UserMetaDataFieldValue = ued.UserMetaDataFieldValue
				});
				var userRole = userExtraDetails.FirstOrDefault(ued => ued.User_MetaDataFieldExternalID == "PILOT_ROLE");
				var userCrewID = userExtraDetails.FirstOrDefault(ued => ued.User_MetaDataFieldExternalID == "PILOT_CREW_ID");


				request.PreferredCrewID = userCrewID == null ? "-1" : userCrewID.UserMetaDataFieldValue;

				request.RequestedPositionName = userRole == null ? "" : userRole.UserMetaDataFieldValue;

				if (request.RequestedPositionName.ToLower() == "captain")
					request.RequestedPositionID = 1;
				else if (request.RequestedPositionName.ToLower() == "first officer")
					request.RequestedPositionID = 2;
				else request.RequestedPositionID = 1;

				var ProductMasterPlanAssos = _context.DWH_ProductsMasterPlanAssociations.FirstOrDefault(pr => pr.ProductID == tr.ProductRequest_ProductID);
				request.MasterPlanID = ProductMasterPlanAssos == null ? default(int?) : ProductMasterPlanAssos.Product_MasterPlanID;
				request.MasterPlanName = ProductMasterPlanAssos == null ? "" : ProductMasterPlanAssos.Product_MasterPlanName;
				request.ParticipantRequirements = getParticipantRequirements(request.MasterPlanID, _context);

				
				if (i_UseEarlyGracePerios)
				{
					request.TrainingRequestStartDateObject = request.TrainingRequestStartDateObject.Value.AddMonths(-1);
					request.TrainingRequestEndDateObject = request.TrainingRequestEndDateObject.Value.AddMonths(1);
				}

				long mpCost, mpRev;
				getMPCostAndRevenue(request.MasterPlanID.Value, _context, out mpCost, out mpRev);
				request.MasterPlanCost = mpCost;
				request.MasterPlanRevenue = mpRev;

				requestsToReturn.Add(request);
			}

			DateTime maxEndDate = requestsToReturn.Max(tr => tr.TrainingRequestEndDateObject).Value;
			foreach (var tr in requestsToReturn.Where(tr => tr.TrainingRequestEndDate == null))
			{
				tr.TrainingRequestEndDateObject = maxEndDate;
			}



			return requestsToReturn;
		}

		private List<MasterPlan> getRelevantMasterPlansOfTrainingRequests(List<TrainingRequest> i_RelevantTrainingRequests, FoxModelContainer _context)
		{
			List<MasterPlan> masterPlansToReturn = new List<MasterPlan>();
			var relevantMasterPlanIDs = i_RelevantTrainingRequests.Select(tr => tr.MasterPlanID).Distinct().ToList();
			var masterPlansList = _context.DWH_MasterPlansBasicData.Where(mp => relevantMasterPlanIDs.Contains(mp.MasterPlanID));

			foreach (var mp in masterPlansList)
			{
				MasterPlan masterPlan = new MasterPlan();
				masterPlan.MasterPlanID = mp.MasterPlanID;
				masterPlan.MasterPlanName = mp.MasterPlanName;
				masterPlan.ActivitiesToSchedule = getActivitiesToSchedule(mp.MasterPlanID, _context);

				masterPlansToReturn.Add(masterPlan);
			}

			return masterPlansToReturn;
		}

		private List<TrainingRequest> getAdditioanlTrainingRequest(IEnumerable<AdditioanlTrainingRequest> i_AdditioanlRequests, FoxModelContainer _context)
		{
			int trIDHash = ADDIRIOAN_REQUEST_START_ID;
			var runningID = 0;

			List<TrainingRequest> requestsToReturn = new List<TrainingRequest>();

			foreach (var req in i_AdditioanlRequests)
			{
				for (int i = 1; i <= req.RequiredQuantity; i++)
				{
					TrainingRequest request = new TrainingRequest();
					request.TrainingRequestID = trIDHash + runningID;
					runningID++;
					request.MasterPlanID = req.RequiredMasterPlan;
					request.ParticipantRequirements = null;
					request.PreferredLocationID = 0;
					request.RequestedForUserID = null;
					request.RequestedPositionID = null;
					request.RequestPriority = req.Priorety + 1; //Ensure lower priorety than original requests
					request.RequestedPositionName = null;
					request.TrainingExpectedIncome = 0;
					request.TrainingRequestStartDateObject = req.RequestStartDateObject;
					request.TrainingRequestEndDateObject = req.RequestEndDateObject;
					request.TrainingRequestTitle = "Additioanl Requirement - " + i;
					long mpCost, mpRev;
					getMPCostAndRevenue(request.MasterPlanID.Value, _context, out mpCost, out mpRev);
					request.MasterPlanCost = mpCost;
					request.MasterPlanRevenue = mpRev;

					requestsToReturn.Add(request);
				}
			}

			return requestsToReturn;
		}

		private OptimizationSettings getOptimizationSettings(FoxModelContainer _context)
		{
			OptimizationSettings settings = new OptimizationSettings();
			settings.OptimizationScheduleResolution = 60;
			settings.OptimizationGoals = new List<OptimizationGoal>();
			settings.OptimizationGoals.Add(new OptimizationGoal()
			{
				GoalTypeID = 1,
				GoalTypeDescription = "Maximize participant quota (pairing)",
				Priority = 1,

			});
			settings.OptimizationGoals.Add(new OptimizationGoal()
			{
				GoalTypeID = 2,
				GoalTypeDescription = "Maximize training troughtput",
				Priority = 2

			});
			settings.OptimizationGoals.Add(new OptimizationGoal()
			{
				GoalTypeID = 3,
				GoalTypeDescription = "Minimize target end date of training",
				GoalConstraintTypeID = 1,
				GoalConstraintTypeDescription = "Do not exceed",
				GoalConstraintTargetValue = "2014-04-30", // TODO: Get actual target end date.
				Priority = 3

			});
			settings.OptimizationGoals.Add(new OptimizationGoal()
			{
				GoalTypeID = 4,
				GoalTypeDescription = "Minimize resource utilization",
				GoalConstraintTypeID = 1,
				GoalConstraintTypeDescription = "Do not exceed",
				GoalConstraintTargetValue = "30%", // TODO: Get actual target end date.
				Priority = 4

			});


			return settings;
		}

		private List<Resource> getResources(FoxModelContainer _context)
		{
			List<Resource> resourcesToReturn = new List<Resource>();
			int[] repositoriesToDisplay = { 7909, 7907, 8255, 7908 };
			foreach (var res in _context.DWH_ResourcesBasicData.Where(r => repositoriesToDisplay.Contains(r.ResourceRepositoryID)))
			{
				Resource resource = new Resource();
				resource.ResourceID = res.ResourceID;
				resource.ResourceName = res.ResourceName;
				resource.ResourceTypeID = res.ResourceMetaDataTypeID;
				resource.ResourceTypeName = res.ResourceMetaDataTypeName;
				resource.ResourceLocationID = 0; //TODO: Get Resource LocationID;
				resource.ResourceProperties = getResourcePropeties(resource.ResourceID, _context);
				resource.ResourceUnavailability = getResourceUnvalability(resource.ResourceID, _context);
				resource.ResourceCertifications = getResourceCerttifications(res.ResourceUserID, _context);

				resourcesToReturn.Add(resource);
			}

			return resourcesToReturn;
		}

		private List<ResourceCertifications> getResourceCerttifications(int? i_UserID, FoxModelContainer _context)
		{

			List<ResourceCertifications> certificationOfResource = new List<ResourceCertifications>();

			var certsFromDB = _context.DWH_User_Certifications.Where(c => c.User_ID == i_UserID);
			foreach (var cert in certsFromDB)
			{
				ResourceCertifications resourceCert = new ResourceCertifications();
				resourceCert.CertificationID = cert.Certification_ID;
				resourceCert.CretificationName = cert.Certification_Name;
				resourceCert.ExpirationDate = cert.User_Certification_Final_Expiration_Date.HasValue ? cert.User_Certification_Final_Expiration_Date.Value.ToString(GLOBAL_DATE_TIME_FORMAT) : "";
			}

			return certificationOfResource;


		}

		private List<UserUnavalability> getUserUnavalabilityEvents(List<TrainingRequest> trainingRequests, FoxModelContainer _context)
		{
			List<UserUnavalability> usersToReturn = new List<UserUnavalability>();
			var userIDsRequestedTraining = trainingRequests.Where(tr=>tr.RequestedForUserID >= 0).Select(tr => tr.RequestedForUserID).Distinct();
			foreach (var userID in userIDsRequestedTraining)
			{
				if (userID.HasValue)
				{
					UserUnavalability ua = new UserUnavalability();
					ua.UserID = userID.Value;
					DWH_User userBasicData = _context.DWH_Users.FirstOrDefault(u => ua.UserID == u.UserID);
					ua.UserName = userBasicData.UserFirstName + " " + userBasicData.UserLastName;
					ua.UserUnavailability = getUserUnavalabilityEvents(userID, _context);

					usersToReturn.Add(ua);
				}
			}
			return usersToReturn;
		}

		private List<Event> getUserUnavalabilityEvents(int? i_UserID, FoxModelContainer _context)
		{
			List<Event> unavalabilityEvets = new List<Event>();
			if (i_UserID == null) return unavalabilityEvets;

			foreach (var uea in _context.DWH_UserEventAssociations.Where(ue => ue.User_ID == i_UserID))
			{
				DWH_EventBasicData eventData = _context.DWH_EventsBasicData.FirstOrDefault(e => e.Event_ID == uea.Event_ID);
				if (eventData != null)
				{
					Event userEvent = new Event();
					userEvent.EventID = eventData.Event_ID;
					userEvent.EventName = eventData.Event_Name;
					userEvent.EventStartTime = eventData.Event_StartTime.Value.ToString(GLOBAL_DATE_TIME_FORMAT);
					userEvent.EventEndTime = eventData.Event_EndTime.Value.ToString(GLOBAL_DATE_TIME_FORMAT);

					unavalabilityEvets.Add(userEvent);
				}
			}

			return unavalabilityEvets;
		}

		private List<Event> getResourceUnvalability(int i_ResourceID, FoxModelContainer _context)
		{
			List<Event> events = new List<Event>();
			foreach (var alloc in _context.DWH_ResourcesAllocations.Where(a => a.ResourceID == i_ResourceID))
			{
				Event ev = new Event();
				ev.EventID = alloc.AllocationID;
				DWH_EventBasicData dbEvent = _context.DWH_EventsBasicData.FirstOrDefault(e => e.Event_ID == alloc.EventID);
				ev.EventName = dbEvent == null ? "" : dbEvent.Event_Name;
				//Ignore allocation time is null (View BUG)
				ev.EventStartTime = alloc.ResourceStartAllocationTime == null ? "" : alloc.ResourceStartAllocationTime.Value.ToString(GLOBAL_DATE_TIME_FORMAT);
				ev.EventEndTime = alloc.ResourceEndAllocationTime == null ? "" : alloc.ResourceEndAllocationTime.Value.ToString(GLOBAL_DATE_TIME_FORMAT);

				events.Add(ev);
			}

			return events;
		}

		private List<ResourceProperties> getResourcePropeties(int i_ResourceID, FoxModelContainer _context)
		{
			List<ResourceProperties> props = new List<ResourceProperties>();
			var propsList = _context.DWH_ResourcesExtraDetails.Where(ed => ed.ResourceID == i_ResourceID).Select(ed => new
			{
				ResourceMetaDataFieldID = ed.ResourceMetaDataFieldID,
				ResourceMetaDataFieldName = ed.ResourceMetaDataFieldName,
				ResourceMetaDataFieldTypeID = ed.ResourceMetaDataFieldTypeID,
				ResourceMetaDataTypeName = ed.ResourceMetaDataTypeName,
				ResourceMetaDataFieldValue = ed.ResourceMetaDataFieldValue,
				ResourceMetaDataFieldIsMultipleSelection = ed.ResourceMetaDataFieldIsMultipleSelection,
			});
			foreach (var prop in propsList)
			{
				ResourceProperties property = new ResourceProperties();
				property.PropertyID = prop.ResourceMetaDataFieldID;
				property.PropertyName = prop.ResourceMetaDataFieldName;
				property.PropertyTypeID = prop.ResourceMetaDataFieldTypeID;
				property.PropertyTypeName = prop.ResourceMetaDataTypeName;
				property.SelectedOptionIDs = prop.ResourceMetaDataFieldValue == null ? null : prop.ResourceMetaDataFieldValue.Split(',');
				property.IsMultipleChoice = prop.ResourceMetaDataFieldIsMultipleSelection;
				property.Value = prop.ResourceMetaDataFieldValue;

				if (property.PropertyID != null) //Workaround for some wierd bug where an extra details row is returned with nulls
				{
					props.Add(property);
				}

			}

			return props;

		}

		private List<Activity> getActivitiesToSchedule(int? i_MasterPlanID, FoxModelContainer _context)
		{

			List<Activity> activitiesToReturn = new List<Activity>();
			if (i_MasterPlanID == null) return activitiesToReturn;

			foreach (var act in _context.DWH_Activities.Where(a => a.Activity_EntityID == i_MasterPlanID))
			{
				Activity activity = new Activity();
				activity.ActivityID = act.Activity_ID;
				activity.ActivityName = act.Activity_Name;
				activity.ActivityDuration = act.Activity_Duration == null ? 0 : act.Activity_Duration.Value;
				activity.ActivityParticipantRequirements = getActivitieParticipantRequirements(act.Activity_ID, _context);
				activity.ActivityPrerequisites = getActivityPrerequisites(act.Activity_ID, _context);
				activity.ActivityResourceRequirements = getActivityResourceRequirements(act.Activity_ID, _context);

				activitiesToReturn.Add(activity);
			}

			return activitiesToReturn;

		}

		private List<ResourceRequirement> getActivityResourceRequirements(int i_ActivityID, FoxModelContainer _context)
		{
			List<ResourceRequirement> resourceRequirementsToReturn = new List<ResourceRequirement>();
			foreach (var req in _context.DWH_ResourcesRequirements.Where(rr => rr.ActivityID == i_ActivityID))
			{
				ResourceRequirement requirement = new ResourceRequirement();
				requirement.RequirementID = req.RequirementResourceID;
				requirement.RequirementResourceTypeName = req.RequirementResourceTypeName;
				requirement.RequirementResourceTypeID = req.RequirementResourceTypeID;
				requirement.RequirementStartTimeDelta = 0; // TODO Add delta start time
				requirement.RequirementEndTimeDelta = 0; // TODO Add delta end time
				requirement.RequirementProperties = getResourceRequirementProperties(requirement.RequirementID, _context);
				requirement.CertificationRequirementIDs = null; // TODO Add requirement certifications

				resourceRequirementsToReturn.Add(requirement);

			}

			return resourceRequirementsToReturn;

		}

		private List<RequirementProperties> getResourceRequirementProperties(int i_RequirementID, FoxModelContainer _context)
		{
			List<RequirementProperties> properties = new List<RequirementProperties>();
			foreach (var prop in _context.DWH_ResourcesRequirementProperty.Where(p => p.ResourceRequirementID == i_RequirementID))
			{
				RequirementProperties property = new RequirementProperties();
				property.PropertyID = prop.ResourceRequirementPropertyID;
				property.PropertyName = prop.ResourceRequirementPropertyName;
				property.IsMultipleChoice = prop.IsMultipleChoice;
				property.MaxValue = prop.ResourceRequirementPropertyMaxValue;
				property.MinValue = prop.ResourceRequirementPropertyMinValue;
				property.OperatorID = //TODO: find the operator ID
				property.PropertyTypeID = prop.ResourceRequirementPropertyTypeID;
				property.SelectedOptionIDs = prop.ResourceRequirementPropertyValue == null ? null : prop.ResourceRequirementPropertyValue.Split(',');
				property.PropertyID = prop.ResourceRequirementPropertyID;
				property.PropertyTypeName = ""; //TODO: Get property type name

				properties.Add(property);
			}

			return properties;

		}

		private List<Prerequisite> getActivityPrerequisites(int i_ActivityID, FoxModelContainer _context)
		{
			List<Prerequisite> prerequisistes = new List<Prerequisite>();
			foreach (var prereq in _context.DWH_ActivityPrerequisit.Where(pr => pr.ActivityID == i_ActivityID))
			{
				Prerequisite prerequisite = new Prerequisite();
				prerequisite.PrerequisiteActivityID = prereq.PrerequisiteID;
				prerequisite.PrerequisiteActivityName = prereq.PrerequisiteName;
				prerequisite.PrerequisiteCompletionsMaxDays = prereq.PrerequisiteMaxDays;
				prerequisite.PrerequisiteCompletionsMinDays = prereq.PrerequisiteMinDays;

				prerequisistes.Add(prerequisite);
			}

			return prerequisistes;
		}

		private List<ParticipantRequirement> getActivitieParticipantRequirements(int p, FoxModelContainer _context)
		{
			//TODO: Add relevant requirements
			List<ParticipantRequirement> participantRequirements = new List<ParticipantRequirement>();
			ParticipantRequirement requirement1 = new ParticipantRequirement();
			requirement1.ParticipantRequirementPositionID = 1;
			requirement1.ParticipantRequirementPositionName = "Captain";
			requirement1.ParticipantRequirementQuota = 1;
			ParticipantRequirement requirement2 = new ParticipantRequirement();
			requirement2.ParticipantRequirementPositionID = 2;
			requirement2.ParticipantRequirementPositionName = "First Officer";
			requirement2.ParticipantRequirementQuota = 1;
			participantRequirements.Add(requirement1);
			participantRequirements.Add(requirement2);

			return participantRequirements;
		}

		private List<ParticipantRequirement> getParticipantRequirements(int? i_MasterPlanID, FoxModelContainer _context)
		{
			//TODO: Add relevant requirements
			List<ParticipantRequirement> participantRequirements = new List<ParticipantRequirement>();
			ParticipantRequirement requirement1 = new ParticipantRequirement();
			requirement1.ParticipantRequirementPositionID = 1;
			requirement1.ParticipantRequirementPositionName = "Captain";
			requirement1.ParticipantRequirementQuota = 1;
			ParticipantRequirement requirement2 = new ParticipantRequirement();
			requirement2.ParticipantRequirementPositionID = 2;
			requirement2.ParticipantRequirementPositionName = "First Officer";
			requirement2.ParticipantRequirementQuota = 1;
			participantRequirements.Add(requirement1);
			participantRequirements.Add(requirement2);

			return participantRequirements;
		}

		private void sendDataToOptimizaer()
		{
			using (var client = new HttpClient())
			{
				client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
				var content = new StringContent("\"{\"OptimizationGoals\": [],\"TrainingRequests\": [1, 2]}\"");

				var result = client.PostAsync("http://192.168.102.51:5000/api/ping/", content).Result;
				string resultContent = result.Content.ReadAsStringAsync().Result;
				Response.Write(resultContent);
				Response.End();
			}
		}

		private void getMPCostAndRevenue(int i_MPID, FoxModelContainer _context, out long o_Cost, out long o_Revenue)
		{
			var mpExtraDetails = _context.Bks_DWH_MasterPlansExtraDetails.Where(mp => mp.MasterPlanID == i_MPID).Select(mp =>
					new
					{
						MasterPlanID = mp.MasterPlanID,
						MasterPlanMetaDataFieldValue = mp.MasterPlanMetaDataFieldValue,
						MasterPlanMetaDataFieldExternalID = mp.MasterPlanMetaDataFieldExternalID
					});

			var mpCostMD = mpExtraDetails.FirstOrDefault(mp => mp.MasterPlanMetaDataFieldExternalID == "MP_COST");
			var mpRevenueMD = mpExtraDetails.FirstOrDefault(mp => mp.MasterPlanMetaDataFieldExternalID == "MP_REVENUE");

			long mpCost = 0, mpRev = 0;

			if (mpRevenueMD != null && mpCostMD != null)
			{
				long.TryParse(mpCostMD.MasterPlanMetaDataFieldValue, out mpCost);
				long.TryParse(mpRevenueMD.MasterPlanMetaDataFieldValue, out mpRev);
			}

			o_Cost = mpCost;
			o_Revenue = mpRev;
		}

		private List<TrainingProgram> getTrainingProgramsFromJSON(JArray jsonTrainingPrograms)
		{
			List<TrainingProgram> trainingProgramsToReturn = new List<TrainingProgram>();
			using (var _context = new FoxModelContainer())
			{
				foreach (var tp in jsonTrainingPrograms)
				{
					TrainingProgram itemToAdd = new TrainingProgram();

					itemToAdd.isScheduled = tp["ScheduledStatus"].Value<bool>();
					itemToAdd.OriginalRequestID = tp["TrainingRequestID"].Value<int>();
					itemToAdd.MasterPlanID = tp["MasterPlanID"].ToObject<Nullable<int>>();
					itemToAdd.MasterPlanName = _context.DWH_MasterPlansBasicData.FirstOrDefault(mp => mp.MasterPlanID == itemToAdd.MasterPlanID).MasterPlanName;
					long mpCost, mpRev;
					getMPCostAndRevenue(itemToAdd.MasterPlanID.Value, _context, out mpCost, out mpRev);
					itemToAdd.MasterPlanCost = mpCost;
					itemToAdd.MasterPlanRevenue = mpRev;
					itemToAdd.StartDate = itemToAdd.isScheduled ? tp["TrainingProgramStartDate"].ToObject<DateTime?>() : null;
					itemToAdd.EndDate = itemToAdd.isScheduled ? tp["TrainingProgramEndDate"].ToObject<DateTime?>() : null;
					itemToAdd.Events = itemToAdd.isScheduled ? getEventsFromJSON((JArray)tp["Events"], _context) : null;
					itemToAdd.AnsweredRequests = getTrainingRequestsFromJSON((JArray)tp["AnsweredRequests"], _context);

					trainingProgramsToReturn.Add(itemToAdd);
				}
			}

			return trainingProgramsToReturn;
		}

		private List<TrainingRequest> getTrainingRequestsFromJSON(JArray i_Data, FoxModelContainer _context)
		{
			List<TrainingRequest> requestsToReturn = new List<TrainingRequest>();

			foreach (var item in i_Data)
			{
				TrainingRequest request = new TrainingRequest();
				request.TrainingRequestID = item["TrainingRequestID"].Value<int?>();
				request.RequestedForUserID = item["ParticipantID"].ToObject<int?>();
				if (request.RequestedForUserID != null && request.RequestedForUserID >=0)
				{
					var userFromDB = _context.DWH_Users.FirstOrDefault(u => u.UserID == request.RequestedForUserID);
					request.RequestedForUserName = userFromDB.UserFirstName + " " + userFromDB.UserLastName;
				}
				request.RequestedPositionID = item["PositionID"].ToObject<int?>();
				request.RequestedPositionName = item["PositionName"].Value<string>();

				requestsToReturn.Add(request);
			}

			return requestsToReturn;
		}

		private List<FoxEvent> getEventsFromJSON(JArray i_EventsData, FoxModelContainer _context)
		{
			List<FoxEvent> eventsToReturn = new List<FoxEvent>();
			foreach (var ev in i_EventsData)
			{
				var eventItem = new FoxEvent();
				eventItem.ActivityID = ev["ActivityID"].Value<int>();
				eventItem.StartDate = ev["ActivityStartDate"].Value<DateTime>();
				eventItem.EndDate = ev["ActivityStartDate"].Value<DateTime>(); //TODO: get event end date
				eventItem.AllocatedResources = getAllocatedResourcesFromJSON((JArray)ev["ResourceAllocations"]);

				eventsToReturn.Add(eventItem);
			}

			return eventsToReturn;
		}

		private List<ResourceAllocation> getAllocatedResourcesFromJSON(JArray i_AllocatedResourceData)
		{
			List<ResourceAllocation> resourcesToReturn = new List<ResourceAllocation>();
			foreach (var alloc in i_AllocatedResourceData)
			{
				var allocation = new ResourceAllocation();
				allocation.AllocationID = alloc["ResourceRequirementID"].Value<int>(); //TODO: Generate AllocationID
				allocation.OriginalRequirementID = alloc["ResourceRequirementID"].Value<int>();
				allocation.StartTime = alloc["AllocationStartTime"].Value<DateTime>();
				allocation.EndTime = alloc["AllocationEndTime"].Value<DateTime>();
				allocation.ResourceID = alloc["AssignedResourceID"].Value<int>();

				resourcesToReturn.Add(allocation);
			}

			return resourcesToReturn;

		}

		private static Dictionary<int, string> s_OptimizationVerbosProgress;
		private static int s_VerbosLineNumber;
		private static bool s_OptimizationProcessHasEnded;
		private object SyncRoot = new object();

		private void startOptimizationProcess(ProcessStartInfo psi)
		{
			var process = new Process { StartInfo = psi };
			process.EnableRaisingEvents = true;
			process.OutputDataReceived += new DataReceivedEventHandler(process_OutputDataReceived);
			process.ErrorDataReceived += new DataReceivedEventHandler(process_OutputDataReceived);
			process.Exited += new EventHandler(process_Exited);

			s_OptimizationProcessHasEnded = false;
			s_VerbosLineNumber = 0;
			s_OptimizationVerbosProgress = new Dictionary<int, string>();
            process.Start();
            process.BeginErrorReadLine();
            process.BeginOutputReadLine();
		}

		private ProcessStartInfo getOptimizationUtilityProcessStartInfo(string i_InputfilePath, string i_OutputfilePath, string i_OptimizationScriptPath, string i_PythonPath, string i_MaxHoursPerDay, string i_MaxOffDays, string i_LogLevel)
		{
			//var psi = new ProcessStartInfo(i_PythonPath, String.Format("{0} --log_level {1} --max_hours_per_day {2} --max_off_days {3} {4} {5}", i_OptimizationScriptPath,i_LogLevel, i_MaxHoursPerDay, i_MaxOffDays, i_InputfilePath, i_OutputfilePath))
			var psi = new ProcessStartInfo(i_PythonPath, String.Format("{0} {1} {2}", i_OptimizationScriptPath, i_InputfilePath, i_OutputfilePath))
			{
				WorkingDirectory = Environment.CurrentDirectory,
				UseShellExecute = false,
				RedirectStandardOutput = true,
				RedirectStandardError = true,
				CreateNoWindow = true
			};
			return psi;
		}

		private void saveInputFile(string inputfilePath, string i_jsonData)
		{
			using (var inputFile = System.IO.File.CreateText(inputfilePath))
			{
				inputFile.Write(i_jsonData);
			}
		}

		void process_OutputDataReceived(object sender, DataReceivedEventArgs e)
		{
			if (e.Data != null) // sometimes a random event is received with null data, not sure why - I prefer to leave it out
			{
				lock (SyncRoot)
				{
					Process p = sender as Process;
					s_VerbosLineNumber++;
					s_OptimizationVerbosProgress.Add(s_VerbosLineNumber, string.Format("<b>ProcessID({0})</b> {1}<br />", p.Id, e.Data));
				}
			}
		}

		void process_Exited(object sender, EventArgs e)
		{
			lock (SyncRoot)
			{
				s_OptimizationProcessHasEnded = true;
				Process p = sender as Process;
				p.Close();
			}
		}


	}
}

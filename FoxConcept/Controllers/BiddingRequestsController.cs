﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Data.Entity.Validation;
using System.Data.Objects.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using FoxConcept.Models;
using FoxConcept.Models.DALModels;
using FoxConcept.Models.ViewModels.BiddingRequests;
using FoxConcept.Models.ViewModels.CostSettings;

namespace FoxConcept.Controllers
{
    public class BiddingRequestsController : Controller
    {
        FoxConceptEntities _FoxConceptDB = new FoxConceptEntities();
        FoxModelContainer _FoxDB = new FoxModelContainer();

        protected override void Dispose(bool disposing)
        {
            _FoxConceptDB.Dispose();
            _FoxDB.Dispose();
            base.Dispose(disposing);
        }

        //
        // GET: /BiddingRequests/

        public ActionResult Index(int InstructorID)
        {
            BiddingRequestsViewMode model = new BiddingRequestsViewMode();
            model.InstractorID = InstructorID;

            // Get Instrctor information
            model.Instructor =
                _FoxDB.DWH_ResourcesBasicData.Where(i => i.ResourceUserID == InstructorID).AsEnumerable().Select(i => new Instructor()
                    {
                        ID = (int)i.ResourceUserID,
                        Name = i.ResourceName,
                        EntityTypeID = i.ResourceMetaDataTypeID
                    }).FirstOrDefault();

            if (model.Instructor != null)
            {
                model.BiddingRules =
                _FoxConceptDB.BiddingRules.Where(r => r.EntityTypeID == model.Instructor.EntityTypeID).ToList();
            }


            model.BiddingRequests =
                _FoxConceptDB.BiddingRequests.Include("BiddingPeriod")
                             .Include("BiddingRule")
                             .AsEnumerable()
                             .Select(b => new BiddingRequestViewModel()
                                 {
                                     ID = b.ID,
                                     BiddingPeriod = b.BiddingPeriod,
                                     BiddingRule = b.BiddingRule,
                                     BiddingPeriodID = b.BiddingPeriodID,
                                     BiddingRuleID = b.BiddingRuleID,
                                     StartDate = b.StartDate,
                                     EndDate = b.EndDate.HasValue ? b.EndDate : null,
                                     InstructorID = b.InstructorID,
                                     InstructorName = b.InstructorName,
                                     Comments = b.Comments,
                                     Status = b.StatusEnum,
                                     Priority = b.Priority
                                 }).ToList();

            return View(model);
        }

        [HttpGet]
        public ActionResult AddBiddingRequest(int InstructorID, int biddingRule)
        {
            BiddingRequestViewModel model = new BiddingRequestViewModel();

            model.BiddingRule = _FoxConceptDB.BiddingRules.FirstOrDefault(r => r.ID == biddingRule);

            model.RequestsLeft = model.BiddingRule.DaysPerInstructor - _FoxConceptDB.BiddingRequests.Where(
                            r =>
                            r.BiddingRuleID == biddingRule && r.InstructorID == InstructorID).AsEnumerable().Select(r => new
                            {
                                Days =
                                                                                                   r.EndDate
                                                                                                    .HasValue
                                                                                                       ? Convert
                                                                                                             .ToInt16
                                                                                                             (((
                                                                                                               DateTime
                                                                                                               )
                                                                                                               r
                                                                                                                   .EndDate -
                                                                                                               (
                                                                                                               DateTime
                                                                                                               )
                                                                                                               r
                                                                                                                   .StartDate)
                                                                                                                  .TotalDays)
                                                                                                       : 1
                            }).Sum(r => r.Days);

            model.InstructorID = InstructorID;
            model.BiddingRuleID = biddingRule;

            model.StartDate = DateTime.Now;
            model.EndDate = DateTime.Now;
            model.Priority = 1;

            model.BiddingPeriods = model.BiddingRule.GetActiveBiddingPeriods();


            return View(model);
        }

        [HttpPost]
        public ActionResult AddBiddingRequest(BiddingRequestViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    int requested =
                        _FoxConceptDB.BiddingRequests.Where(
                            r =>
                            r.BiddingRuleID == model.BiddingRuleID && r.InstructorID == model.InstructorID &&
                            r.BiddingPeriodID == model.BiddingPeriodID).AsEnumerable().Select(r => new
                                {
                                    Days =
                                                                                                       r.EndDate
                                                                                                        .HasValue
                                                                                                           ? Convert
                                                                                                                 .ToInt16
                                                                                                                 (((
                                                                                                                   DateTime
                                                                                                                   )
                                                                                                                   r
                                                                                                                       .EndDate -
                                                                                                                   (
                                                                                                                   DateTime
                                                                                                                   )
                                                                                                                   r
                                                                                                                       .StartDate)
                                                                                                                      .TotalDays)
                                                                                                           : 1
                                }).Sum(r => r.Days);


                    model.BiddingRule = _FoxConceptDB.BiddingRules.FirstOrDefault(r => r.ID == model.BiddingRuleID);
                    if (model.BiddingRule.DaysPerInstructor < requested)
                    {
                        ModelState.AddModelError("Requestleft", "You exceeded the number of requests for this pertiod");
                        model.BiddingPeriods = model.BiddingRule.GetActiveBiddingPeriods();
                        return View(model);
                    }

                    BiddingRequest _request = new BiddingRequest();
                    _request.InstructorID = model.InstructorID;
                    _request.BiddingRuleID = model.BiddingRuleID;
                    _request.BiddingPeriodID = model.BiddingPeriodID;
                    _request.StartDate = model.StartDate;
                    _request.StatusEnum = BiddingRequest.eStatusRequest.New;
                    _request.InstructorName =
                        _FoxDB.DWH_ResourcesBasicData.FirstOrDefault(r => r.ResourceUserID == _request.InstructorID)
                              .ResourceName;
                    if (model.EndDate.HasValue)
                    {
                        _request.EndDate = model.EndDate;
                    }
                    else
                    {
                        _request.EndDate = _request.StartDate;
                    }

                    _request.Priority = model.Priority;
                    _request.Comments = model.Comments;

                    _FoxConceptDB.BiddingRequests.Add(_request);

                    int results = _FoxConceptDB.SaveChanges();

                    return RedirectToAction("EditBiddingRequest", new { @ID = _request.ID });

                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw;
                }

            }

            model.BiddingPeriods = model.BiddingRule.GetActiveBiddingPeriods();

            return View(model);
        }

        public ActionResult EditBiddingRequest(int ID)
        {
            BiddingRequest _request = _FoxConceptDB.BiddingRequests.Include("BiddingRule").Include("BiddingPeriod").FirstOrDefault(b => b.ID == ID);
            BiddingRequestViewModel model = new BiddingRequestViewModel();
            model.BiddingPeriod = _request.BiddingPeriod;
            model.BiddingPeriodID = _request.BiddingPeriodID;
            model.BiddingRule = _request.BiddingRule;
            model.BiddingRuleID = _request.BiddingRuleID;
            model.BiddingPeriods = model.BiddingRule.GetActiveBiddingPeriods();
            model.InstructorID = _request.InstructorID;

            model.StartDate = _request.StartDate;
            model.EndDate = _request.EndDate;
            model.Comments = _request.Comments;
            model.Priority = model.Priority;
            model.ID = model.ID;

            return View(model);
        }

        public ActionResult GetBiddingManagement()
        {
            BiddingManagementViewModel model = new BiddingManagementViewModel();

            model.Instructors.AddRange(GetInstructors());

            model.BiddingPeriods.AddRange(GetBiddingPeriods());


            model.BiddingRequests =
               _FoxConceptDB.BiddingRequests.Include("BiddingPeriod")
                            .Include("BiddingRule")
                            .AsEnumerable()
                            .Select(b => new BiddingRequestViewModel()
                            {
                                ID = b.ID,
                                BiddingPeriod = b.BiddingPeriod,
                                BiddingRule = b.BiddingRule,
                                BiddingPeriodID = b.BiddingPeriodID,
                                BiddingRuleID = b.BiddingRuleID,
                                StartDate = b.StartDate,
                                EndDate = b.EndDate.HasValue ? b.EndDate : null,
                                InstructorID = b.InstructorID,
                                InstructorName = b.InstructorName,
                                Comments = b.Comments,
                                Status = b.StatusEnum,
                                Priority = b.Priority
                            }).ToList();

            return View(model);
        }

        private List<EntityType> GetBiddingPeriods()
        {
            return _FoxConceptDB.BiddingRequests.Include("BiddingPeriod").AsEnumerable().Select(b => new
            {
                b.BiddingPeriod.ID,
                b.BiddingPeriod.BiddingPeriodString
            }).Distinct().Select(b => new EntityType()
            {
                ID = b.ID,
                Name = b.BiddingPeriodString
            }).ToList();
        }

        private List<EntityType> GetInstructors()
        {
            return _FoxConceptDB.BiddingRequests.Select(b => new EntityType()
            {
                Name = b.InstructorName,
                ID = b.InstructorID
            }).Distinct().ToList();
        }

        [HttpPost]
        public ActionResult GetBiddingManagement(BiddingManagementViewModel model)
        {
            model.Instructors.AddRange(GetInstructors());

            model.BiddingPeriods.AddRange(GetBiddingPeriods());


            model.BiddingRequests =
               _FoxConceptDB.BiddingRequests.Include("BiddingPeriod")
                            .Include("BiddingRule")
                            .AsEnumerable()
                            .Select(b => new BiddingRequestViewModel()
                            {
                                ID = b.ID,
                                BiddingPeriod = b.BiddingPeriod,
                                BiddingRule = b.BiddingRule,
                                BiddingPeriodID = b.BiddingPeriodID,
                                BiddingRuleID = b.BiddingRuleID,
                                StartDate = b.StartDate,
                                EndDate = b.EndDate.HasValue ? b.EndDate : null,
                                InstructorID = b.InstructorID,
                                InstructorName = b.InstructorName,
                                Comments = b.Comments,
                                Status = b.StatusEnum,
                                Priority = b.Priority
                            }).ToList();

            if (model.InstructorID != null && model.InstructorID != -1)
            {
                model.BiddingRequests = model.BiddingRequests.Where(r => r.InstructorID.Equals(model.InstructorID)).ToList();
            }

            if (model.BiddingPeriodID != null && model.BiddingPeriodID != -1)
            {
                model.BiddingRequests =
                    model.BiddingRequests.Where(r => r.BiddingPeriodID.Equals(model.BiddingPeriodID)).ToList();
            }

            return View("GetBiddingManagement", model);
        }

        public ActionResult ApproveBiddingRequest(int ID)
        {
            BiddingRequestViewModel model = new BiddingRequestViewModel();

            var _biddingRequest = _FoxConceptDB.BiddingRequests.Include("BiddingPeriod").Include("BiddingRule").FirstOrDefault(b => b.ID == ID);

            model.BiddingRule = _biddingRequest.BiddingRule;
            model.BiddingRuleID = _biddingRequest.BiddingRuleID;
            model.BiddingPeriod = _biddingRequest.BiddingPeriod;
            model.BiddingPeriodID = _biddingRequest.BiddingPeriodID;

            model.Comments = _biddingRequest.Comments;
            model.EndDate = _biddingRequest.EndDate;
            model.ID = _biddingRequest.ID;
            model.InstructorID = _biddingRequest.InstructorID;
            model.InstructorName = _biddingRequest.InstructorName;
            model.Priority = _biddingRequest.Priority;
            model.StartDate = _biddingRequest.StartDate;
            model.Status = _biddingRequest.StatusEnum;

            return View(model);
        }

        [HttpPost]
        public ActionResult ApproveBiddingRequest(BiddingRequestViewModel model)
        {
            if (ModelState.IsValid)
            {
                var _biddingRequest = _FoxConceptDB.BiddingRequests.FirstOrDefault(b => b.ID == model.ID);

                _biddingRequest.Status = (int)model.Status;

                _FoxConceptDB.BiddingRequests.AddOrUpdate(_biddingRequest);

                int results = _FoxConceptDB.SaveChanges();

                string url = string.Format("{0}://{1}{2}{3}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"), Url.Action("EditApproveBiddingRequest", new { @ID = _biddingRequest.ID }));
                return Content(url);
            }

            return View(model);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FoxConcept.Models;
using FoxConcept.Models.DALModels;
using FoxConcept.Models.ViewModels.Shared;

namespace FoxConcept.Controllers
{
    public class SchedulingRulesController : Controller
    {
        FoxConceptEntities _FoxConceptDB = new FoxConceptEntities();
        FoxModelContainer _FoxDB = new FoxModelContainer();
        public ActionResult Index()
        {
            SchedulingRulesViewModel model = new SchedulingRulesViewModel(_FoxDB,_FoxConceptDB);
            model.SchedulingRules = _FoxConceptDB.SchedulingBusinessRules.ToList().OrderBy(r => r.Priority);

            foreach (var rule in model.SchedulingRules)
            {
                
               if (rule.AppliesToMDTypeID == 1)
                {
                    rule.AppliesToMDTypeName = "Pilots";
                }
                else if (rule.AppliesToMDTypeID == 2)
                {
                    rule.AppliesToMDTypeName = "Customers";
                }
                else
                {
                    rule.AppliesToMDTypeName = model.ApplyTo.FirstOrDefault(a => a.Key == rule.AppliesToMDTypeID).Value;
                }
            }
            

            return View("Index", model);
        }

        #region AllocationTimeLimit
        public ActionResult AddAllocationTimeLimit()
        {
            AddAllocationTimeLimitViewModel model = new AddAllocationTimeLimitViewModel(_FoxDB, _FoxConceptDB);

            return View("AddAllocationTimeLimit", model);
        }

        public ActionResult EditAllocationTimeLimit(int id, bool newRecord = false)
        {
            AddAllocationTimeLimitViewModel model = new AddAllocationTimeLimitViewModel(_FoxDB, _FoxConceptDB);

            model.GeneralDetails = _FoxConceptDB.SchedulingBusinessRules.Where(s => s.RuleID == id && s.RuleTypeID == (int)SchedulingBusinessRule.eRuleType.AllocationTimeLimit).SingleOrDefault();

            model.RuleDetails = _FoxConceptDB.AllocationTimeLimitRules.Where(r => r.ID == id).SingleOrDefault();

            if (newRecord)
            {
                ViewData["NewRecord"] = "True";
            }

            return View("EditAllocationTimeLimit", model);
        }

        [HttpPost]
        public ActionResult EditAllocationTimeLimit(AddAllocationTimeLimitViewModel i_model)
        {
            _FoxConceptDB.SchedulingBusinessRules.AddOrUpdate(i_model.GeneralDetails);

            _FoxConceptDB.AllocationTimeLimitRules.AddOrUpdate(i_model.RuleDetails);

            _FoxConceptDB.SaveChanges();

            return RedirectToAction("EditAllocationTimeLimit", new { id = i_model.GeneralDetails.RuleID, newRecord = true });
        }

        [HttpPost]
        public ActionResult AddAllocationTimeLimit(AddAllocationTimeLimitViewModel i_model)
        {
            // 1. Add the Rule to Allocation Interval Limit and get the ID to associate tot he scheduling business
            _FoxConceptDB.AllocationTimeLimitRules.Add(i_model.RuleDetails);

            int results = _FoxConceptDB.SaveChanges();

            int lastRuleID = _FoxConceptDB.AllocationTimeLimitRules.Max(M => M.ID);

            //2. Associalte the RuleID form section 1 to scheduling rules
            i_model.GeneralDetails.RuleID = lastRuleID;
            i_model.GeneralDetails.CreationDate = DateTime.Now;
            i_model.GeneralDetails.ExternalID = DateTime.Now.ToString();
            i_model.GeneralDetails.RuleType = SchedulingBusinessRule.eRuleType.AllocationTimeLimit;
            _FoxConceptDB.SchedulingBusinessRules.Add(i_model.GeneralDetails);

            results = _FoxConceptDB.SaveChanges();

            return RedirectToAction("EditAllocationTimeLimit", new { id = i_model.GeneralDetails.RuleID, newRecord = true });
        }
        #endregion

        #region ReschedulingLimit
        public ActionResult AddReschedulingLimit()
        {
            AddReschedulingLimitViewModel model = new AddReschedulingLimitViewModel(_FoxDB, _FoxConceptDB);

            return View("AddReschedulingLimit", model);
        }

        public ActionResult EditReschedulingLimit(int id, bool newRecord = false)
        {
            AddReschedulingLimitViewModel model = new AddReschedulingLimitViewModel(_FoxDB, _FoxConceptDB);

            model.GeneralDetails = _FoxConceptDB.SchedulingBusinessRules.Where(s => s.RuleID == id && s.RuleTypeID == (int)SchedulingBusinessRule.eRuleType.ReschedulingLimit).SingleOrDefault();

            model.RuleDetails = _FoxConceptDB.ReschedulingLimitRules.Where(r => r.ID == id).SingleOrDefault();

            if (newRecord)
            {
                ViewData["NewRecord"] = "True";
            }

            return View("EditReschedulingLimit", model);
        }

        [HttpPost]
        public ActionResult EditReschedulingLimit(AddReschedulingLimitViewModel i_model)
        {
            _FoxConceptDB.SchedulingBusinessRules.AddOrUpdate(i_model.GeneralDetails);

            _FoxConceptDB.ReschedulingLimitRules.AddOrUpdate(i_model.RuleDetails);

            _FoxConceptDB.SaveChanges();

            return RedirectToAction("EditReschedulingLimit", new { id = i_model.GeneralDetails.RuleID, newRecord = true });
        }

        [HttpPost]
        public ActionResult AddReschedulingLimit(AddReschedulingLimitViewModel i_model)
        {
            // 1. Add the Rule to Allocation Interval Limit and get the ID to associate tot he scheduling business
            _FoxConceptDB.ReschedulingLimitRules.Add(i_model.RuleDetails);

            int results = _FoxConceptDB.SaveChanges();

            int lastRuleID = _FoxConceptDB.ReschedulingLimitRules.Max(M => M.ID);

            //2. Associalte the RuleID form section 1 to scheduling rules
            i_model.GeneralDetails.RuleID = lastRuleID;
            i_model.GeneralDetails.CreationDate = DateTime.Now;
            i_model.GeneralDetails.ExternalID = DateTime.Now.ToString();
            i_model.GeneralDetails.RuleType = SchedulingBusinessRule.eRuleType.ReschedulingLimit;
            _FoxConceptDB.SchedulingBusinessRules.Add(i_model.GeneralDetails);

            results = _FoxConceptDB.SaveChanges();

            return RedirectToAction("EditReschedulingLimit", new { id = i_model.GeneralDetails.RuleID, newRecord = true });
        }

        #endregion

        #region AllocationIntervalLimit
        [HttpGet]
        public ActionResult AddAllocationIntervalLimit()
        {
            AddAllocationIntervalLimitViewModel model = new AddAllocationIntervalLimitViewModel(_FoxDB, _FoxConceptDB);

            return View("AddAllocationIntervalLimit", model);
        }

        public ActionResult EditAllocationIntervalLimit(int id, bool newRecord = false)
        {
            AddAllocationIntervalLimitViewModel model = new AddAllocationIntervalLimitViewModel(_FoxDB, _FoxConceptDB);

            model.GeneralDetails = _FoxConceptDB.SchedulingBusinessRules.Where(s => s.RuleID == id && s.RuleTypeID == (int)SchedulingBusinessRule.eRuleType.AllocationIntervalLimit).SingleOrDefault();

            model.RuleDetails = _FoxConceptDB.AllocationIntervalLimitRules.Where(r => r.ID == id).SingleOrDefault();

            if (newRecord)
            {
                ViewData["NewRecord"] = "True";
            }

            return View("EditAllocationIntervalLimit", model);
        }

        [HttpPost]
        public ActionResult EditAllocationIntervalLimit(AddAllocationIntervalLimitViewModel i_model)
        {
            _FoxConceptDB.SchedulingBusinessRules.AddOrUpdate(i_model.GeneralDetails);

            _FoxConceptDB.AllocationIntervalLimitRules.AddOrUpdate(i_model.RuleDetails);

            _FoxConceptDB.SaveChanges();


            return RedirectToAction("EditAllocationIntervalLimit", new { id = i_model.GeneralDetails.RuleID, newRecord = true });
        }


        [HttpPost]
        public ActionResult AddAllocationIntervalLimit(AddAllocationIntervalLimitViewModel i_model)
        {
            // 1. Add the Rule to Allocation Interval Limit and get the ID to associate tot he scheduling business
            _FoxConceptDB.AllocationIntervalLimitRules.Add(i_model.RuleDetails);

            int results = _FoxConceptDB.SaveChanges();

            int lastRuleID = _FoxConceptDB.AllocationIntervalLimitRules.Max(M => M.ID);

            //2. Associalte the RuleID form section 1 to scheduling rules
            i_model.GeneralDetails.RuleID = lastRuleID;
            i_model.GeneralDetails.CreationDate = DateTime.Now;
            i_model.GeneralDetails.ExternalID = DateTime.Now.ToString();
            i_model.GeneralDetails.RuleType = SchedulingBusinessRule.eRuleType.AllocationIntervalLimit;
            _FoxConceptDB.SchedulingBusinessRules.Add(i_model.GeneralDetails);

            results = _FoxConceptDB.SaveChanges();

            return RedirectToAction("EditAllocationIntervalLimit", new { id = i_model.GeneralDetails.RuleID, newRecord = true });

        }
        #endregion

        #region WorkloadLimit
        public ActionResult AddWorkloadLimit()
        {
            AddWorkloadLimitViewModel model = new AddWorkloadLimitViewModel(_FoxDB, _FoxConceptDB);
            
            return View("AddWorkloadLimit", model);
        }

        public ActionResult EditWorkloadLimit(int id, bool newRecord = false)
        {
            AddWorkloadLimitViewModel model = new AddWorkloadLimitViewModel(_FoxDB, _FoxConceptDB);

            model.GeneralDetails = _FoxConceptDB.SchedulingBusinessRules.Where(s => s.RuleID == id && s.RuleTypeID == (int)SchedulingBusinessRule.eRuleType.AllocationWorkloadLimit).SingleOrDefault();

            model.RuleDetails = _FoxConceptDB.AllocationWorkloadLimitRules.Where(r => r.ID == id).SingleOrDefault();

            if (newRecord)
            {
                ViewData["NewRecord"] = "True";
            }

            return View("EditWorkloadLimit", model);
        }

        [HttpPost]
        public ActionResult EditWorkloadLimit(AddWorkloadLimitViewModel i_model)
        {
            _FoxConceptDB.SchedulingBusinessRules.AddOrUpdate(i_model.GeneralDetails);

            _FoxConceptDB.AllocationWorkloadLimitRules.AddOrUpdate(i_model.RuleDetails);

            _FoxConceptDB.SaveChanges();

            return RedirectToAction("EditWorkloadLimit", new { id = i_model.GeneralDetails.RuleID, newRecord = true });
        }

        [HttpPost]
        public ActionResult AddWorkloadLimit(AddWorkloadLimitViewModel i_model)
        {
            // 1. Add the Rule to Allocation Interval Limit and get the ID to associate tot he scheduling business
            _FoxConceptDB.AllocationWorkloadLimitRules.Add(i_model.RuleDetails);

            int results = _FoxConceptDB.SaveChanges();

            int lastRuleID = _FoxConceptDB.AllocationWorkloadLimitRules.Max(M => M.ID);

            //2. Associalte the RuleID form section 1 to scheduling rules
            i_model.GeneralDetails.RuleID = lastRuleID;
            i_model.GeneralDetails.CreationDate = DateTime.Now;
            i_model.GeneralDetails.ExternalID = DateTime.Now.ToString();
            i_model.GeneralDetails.RuleType = SchedulingBusinessRule.eRuleType.AllocationWorkloadLimit;
            _FoxConceptDB.SchedulingBusinessRules.Add(i_model.GeneralDetails);

            results = _FoxConceptDB.SaveChanges();

            return RedirectToAction("EditWorkloadLimit", new { id = i_model.GeneralDetails.RuleID, newRecord = true });
        }

        #endregion

        #region DaysOffRule


        public ActionResult AddDaysOffRule()
        {
            AddDaysOffRuleViewModel model = new AddDaysOffRuleViewModel(_FoxDB, _FoxConceptDB);


            return View("AddDaysOffRule", model);
        }

        public ActionResult EditDaysOffRule(int id, bool newRecord = false)
        {
            AddDaysOffRuleViewModel model = new AddDaysOffRuleViewModel(_FoxDB, _FoxConceptDB);

            model.GeneralDetails = _FoxConceptDB.SchedulingBusinessRules.Where(s => s.RuleID == id && s.RuleTypeID == (int)SchedulingBusinessRule.eRuleType.DaysOfftRule).SingleOrDefault();

            model.RuleDetails = _FoxConceptDB.AllocationDaysOfftRules.Where(r => r.ID == id).SingleOrDefault();

            if (newRecord)
            {
                ViewData["NewRecord"] = "True";
            }

            return View("EditDaysOffRule", model);
        }

        [HttpPost]
        public ActionResult EditDaysOffRule(AddDaysOffRuleViewModel i_model)
        {
            _FoxConceptDB.SchedulingBusinessRules.AddOrUpdate(i_model.GeneralDetails);

            _FoxConceptDB.AllocationDaysOfftRules.AddOrUpdate(i_model.RuleDetails);

            _FoxConceptDB.SaveChanges();

            return RedirectToAction("EditDaysOffRule", new { id = i_model.GeneralDetails.RuleID, newRecord = true });
        }

        [HttpPost]
        public ActionResult AddDaysOffRule(AddDaysOffRuleViewModel i_model)
        {

            // 1. Add the Rule to Allocation Interval Limit and get the ID to associate tot he scheduling business
            _FoxConceptDB.AllocationDaysOfftRules.Add(i_model.RuleDetails);

            int results = _FoxConceptDB.SaveChanges();

            int lastRuleID = _FoxConceptDB.AllocationDaysOfftRules.Max(M => M.ID);

            //2. Associalte the RuleID form section 1 to scheduling rules
            i_model.GeneralDetails.RuleID = lastRuleID;
            i_model.GeneralDetails.CreationDate = DateTime.Now;
            i_model.GeneralDetails.ExternalID = DateTime.Now.ToString();
            i_model.GeneralDetails.RuleType = SchedulingBusinessRule.eRuleType.DaysOfftRule;
            _FoxConceptDB.SchedulingBusinessRules.Add(i_model.GeneralDetails);

            results = _FoxConceptDB.SaveChanges();

            return RedirectToAction("EditDaysOffRule", new { id = i_model.GeneralDetails.RuleID, newRecord = true });
        }

        #endregion
        protected override void Dispose(bool disposing)
        {
            _FoxConceptDB.Dispose();
            base.Dispose(disposing);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FoxConcept.Models;
using FoxConcept.Models.DALModels;
using FoxConcept.Models.ViewModels.Shared;

namespace FoxConcept.Controllers
{
	public class CertificationController : Controller
	{
		
		[HttpGet]
		public ActionResult CertificationStatus()
		{
			CertificationStatusViewModel model = new CertificationStatusViewModel();
			using (var _context = new FoxModelContainer())
			{
				model.UserCertificationData = _context.DWH_User_Certifications.ToList();
				model.AvailableCertifications = getAvailableCertificationFIlter(_context);
			}

			return View("CertificationStatus", model);
		}

		private IEnumerable<DWH_Certification_Details_Main> getAvailableCertificationFIlter(FoxModelContainer _context)
		{
			var distinctCertsOfAllUsers = _context.DWH_User_Certifications.Select(c => c.Certification_ID).Distinct().ToList();
			var availableCerts = new List<DWH_Certification_Details_Main>();
			availableCerts.Add(new DWH_Certification_Details_Main { Cert_ID=-1, Cert_Name="" });
			availableCerts.AddRange(_context.DWH_Certification_Details_Main.Where(c => distinctCertsOfAllUsers.Contains(c.Cert_ID)).ToList());
			return availableCerts;
		}

		[HttpPost]
		public ActionResult CertificationStatus(CertificationStatusViewModel.CertificationStatusFilter FilterData)
        {
			CertificationStatusViewModel model = new CertificationStatusViewModel();
			using(var _context = new FoxModelContainer())
			{
				model.AvailableCertifications = getAvailableCertificationFIlter(_context);
				model.FilterData.CertificationIDsFilter = FilterData.CertificationIDsFilter;
				model.FilterData.ExpirationStartDateFilter = FilterData.ExpirationStartDateFilter;
				model.FilterData.ExpirationEndDateFilter = FilterData.ExpirationEndDateFilter;

				var CertificationIDsToFilter = FilterData.CertificationIDsFilter;
				model.UserCertificationData = _context.DWH_User_Certifications
					.Where(uc => uc.Certification_ID == CertificationIDsToFilter || CertificationIDsToFilter == -1).ToList();

				if (FilterData.ExpirationStartDateFilter.HasValue)
				{
					model.UserCertificationData.Where(uc=> uc.User_Certification_Final_Expiration_Date >= FilterData.ExpirationStartDateFilter);
				}

				if (FilterData.ExpirationEndDateFilter.HasValue)
				{
					model.UserCertificationData.Where(uc => uc.User_Certification_Final_Expiration_Date <= FilterData.ExpirationEndDateFilter);
				}

				
			}

			return View("CertificationStatus", model);
        }

		[HttpPost]
		public ActionResult ScheduleTraining(IEnumerable<string> UserCertifications)
		{
			var userCertificationData = UserCertifications.Select(s=> System.Web.Helpers.Json.Decode<DWH_User_Certification>(s)).ToList();
			AutomaticSchedulingSettingsViewModel model = new AutomaticSchedulingSettingsViewModel();
			using (var _context = new FoxModelContainer())
			{
				var userCertIDs = userCertificationData.Select(uc=>uc.Certification_ID).Distinct();
				var requiredCertifications = _context.DWH_Certification_Details_Main.Where(c => userCertIDs.Contains(c.Cert_ID)).ToList();
				var requiredTrainingForUsers = userCertificationData
					.Join(requiredCertifications, uc => uc.Certification_ID, c => c.Cert_ID, (uc, c) => new { 
						UserID  = uc.User_ID,
						CertificationID = uc.Certification_ID,
						CertificationStatus = uc.User_Certification_Status,
						ExpirationDate = uc.User_Certification_Final_Expiration_Date,
						MasterPlanID = ((new[] {1,8,9,10,13}).Contains(uc.User_Certification_Status.Value))? c.Cert_Acquiring_Master_Plan_ID : c.Cert_Renewal_Master_Plan_ID,
						MasterPlanName = ((new[] {1,8,9,10,13}).Contains(uc.User_Certification_Status.Value))? c.Cert_Acquiring_Master_Plan_Name : c.Cert_Renewal_Master_Plan_Name,
					}).ToList();

				var schedulingRequirements = requiredTrainingForUsers
					.Where(uc=>uc.MasterPlanID.HasValue)
					.GroupBy(t => new { MasterPlanID = t.MasterPlanID, MasterPlanName = t.MasterPlanName },
					(key, g) => new AutomaticSchedulingSettingsViewModel.AutomaticScheduleRequirement
					{
						MasterPlan = new MasterPlan {MasterPlanID=key.MasterPlanID.Value, MasterPlanName=key.MasterPlanName},
						RequiredEndDate = g.Max(uc=>uc.ExpirationDate),
						RequiredStartDate = g.Min(uc=>uc.ExpirationDate),
						RequiredQuantity = g.Count()
					}).ToList();

				model.SchedulingRequirements = schedulingRequirements.ToArray();
				
			}
			
			return RedirectToAction("Settings", "AutomaticScheduling", model);
		}

	}
}

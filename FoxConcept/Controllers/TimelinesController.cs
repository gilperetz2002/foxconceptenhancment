﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FoxConcept.Models;
using FoxConcept.Models.DALModels;
using FoxConcept.Models.ViewModels.Timeline;

namespace FoxConcept.Controllers
{
    public class TimelinesController : Controller
    {
        //
        // GET: /Timelines/

        FoxConceptEntities _FoxConceptDB = new FoxConceptEntities();
        FoxModelContainer _FoxDB = new FoxModelContainer();

        protected override void Dispose(bool disposing)
        {
            _FoxConceptDB.Dispose();
            _FoxDB.Dispose();
            base.Dispose(disposing);
        }

        public ActionResult GetResourceRepositoryRresources(int ResourceRepositoryID, int StartDays, int EndDays)
        {
            ResourceGanttViewModel Model = new ResourceGanttViewModel();
            Model.Items = (from RP in _FoxDB.Bks_DWH_ResourceRepositoriesBasicData
                join RS in _FoxDB.DWH_ResourcesBasicData on RP.ResourceRepositoryID equals RS.ResourceRepositoryID
                join RA in _FoxDB.DWH_ResourcesAllocations on RS.ResourceID equals RA.ResourceID
                join EV in _FoxDB.Bks_DWH_Events on RA.EventID equals EV.Event_ID
                where RP.ResourceRepositoryID == ResourceRepositoryID && RA.ResourceStartAllocationTime >= DateTime.Now.AddDays(StartDays) && RA.ResourceEndAllocationTime <= DateTime.Now.AddDays(EndDays)
                select new GanttItem()
                {
                    ClassName = EV.,
                    Content = "<div data-tooltip='" + EV.Event_Name + "'><div class='GantItemMainData'>" + EV.Event_Name +"<div/><div class='GanttItemSecondaryData'></div></div>",
                    StartDate = RA.ResourceStartAllocationTime,
                    EndDate = RA.ResourceEndAllocationTime,
                    GroupName = RS.ResourceName
                }).ToList();


            

            return View(Model);
        }

    }
}

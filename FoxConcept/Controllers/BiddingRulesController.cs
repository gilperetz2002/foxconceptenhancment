﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FoxConcept.Models;
using FoxConcept.Models.DALModels;
using FoxConcept.Models.ViewModels.BiddingRules;
using FoxConcept.Models.ViewModels.CostSettings;

namespace FoxConcept.Controllers
{
    public class BiddingRulesController : Controller
    {
        //
        // GET: /BiddingRules/

        FoxConceptEntities _FoxConceptDB = new FoxConceptEntities();
        FoxModelContainer _FoxDB = new FoxModelContainer();

        protected override void Dispose(bool disposing)
        {
            _FoxConceptDB.Dispose();
            _FoxDB.Dispose();
            base.Dispose(disposing);
        }

        public ActionResult Index()
        {
            IEnumerable<BiddingPeriodViewModel> model =
                _FoxConceptDB.BiddingPeriods.Select(b => new BiddingPeriodViewModel()
                {
                    ID = b.ID,
                    Name = b.Name,
                    StartDate = b.StartDate,
                    EndDate = b.EndDate
                });

            return View(model);
        }

        public ActionResult GetBiddingPeriods(int ID)
        {
            IEnumerable<BiddingPeriodViewModel> model =
                _FoxConceptDB.BiddingPeriods.Where(b => b.BiddingCalendarID == ID).OrderByDescending(c => c.StartDate).Select(b => new BiddingPeriodViewModel()
                {
                    ID = b.ID,
                    Name = b.Name,
                    StartDate = b.StartDate,
                    EndDate = b.EndDate
                });

            ViewData["CalendarID"] = ID;

            return View("Index", model);
        }

        public ActionResult AddBiddingCalendar()
        {
            BiddingCalendarViewMode model = new BiddingCalendarViewMode();

            return View(model);
        }

        [HttpPost]
        public ActionResult AddBiddingCalendar(BiddingCalendarViewMode model)
        {
            if (ModelState.IsValid)
            {
                BiddingCalendar _cal = new BiddingCalendar();
                _cal.Name = model.Name;

                _FoxConceptDB.BiddingCalendars.Add(_cal);

                int results = _FoxConceptDB.SaveChanges();

                return RedirectToAction("EditBiddingCalendar", new {ID = _cal.ID});
            }
            return View(model);
        }

        public ActionResult EditBiddingCalendar(int ID)
        {
            BiddingCalendar _cal = _FoxConceptDB.BiddingCalendars.FirstOrDefault(c => c.ID == ID);
            BiddingCalendarViewMode model = new BiddingCalendarViewMode();

            model.ID = _cal.ID;
            model.Name = _cal.Name;

            return View(model);
        }

        [HttpPost]
        public ActionResult EditBiddingCalendar(BiddingCalendarViewMode model)
        {
            BiddingCalendar _cal = _FoxConceptDB.BiddingCalendars.FirstOrDefault(c => c.ID == model.ID);

            _cal.Name = model.Name;

            _FoxConceptDB.BiddingCalendars.AddOrUpdate(_cal);

            int results = _FoxConceptDB.SaveChanges();

            return RedirectToAction("EditBiddingCalendar", new { ID = _cal.ID });

        }

        public ActionResult GetBiddingCalendars()
        {
            IEnumerable<BiddingCalendarViewMode> model =
                _FoxConceptDB.BiddingCalendars.Select(c => new BiddingCalendarViewMode()
                    {
                        ID = c.ID,
                        Name = c.Name
                    }).ToList();

            return View(model);
        }

        public ActionResult AddBiddingPeriod(int ID)
        {
            BiddingPeriodViewModel model = new BiddingPeriodViewModel();
            DateTime date = DateTime.Now;
            model.CalendarID = ID;
            model.StartDate = new DateTime(date.Year, date.Month, 1);
            model.EndDate = new DateTime(date.Year, date.Month, DateTime.DaysInMonth(date.Year, date.Month));

            return View(model);
        }

        [HttpPost]
        public ActionResult AddBiddingPeriod(BiddingPeriodViewModel model)
        {
            if (ModelState.IsValid)
            {
                BiddingPeriod _period = new BiddingPeriod();

                _period.Name = model.Name;
                _period.StartDate = model.StartDate;
                _period.EndDate = model.EndDate;
                _period.BiddingCalendarID = model.CalendarID;

                _FoxConceptDB.BiddingPeriods.Add(_period);

                int results = _FoxConceptDB.SaveChanges();

                return RedirectToAction("EditBiddingPeriod", new {@ID = _period.ID});
            }
            

            return View(model);
        }

        public ActionResult EditBiddingPeriod(int ID)
        {
            var period = _FoxConceptDB.BiddingPeriods.FirstOrDefault(p => p.ID == ID);

            BiddingPeriodViewModel model = new BiddingPeriodViewModel();
            model.StartDate = period.StartDate;
            model.EndDate = period.EndDate;
            model.Name = period.Name;
            model.ID = period.ID;
            model.CalendarID = period.BiddingCalendarID;

            return View(model);
        }

        [HttpPost]
        public ActionResult EditBiddingPeriod(BiddingPeriodViewModel model)
        {
            if (ModelState.IsValid)
            {
                BiddingPeriod period = _FoxConceptDB.BiddingPeriods.FirstOrDefault(p => p.ID == model.ID);

                period.StartDate = model.StartDate;
                period.EndDate = model.EndDate;
                period.Name = model.Name;

                _FoxConceptDB.BiddingPeriods.AddOrUpdate(period);

                int results = _FoxConceptDB.SaveChanges();

                return RedirectToAction("EditBiddingPeriod", new { @ID = period.ID });
            }

            return View(model);
        }

        public ActionResult DeleteBiddingPeriod(int ID)
        {
            var period = _FoxConceptDB.BiddingPeriods.FirstOrDefault(p => p.ID == ID);

            _FoxConceptDB.BiddingPeriods.Remove(period);

            int results = _FoxConceptDB.SaveChanges();

            return RedirectToAction("Index");
        }



        public ActionResult GetBiddingRules()
        {
            IEnumerable<BiddingRuleItemList> model =
                _FoxConceptDB.BiddingRules.Include("BiddingCalendar").Select(b => new BiddingRuleItemList()
                    {
                        ID = b.ID,
                        BiddingCalendar = b.BiddingCalendar,
                        EntityType = (eEntityType) b.EntityType,
                        EntityTypeID = (int) b.EntityTypeID,
                        EntityTypeName = b.EntityTypeName,
                        Name = b.Name,
                        EndBidding = (int) b.EndBidding,
                        StartBidding = (int) b.StartBidding,
                        BiddingType = (EBiddingType)b.BiddingTypeID,
                        DaysPerInstructor = b.DaysPerInstructor
                    });

            return View(model);
        }

        public ActionResult AddBiddingRule()
        {
            BiddingRuleViewModel model = new BiddingRuleViewModel();

            model.BiddingCalendars = LoadBidingCalenders();

            model.EntityType = eEntityType.HumanResource;
            model.EntityTypes =
                _FoxDB.DWH_ResourcesBasicData.Where(r => r.ResourceType == 2).Select(r => new EntityType()
                {
                    ID = r.ResourceMetaDataTypeID,
                    Name = r.ResourceMetaDataTypeName
                }).Distinct().OrderBy(r => r.Name).ToList();

            return View(model);
        }

        private List<EntityType> LoadBidingCalenders()
        {
            return _FoxConceptDB.BiddingCalendars.AsEnumerable().Select(b => new EntityType()
            {
                ID = b.ID,
                Name = b.Name
            }).ToList();
        }

        [HttpPost]
        public ActionResult AddBiddingRule(BiddingRuleViewModel model)
        {
            if (ModelState.IsValid)
            {
                BiddingRule _biddingRule = new BiddingRule();

                _biddingRule.BiddingCalendarID = model.BiddingCalendarID;
                _biddingRule.BiddingTypeID = (int) model.BiddingType;
                _biddingRule.EndBidding = model.EndBidding;
                _biddingRule.EntityType = (int)eEntityType.HumanResource;
                _biddingRule.EntityTypeID = model.EntityTypeID;
                _biddingRule.EntityTypeName = _FoxDB.DWH_ResourcesBasicData.Where(r => r.ResourceMetaDataTypeID == model.EntityTypeID)
                          .FirstOrDefault()
                          .ResourceMetaDataTypeName;

                _biddingRule.Name = model.Name;
                _biddingRule.StartBidding = model.StartBidding;

                _FoxConceptDB.BiddingRules.Add(_biddingRule);

                int results = _FoxConceptDB.SaveChanges();

                return RedirectToAction("EditBiddingRule", new {@ID = _biddingRule.ID});

            }
            model.BiddingCalendars = LoadBidingCalenders();

            model.EntityType = eEntityType.HumanResource;
            model.EntityTypes =
                _FoxDB.DWH_ResourcesBasicData.Where(r => r.ResourceType == 2).Select(r => new EntityType()
                {
                    ID = r.ResourceMetaDataTypeID,
                    Name = r.ResourceMetaDataTypeName
                }).Distinct().OrderBy(r => r.Name).ToList();

            return View(model);

        }

        public ActionResult EditBiddingRule(int ID)
        {
            var _biddingRule = _FoxConceptDB.BiddingRules.FirstOrDefault(b => b.ID == ID);

            BiddingRuleViewModel model = new BiddingRuleViewModel();

            model.BiddingCalendars = LoadBidingCalenders();

            model.EntityType = eEntityType.HumanResource;
            model.EntityTypes =
                _FoxDB.DWH_ResourcesBasicData.Where(r => r.ResourceType == 2).Select(r => new EntityType()
                {
                    ID = r.ResourceMetaDataTypeID,
                    Name = r.ResourceMetaDataTypeName
                }).Distinct().OrderBy(r => r.Name).ToList();

            model.BiddingCalendarID = (int)_biddingRule.BiddingCalendarID;
            model.BiddingType = (EBiddingType) _biddingRule.BiddingTypeID;
            model.EndBidding = (int)_biddingRule.EndBidding;
            model.EntityType = (eEntityType) _biddingRule.EntityType;
            model.EntityTypeID = (int) _biddingRule.EntityTypeID;
            model.ID = _biddingRule.ID;
            model.Name = _biddingRule.Name;
            model.StartBidding = (int) _biddingRule.StartBidding;
            model.DaysPerInstructor = _biddingRule.DaysPerInstructor;


            return View(model);
        }

        [HttpPost]
        public ActionResult EditBiddingRule(BiddingRuleViewModel model)
        {
            if (ModelState.IsValid)
            {
                var _biddingRule = _FoxConceptDB.BiddingRules.FirstOrDefault(b => b.ID == model.ID);

                _biddingRule.BiddingCalendarID = model.BiddingCalendarID;
                _biddingRule.BiddingTypeID = (int)model.BiddingType;
                _biddingRule.EndBidding = model.EndBidding;
                _biddingRule.EntityType = (int)eEntityType.HumanResource;
                _biddingRule.EntityTypeID = model.EntityTypeID;
                _biddingRule.EntityTypeName = _FoxDB.DWH_ResourcesBasicData.Where(r => r.ResourceMetaDataTypeID == model.EntityTypeID)
                          .FirstOrDefault()
                          .ResourceMetaDataTypeName;

                _biddingRule.Name = model.Name;
                _biddingRule.StartBidding = model.StartBidding;
                _biddingRule.DaysPerInstructor = model.DaysPerInstructor;

                _FoxConceptDB.BiddingRules.AddOrUpdate(_biddingRule);

                int results = _FoxConceptDB.SaveChanges();

                return RedirectToAction("EditBiddingRule", new { @ID = _biddingRule.ID });
            }

            model.BiddingCalendars = LoadBidingCalenders();

            model.EntityType = eEntityType.HumanResource;
            model.EntityTypes =
                _FoxDB.DWH_ResourcesBasicData.Where(r => r.ResourceType == 2).Select(r => new EntityType()
                {
                    ID = r.ResourceMetaDataTypeID,
                    Name = r.ResourceMetaDataTypeName
                }).Distinct().OrderBy(r => r.Name).ToList();

            return View(model);
        }

        public ActionResult DeleteBiddingRule(int ID)
        {
            var _biddingRule = _FoxConceptDB.BiddingRules.FirstOrDefault(b => b.ID == ID);

            _FoxConceptDB.BiddingRules.Remove(_biddingRule);

            int results = _FoxConceptDB.SaveChanges();

            return RedirectToAction("GetBiddingRules");
        }

    }
}

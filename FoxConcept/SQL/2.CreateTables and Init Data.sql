CREATE TABLE [SchedulingBusinessRules](
	[ID] int IDENTITY(1,1) NOT NULL,
	[ExternalID] nvarchar(3000) NULL,
	[Title] nvarchar(1000) NOT NULL,
	[Description] nvarchar(2000),
	[CreationDate] datetime NOT NULL,
	[AppliesToMDTypeID] int NOT NULL,
	[RuleTypeID] int NOT NULL,
	[RuleID] int NOT NULL,
	[ConstraintTypeID] int NOT NULL,
	[Priority] int NOT NULL,
	Optional bit NOT NULL DEFAULT 0,
	PRIMARY KEY ([ID])	
)


CREATE TABLE [AllocationTimeLimitRule](
	[ID] int IDENTITY(1,1) NOT NULL,
	[ActivityTypeID] int NULL,
	[DayOfWeek] int not NULL,
	[StartTimeValue] int not NULL,
	[EndTimeValue] int not NULL,
	PRIMARY KEY ([ID])
)


CREATE TABLE [ReschedulingLimitRule](
	[ID] int IDENTITY(1,1) NOT NULL,
	[ActivityTypeID] int NULL,
	[Operator] int NOT NULL,
	[ValueInMinutes] int not NULL,
	[LimitToOriginalWorkDay] bit not NULL,
	PRIMARY KEY ([ID])	
)

CREATE TABLE [AllocationIntervalLimitRule](
	[ID] int IDENTITY(1,1) NOT NULL,
	[ActivityTypeIDFrom] int NULL,
	[ActivityTypeIDTo] int NULL,
	[ValueInMinutes] int not NULL,
	[ExcludeTypeID] int NULL,
	[ExcludeThresholdInMinutes] int NULL,
	PRIMARY KEY ([ID])	
)

CREATE TABLE [AllocationWorkloadLimitRule](
	[ID] int IDENTITY(1,1) NOT NULL,
	[TimeSpanInDays] int not NULL,
	[AllowdWorkloadInDays] int not NULL,
	PRIMARY KEY ([ID])	
)

CREATE TABLE [AllocationDaysOfftRule](
	[ID] int IDENTITY(1,1) NOT NULL,
	[TimeSpanInDays] int not NULL,
	[DaysOff] int not NULL,
	PRIMARY KEY ([ID])	
)

CREATE TABLE CostTypesSettings (
ID int IDENTITY(1,1) NOT NULL,
CostType int NOT NULL,
Cost decimal NOT NULL,
Name nvarchar(1500),
LocationID int,
LocationName nvarchar(1500),
CostPer int NOT NULL,
PRIMARY KEY ([ID])
)

CREATE TABLE EntitiesCostSettings(
ID int IDENTITY(1,1) NOT NULL,
Name nvarchar(1500),
EntityType int NOT NULL,
EntityTypeID int NOT NULL,
EntityTypeName nvarchar(3000),
Cost decimal(8,2),
CostType int NOT NULL,
CostPer int NOT NULL,
ConsiderInOptimization bit, 
Comments nvarchar(3500),
LocationID int,
LocationName nvarchar(1000),
TargetGroupType int NOT NULL,
StateType int NOT NULL,
Primary key (ID)
)

CREATE TABLE BiddingCalendar(
ID int IDENTITY(1,1) NOT NULL,
Name nvarchar(1000) NOT NULL,
PRIMARY KEY (ID)
)

CREATE TABLE BiddingPeriod(
ID int IDENTITY(1,1) NOT NULL,
Name nvarchar(1500),
StartDate DATETIME NOT NULL,
EndDate DATETIME NOT NULL,
BiddingCalendarID int NOT NULL,
Primary key (ID),
CONSTRAINT BiddingCalendarIDKF1 FOREIGN KEY (BiddingCalendarID)
		REFERENCES BiddingCalendar(ID)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
)

CREATE TABLE BiddingRules(
ID int IDENTITY(1,1) NOT NULL,
Name nvarchar(1500),
EntityType int,
EntityTypeID int,
EntityTypeName nvarchar(250),
BiddingCalendarID int,
StartBidding int,
EndBidding int,
DaysPerInstructor int NOT NULl,
BiddingTypeID int NOT NULL,
Primary key (ID),
CONSTRAINT BiddingCalendarIDKF2 FOREIGN KEY (BiddingCalendarID)
		REFERENCES BiddingCalendar(ID)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
)

CREATE TABLE BiddingRequest(
ID int IDENTITY(1,1) NOT NULL,
InstructorID int NOT NULL,
InstructorName nvarchar(1500) NOT NULL,
BiddingRuleID int NOT NULL,
BiddingPeriodID int NOT NULl,
StartDate DateTime NOT NULL,
EndDate datetime,
[Priority] int NOT NULL,
Comments nvarchar(3000),
Status int NOT NULL,
Primary key (ID),
CONSTRAINT BiddingReqRuleIDKF2 FOREIGN KEY (BiddingRuleID)
		REFERENCES BiddingRules(ID)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
CONSTRAINT BiddingReqPeriodIDKF2 FOREIGN KEY (BiddingPeriodID)
		REFERENCES BiddingPeriod(ID)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
)